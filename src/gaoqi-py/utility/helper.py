#coding:utf-8

import os
import sys
import web
import datetime
import time
from jinja2 import Environment,FileSystemLoader

__all__ = [
       'render_template' , 'return_json'
       'datetime_to_epoch','epoch_to_datetime'
    ]

BUF_SIZE = 262144
            
def render_template(template_name, **context):
    extensions = context.pop('extensions', [])
    globals = context.pop('globals', {})

    parentdir, curdir = os.path.split(os.path.dirname(__file__))
    jinja_env = Environment(
            loader=FileSystemLoader(os.path.join(parentdir, 'html')),
            extensions=extensions,
            )
    jinja_env.globals.update(globals)

    #jinja_env.update_template_context(context)
    web.header('Content-Type','text/html')
    return jinja_env.get_template(template_name).render(context)

def datetime_to_epoch(d):
    return int(time.mktime(d.timetuple()))

def epoch_to_datetime(epoch):
    return datetime.datetime.fromtimestamp(int(epoch))


def return_json(func):
    def Function(*args):
        web.header('Content-Type', 'application/json')
        return func(*args)
    return Function


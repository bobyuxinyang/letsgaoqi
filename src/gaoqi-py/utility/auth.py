#coding:utf-8
import web
from helper import datetime_to_epoch, epoch_to_datetime
from datetime import datetime
import hashlib


__all__ = [
    'login_required', 'current_user_id', 'auth_sign', 'auth_signout', 'md5pwd'
]


def _sign_out():
    web.setcookie('gaoqi_auth', '', -1, path = '/')
    pass

def _sign_in(auth_code):
    web.setcookie('gaoqi_auth', auth_code, path = '/')
    pass

def _gen_auth_code(user_id, expir_str, ip):
    md5_code = hashlib.md5(user_id + expir_str + ip).hexdigest()
    return '%s,%s,%s' % (user_id, expir_str, md5_code)

def _get_curr_user():
    user_id = None
    auth_code = web.cookies().get('gaoqi_auth')
    if auth_code:
        temp = auth_code.split(',')
        if len(temp) != 3:
            raise Exception('auth_code错误')

        user_id = temp[0]
        expir = epoch_to_datetime(int(temp[1]))
        if datetime.now() > expir:
            # auth 过期，自动注销
            _sign_out()
            return None

        ip = web.ctx.ip
        if _gen_auth_code(user_id, str(datetime_to_epoch(expir)), ip) != auth_code:
            user_id = None
            #raise Exception('auth_code可能伪造')
            _sign_out()
            web.seeother('/')
    return user_id


def md5pwd(pwd):
    return hashlib.md5(pwd + 'gaoqi.fm').hexdigest()

def login_required(func):
    def Function(*args):
        is_login = False
        try:
            user_id = _get_curr_user()
            if user_id:
                is_login = True
        except:
            pass

        if is_login:
            return func(*args)
        else:
            #todo: 这里返回个错误信息
            return web.seeother('/')
        
    return Function

def current_user_id():
    return _get_curr_user()


def auth_sign(user_id, expir):
    epoch = datetime_to_epoch(expir)
    ip = web.ctx.ip
    auth_code = _gen_auth_code(user_id, str(epoch), ip)
    _sign_in(auth_code)
    return

def auth_signout():
    _sign_out()
    pass
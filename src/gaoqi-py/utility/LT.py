#! -*- coding:utf8 -*-
from settings import db
from datetime import datetime

# 通过新浪微博注册
REGISTER_SINA           = 0
# 发送验证邮件
ACTIVE_MAIL_SEND        = 1
# 通过邮件认证
ACTIVE_MAIL_VALIDED     = 2
# 创建一个活动
ACTIVITY_NEW            = 3
# 邀请朋友参加活动
ACTIVITY_INVITE         = 4
# 直接登陆
SIGNIN_NORMAl           = 5
# 通过新浪微博登陆
SIGNIN_SINA             = 6
# 好友tag表更新计时
FRIEND_TAGS             = 6


def TRACE(type, msg):
    now = datetime.now()
    c = {
        'time': now,
        'type': type,
        'text': msg
    }
    #print c
    db.log.save(c)

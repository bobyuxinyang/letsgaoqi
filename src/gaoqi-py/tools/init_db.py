#!/usr/bin/env python
# -*- coding: utf8 -*-
import sys

sys.path.append("..")
from settings import db

def init_tag():
    origin = ["野外烧烤","徒步","逛街","桌游","吃饭","自驾游","唱歌","家庭聚会","摄影","露营","爬山","电影","话剧","看展览","逛博物馆","动物园","演唱会","酒吧","公园散步","骑自行车","滑雪","游泳","健身","打羽毛球","打篮球","高尔夫","踢足球","打桌球","宵夜","电玩","随便逛逛","听讲座","练瑜伽"]
    tags = []
    c = 1
    for i in origin:
        tags.append({"name": i, "count": 0, 'index': c})
        c += 1
    db.tags.insert(tags)

if __name__ == '__main__':
    if len(sys.argv) < 2 or sys.argv[1] != 'dbreset':
        print """
Warnning: this program use to reset mongodb.

USAGE:
    init_db dbreset
"""
        exit()

    print 'start'
    db.drop_collection('user')
    db.drop_collection('activity')
    db.drop_collection('sinaweibo_friends')
    db.drop_collection('log')
    db.drop_collection('tags')
    init_tag()
    print '...Succ'

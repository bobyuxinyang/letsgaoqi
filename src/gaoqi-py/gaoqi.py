# coding=utf-8
from urls import urls

import web
import newrelic.agent
newrelic.agent.initialize('/etc/newrelic.ini')

app = web.application(urls, globals(), autoreload = True)
application = app.wsgifunc()
application = newrelic.agent.wsgi_application()(application)

session = web.session.Session(app, web.session.DiskStore('tmp/sessions'), initializer={'count': 0})

def session_hook():
    web.ctx.session = session

app.add_processor(web.loadhook(session_hook))

if __name__ == '__main__':
    app.run()
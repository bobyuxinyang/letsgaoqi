urls = (
    '/', "controller.static.index",
    '/', "controller.static.pre_index",
    "/favicon.ico" , "controller.static.favicon",

    '/register', "controller.static.register",
    '/activity', "controller.static.activity",
    '/all_tags', "controller.static.all_tags",

    '/signup/email_chk', "controller.signup.email_chk",
    '/signup/pwd_chk', "controller.signup.pwd_chk",
    '/signup/reg_user', "controller.signup.reg_user",
    '/signup/active_mail', "controller.signup.active_mail",
    '/signup/active_mail_info', "controller.signup.get_active_mail_info",

    '/user/signin', "controller.user.signin",
    '/user/signout', "controller.user.signout",
    '/user/detail', "controller.user.signout",
    '/user/current_user', "controller.user.get_current_user",
    '/user/active/(.*)', "controller.user.active",
    '/user/tip', "controller.user.tip",
    '/user/my_tags', "controller.user.my_tags",
    '/user/choose_tags', "controller.user.choose_tags",
    '/user/choose_one_tag', "controller.user.choose_one_tag",
    '/user/friends_tags', "controller.user.friends_tags",


    '/connect/sinaweibo/go', "controller.connect.sinaweibo_go",
    '/connect/sinaweibo/callback', "controller.connect.sinaweibo_callback",
    '/connect/sinaweibo/find_friends', "controller.connect.find_sinaweibo_friends",
    '/connect/sinaweibo/userinfo', "controller.connect.get_sinaweibo_userinfo",

    '/activities/user_now', "controller.activity.get_user_now",
    '/activities/user_before', "controller.activity.get_user_before",

    '/activities/update', "controller.activity.update_activity",
    '/activities/interest', "controller.activity.interest",
    '/activities/activity_detail', "controller.activity.get_activity_detail",
    '/activities/simple_activity', "controller.activity.get_simple_activity",


    '/activities/confirm', "controller.activity.confirm_activity",
    '/activities/quit', "controller.activity.quit_activity",
    '/activities/invite_more', "controller.activity.invite_more_participators",
    '/activities/update_plan_at', "controller.activity.update_plan_at",
    '/activities/update_text', "controller.activity.update_text",
    '/activities/update_detail_text', "controller.activity.update_detail_text",

    '/activities/comments', "controller.activity.get_comments",
    '/activities/new_comment', "controller.activity.new_comment",
    '/activities/new_comment_reply', "controller.activity.new_comment_reply",
    '/activities/get_comment_replies', "controller.activity.get_comment_replies",
    '/activities/has_before_activity', "controller.activity.has_before_activity",

    '/notification/invitations', "controller.notification.get_invitations"
)
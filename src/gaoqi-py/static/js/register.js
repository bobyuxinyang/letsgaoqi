﻿define(function (require) {
    var service = require('utility/service');
    require('utility/gaoqi-artTxtCount');

    var currentUser = {};

//    service.get_sinaweibo_userinfo(function (e){
//        currentUser = e;
//        $('#user-icon').prop('src', e.profile_image_url);
//        $('#txt-weibo_name').html(e.username);
//        $('#txt-username').val(e.username);
//    });


    function initValidate() {
        $.validator.addMethod("stringCheck", function(value, element) {
                return this.optional(element) || /^[\u0391-\uFFE5\w]+$/.test(value);
            }, "只能包括中文、英文、数字和下划线");


        $.validator.addMethod("byteRangeLength", function(value, element, param) {
            var length = value.length;

            return this.optional(element) || ( length >= param[0] && length <= param[1] );
        },  "请确保输入的值在3-15个字节之间");

        $.validator.addMethod('email_dup_chk', function (value, element, param) {
            var result = service.email_chk_Sync(function() {}, {email: value});
            return result.Status == "Success";
        });


        $('#submitForm').validate({
            rules: {
                username: {
                    required:true,
                    stringCheck:true,
                    byteRangeLength:[3,15]
                },
                email:{
                    required:true,
                    email:true,
                    email_dup_chk: true
                },
                password: {
                    required:true
                }
            },

            messages: {
                username: {
                    required: "请填写用户名",
                    stringCheck: "用户名只能包括中文、英文、数字和下划线",
                    byteRangeLength: "用户名必须在3-15个字符之间"
                },
                email:{
                    required: "请输入一个Email地址",
                    email: "你输入的邮箱地址格式不正确",
                    email_dup_chk: '这个email已经被注册了'
                },
                password: {
                    required: "密码不能为空"
                }
            },
            focusInvalid: false,
            onkeyup: false
        });
    };


    function register(){
        var params = {
              email: $('#txt-email').val()
            , password: $('#txt-password').val()
        };
        service.reg_user(function (e) {
            if (e.Status == 'Success') {
                // 注册成功，直接登录
                window.location = '/signup/active_mail';
            }else {
                window.location.reload();
            }
        }, params);
   };

    function initEvent() {
        $('#btn-reg').live('click', function (event) {
            var validate = $('#submitForm').valid();
            if(validate) register();
        });

        $('#txt-password, #txt-email').artTxtCount({
            btnOnCallback: function () {
                $(this).parent().prev().css({'visibility': 'hidden'});
            },
            btnOffCallback: function () {
                $(this).parent().prev().css({'visibility': 'visible'});
            }
        });

        $('#txt-password').live("keydown",function(event){
            if(event.keyCode == 13) {
                var validate = $('#submitForm').valid();
                if(validate)  register();
            }
        });
    };


    
    $(function () {
        initValidate();
        initEvent();
    });
});

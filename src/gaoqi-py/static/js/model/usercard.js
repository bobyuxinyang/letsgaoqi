﻿define(function (require) {
    var service = require('utility/service');
    
    window.UserCard = Backbone.Model.extend({
        initialize: function () {

        },
        getAll: function () {
            var _self = this;

            // 从服务器上拿当前用户的基本信息
            var userCard = service.get_current_user_Sync();

            _self.attributes = userCard;
            _self.trigger("getAll");
        }

    });

});

﻿define(function (require) {
    require('model/participator');
    var service = require('utility/service');
    
    window.ParticipatorList = Backbone.Collection.extend({
        model: Participator,
        comparator: function (e) {
            return e.get('update_at');
        },
        idList: [],
        initialize: function () {
            var _self = this;
            _self.idList = [];
            var models = _self.models;

            for(var key in models){
                _self.idList.push(models[key].get('user')['_id']);
            }
        },

        getParticipators: function(){
            return this.idList;
        },
        removeOne: function(index){
            var _self = this;
            var id = _self.models[index].get('user')['_id'];

            _self.idList = _.reject(_self.idList, function(item){
                return item == id;
            })
        },
        addOne: function(index){
            var _self = this;
            var id = _self.models[index].get('user')['_id'];

            _self.idList.push(id);
        }
    });
});
﻿define(function (require) {
    var stautsMap = {
        ongoing: {
            StatusClass: 'icon-face-smile-b',
            StatusInfo: '正在搞',
            Des: '正在兴高彩烈的搞起中！',
            Opposite: {
                Status: "canceled",
                StatusClass: 'icon-face-sad',
                StatusInfo: '不搞了'
            }
        },
        past: {
            StatusClass: 'icon-face-smile-b',
            StatusInfo: '搞定了',
            Des: '恭喜！这是个已经搞定的活动！',
            Opposite: {
                Status: "ongoing",
                StatusClass: 'icon-face-smile',
                StatusInfo: '重新搞起'
            }
        },
        canceled: {
            StatusClass: 'icon-face-sad-b',
            StatusInfo: '不搞了',
            Des: '因为某些原因暂时不搞了',
            Opposite: {
                Status: "ongoing",
                StatusClass: 'icon-face-smile',
                StatusInfo: '搞起'
            }
        },
        
        private: {
            OpenClass: 'icon-lock-b',
            OpenInfo: '不公开',
            Des: '只有活动发起者和参与者能看',
            Opposite: {
                Status: "public",
                OpenClass: 'icon-unlock',
                OpenInfo: '公开'
            }
        },
        public: {
            OpenClass: 'icon-unlock-b',
            OpenInfo: '公开',
            Des: '活动发起者和参与者的朋友也可以看到',
            Opposite: {
                Status: "private",
                OpenClass: 'icon-lock',
                OpenInfo: '不公开'
            }
        }
    };
    
    window.Status= Backbone.Model.extend({
        setStatusInfo: function () {
            var status = this.get('status');
            
            this.set({ 'available_class': stautsMap[status].StatusClass,
                       'available_info': stautsMap[status].StatusInfo,
                       'available_des': stautsMap[status].Des,
                       'available_op_info': stautsMap[status].Opposite.StatusInfo,
                       'available_op_class': stautsMap[status].Opposite.StatusClass });
        },
        setOpenInfo: function () {
            var status = this.get('visibility');
           
            this.set({ 'visibile_class': stautsMap[status].OpenClass,
                       'visibile_info': stautsMap[status].OpenInfo,
                       'visibile_des': stautsMap[status].Des,
                       'visibile_op_info': stautsMap[status].Opposite.OpenInfo,
                       'visibile_op_class': stautsMap[status].Opposite.OpenClass});
        },
        initialize: function (model) {
            this.setStatusInfo();
            this.setOpenInfo();
        },
        changeVisibility: function(){
            var status = this.get('visibility');
            this.set({'visibility': stautsMap[status].Opposite.Status});
            this.setOpenInfo();
        },
        changeAvailability: function(stuatus){
            var status = this.get('status');
            this.set({'status': stautsMap[status].Opposite.Status});
            this.setStatusInfo();
        }
    });

});

﻿define(function (require) {
    var service = require('utility/service');

    window.Topbar = Backbone.Model.extend({
        initialize: function () {
        },
        getAll: function () {
            var _self = this;
            var notifications = [];

            _self.attributes = {
                invitations: service.get_invitations_Sync(),
                notifications: notifications
            };

            _self.trigger("getAll");
        },
        checkInvitation: function (id) {
            var invitations = this.get('invitations');

            invitations = _.reject(invitations, function (invitation) {
                return invitation.invitation_id == id;
            });

            this.set({ 'invitations': invitations })
        },
        ignoreNotification: function (noti_id_list) {
            var notifications = this.get('notifications');

            for (var key in noti_id_list) {
                notifications = _.reject(notifications, function (notification) {
                    return notification.latest_id == noti_id_list[key];
                });
            }


            this.set({ 'notifications': notifications })
        },
        signout: function () {
            service.signout(function (e) {
                if (e.Status == "Success") {
                     window.location = '/';
                }
            });
        }
    });

});

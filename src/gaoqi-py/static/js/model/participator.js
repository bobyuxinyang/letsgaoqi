﻿define(function (require) {
    window.Participator = Backbone.Model.extend({
        initialize: function () {
            if(this.get('status') === 'no-reply')
                this.set({'status_class': 'unrecognized'})
            else
                this.set({'status_class': 'recognized'})
        }

    });

});

define(function (require) {
    window.User = Backbone.Model.extend({
        initialize: function () {
        },
        selected: function () {
            this.view.selectUserCallback();
        },
        activeRow: function () {
            this.view.activeRow();
        },
        unActiveRow: function () {
            this.view.unActiveRow();
        }
    });

});

define(function (require) {
    var service = require('utility/service');

    require('utility/datetime');
    require('utility/string');


    window.SimpleActivity = Backbone.Model.extend({
        id: null,

        initialize: function (model, id) {
            var $self = this;
            $self.id = id;
        },

        getAll: function () {
            var $self = this;
            service.get_simple_activity(
                function(activity) {
                    // bind the activity
                    $self.attributes = activity;

                    // set time info
                    var starTime = Date.fromEpoch($self.get('plan_at'));
                    var createTime = Date.fromEpoch($self.get('created_at'));
                    $self.set({ 'plan_at_str':
                        starTime.toFullDateTimeAndDay() + "  "
                        + starTime.DateDiff()
                    });
                    $self.set({ 'created_at_str':
                       createTime.DateDiff()
                    });

                    $self.trigger('getAll');

                }, {activity_id: $self.id});
        }

    });
});
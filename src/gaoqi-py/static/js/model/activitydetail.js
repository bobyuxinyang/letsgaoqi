﻿define(function (require) {
    var service = require('utility/service');
    
    require('utility/gaoqi-displaystatusinfo');
    require('utility/datetime');
    require('utility/string');


    //todo merge before
    window.Current_user = null;
    service.get_current_user_Sync(function (e) {
        window.Current_user = e;
    });

    window.ActivityDetail = Backbone.Model.extend({
        id: null,
        userInfo: null,

        initialize: function (model, id) {
            this.id = id;
        },

        getAll: function () {
            var $self = this;
            var id = $self.id;

            $.ajaxSetup({cache: false});
            
//            service.get_activity_detail(function (activity) {
//                $self.attributes = activity;
//
//                if ($self.checkRole()) {
//                    $self.setTimeInfo();
//                    $self.setUserStauts();
//
//                    $.ajaxSetup({cache: true});
//                }
//
//                $self.trigger("getAll");
//            }, { activity_id: id });

            var activity = {
                'activity_id': 1,
                'created_at': new Date().toEpoch() - 80000,
                'text': '咦咦咦咦咦咦咦咦咦咦咦咦',
                'detail_text': 'FUCK!!!',
                'plan_at': new Date().toEpoch() + 30000,
                'owner': {
                   '_id': 22, 'username': '王昊',  'description': 1, 'profile_image_url': 'http://tp3.sinaimg.cn/1930080130/50/5609852091/0', 'weibo_id': '1930080130'
                },
                'participators':[{
                   'update_at': new Date().toEpoch() - 20000,
                   'user': {
                       '_id': 22, 'username': '王昊',  'description': 1, 'profile_image_url': 'http://tp3.sinaimg.cn/1930080130/50/5609852091/0', 'weibo_id': '1930080130'
                   }
                },
                {
                   'update_at': new Date().toEpoch() - 40000,
                   'user': {
                       '_id': 11, 'username': '王昊',  'description': 1, 'profile_image_url': 'http://tp2.sinaimg.cn/1310915297/50/5614164220/0', 'weibo_id': '1310915297'
                   }
                }],
                tags: [{
                    'order': 2, 'name': '梦游'
                }, {
                    'order': 3, 'name': '踢足球'
                },{
                    'order': 4, 'name': '摄影'
                }],
                'comment_count': '5'
            };

            $self.attributes = activity;
            $self.setTimeInfo();
//            $self.setUserStauts();
            $self.trigger("getAll");

        },



        /*check user role*/
        checkRole: function () {
            var $self = this;
            var participators = $self.get('participators');

            var participator = _.find(participators, function (item) {
                return item.user.user_id == window.Current_user.user_id;
            });

            if (participator) {
                $self.userInfo = participator;
                return true;
            }
            else {
                //todo:如果不是参与者，跳回首页
                window.location.href = '/';
                return false;
            }
        },


        /*set activity info*/
        setTimeInfo: function () {
            var starTime = Date.fromEpoch(this.get('plan_at'));
            var createTime = Date.fromEpoch(this.get('created_at'));

            this.set({
                'plan_at_str': starTime.toFullDateTimeAndDay() + "  " + starTime.DateDiff()
                 , 'created_at_str': createTime.DateDiff()
            });
        },


        setUserStauts: function () {
            var self = this;
            self.set({ 'my-status': self.userInfo.status });
        },


        /*edit activity info*/
        editDetail: function (detail) {
            var $self = this;
            $self.set({ 'detail_text': detail });

            service.update_detail_text(function (e) {
                $.displayStatusInfo(e);
            }, { activity_id: $self.id, detail_text: detail})
        },
        editTime: function (starTime, timeStr) {
            var $self = this;
            $self.set({ 'plan_at': starTime });
            $self.set({ 'plan_at_str': timeStr });

            service.update_plan_at(function (e) {
                $.displayStatusInfo(e);
            }, { activity_id: $self.id, plan_at: starTime })
        },
        editTitle: function (title) {
            var $self = this;
            $self.set({ 'text': title });

            service.update_text(function (e) {
                $.displayStatusInfo(e);
            }, { activity_id: $self.id, text: title.htmlEncode() })
        },

        


        /* user action*/
        changeParticipatorStauts: function (id, status) {
            var participators = this.get('participators');
            for (var key in participators) {
                if (participators[key].user_id == id)
                    participators[key].status = status;
            }

            return participators;
        },



        confirm: function () {
            var self = this;
            var id = self.userInfo.user_id;

            service.confirm(function (e) {
                var list = self.changeParticipatorStauts(id, 'in-activity');
                self.trigger('user-confirmed', list);

                $.displayStatusInfo(e);
            }, { activity_id: self.id });
        },


        interest: function () {
            var self = this;
            var id = self.userInfo.user_id;

            service.interest(function (e) {
                var list = self.changeParticipatorStauts(id, 'making-decision');
                self.trigger('user-interested', list);

                $.displayStatusInfo(e);
            }, { activity_id: self.id });
        },


        quit: function () {
            var $self = this;

            service.quit(function (e) {
                if (e.Status == 'Success')
                    window.location.href = '/';
            }, { activity_id: $self.id });
        },

        addParticipators: function (list, isSendWeiboInvitation, inviteText) {
            var uniq = function (array) {
                var i = 0, j = 0;
                while (undefined !== array[i]) {
                    j = i + 1;
                    while (undefined !== array[j]) {
                        if (array[i].user.weibo_id == array[j].user.weibo_id) {
                            array.splice(j, 1);
                        }
                        ++j;
                    }
                    ++i;
                }

                return array;
            }

            var participators = this.get('participators');
            var newList = uniq(participators.concat(list));

            this.invite(newList, isSendWeiboInvitation, inviteText);
        },

        invite: function (list, isSendWeiboInvitation, inviteText) {
            var $self = this;
            var userList = _.map(list, function (user) {
                return user.user;
            })


            service.invite_more(function (e) {
                var data = e.Data;
                for(var key in list){
                    for(var index in data){
                        if(list[key].user.weibo_id == data[index].weibo_id)
                            list[key].user_id = list[key].user.user_id = data[index].user_id;
                    }
                }

                $self.set({ 'participators': list });
                $self.trigger("invitedFriends", list);

                $.displayStatusInfo(e);
            }, {
                activity_id: $self.id,
                invitePeople: JSON.stringify(userList),
                invite_text: inviteText,
                is_send_weibo_invitation: isSendWeiboInvitation
            })
        },



        comment: function (newComment) {
            var self = this;
            var comments = self.get('comments');

            service.new_comment(function (e) {
                newComment['comment_id'] = e.Data['comment_id'];
                comments.push(newComment);

                self.set({'comments': comments});
                self.trigger("new_message", comments);

                $.displayStatusInfo(e);
            }, { activity_id: self.id, sender_id: newComment.sender.sender_id, text: newComment.text })
        },

        changeComments: function(comment_id){
            var comments = this.get('comments');
            for(var key in comments){
                if(comments[key]['comment_id'] == comment_id)
                    ++ comments[key]['replies_count'];
            }
            this.set({'comments': comments})
        }
    });
});

define(function (require) {
    require('model/user');
    var service = require('utility/service');
    window.UserList = Backbone.Collection.extend({
        model: User,
        comparator: function (e) {
        },
        initialize: function () {
            this.getAll();
        },
        getAll: function () {
            var _self = this;

            userlist = [];

            _self.add(userlist);

        },
        updateKey: function (newKey) {
            var _self = this;
            service.find_sinaweibo_friends(function (e) {
                _self._reset();
                _self.add(e);
                _self.trigger('getAll');
               
            }, {key: newKey});
        }
    });
});
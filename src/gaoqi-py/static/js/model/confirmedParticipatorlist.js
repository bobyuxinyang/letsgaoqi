﻿define(function (require) {
    require('model/confirmedParticipator');
    var service = require('utility/service');

    window.ConfirmedParticipatorList = Backbone.Collection.extend({
        model: Participator,
        comparator: function (e) {
            return e.get('update_at');
        },
        initialize: function () {
        }
    });
});
﻿define(function () {
    /*---- Datetime 相关方法 --*/

    /**
    * 对Date的扩展，将 Date 转化为指定格式的String
    * 月(M)、日(d)、12小时(h)、24小时(H)、分(m)、秒(s)、周(E)、季度(q) 可以用 1-2 个占位符
    * 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
    * eg:
    * (new Date()).pattern("yyyy-MM-dd hh:mm:ss.S") ==> 2007-07-02 08:09:04.423
    * (new Date()).pattern("yyyy-MM-dd E HH:mm:ss") ==> 2007-03-10 二 20:09:04
    * (new Date()).pattern("yyyy-MM-dd EE hh:mm:ss") ==> 2007-03-10 周二 08:09:04
    * (new Date()).pattern("yyyy-MM-dd EEE hh:mm:ss") ==> 2007-03-10 星期二 08:09:04
    * (new Date()).pattern("yyyy-M-d h:m:s.S") ==> 2007-7-2 8:9:4.18
    */
    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1,
            //月份
            "d+": this.getDate(),
            //日
            "h+": this.getHours() % 12 == 0 ? 12 : this.getHours() % 12,
            //小时
            "H+": this.getHours(),
            //小时
            "m+": this.getMinutes(),
            //分
            "s+": this.getSeconds(),
            //秒
            "q+": Math.floor((this.getMonth() + 3) / 3),
            //季度
            "S": this.getMilliseconds() //毫秒
        };
        var week = {
            "0": "\u65e5",
            "1": "\u4e00",
            "2": "\u4e8c",
            "3": "\u4e09",
            "4": "\u56db",
            "5": "\u4e94",
            "6": "\u516d"
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        }
        if (/(E+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, ((RegExp.$1.length > 1) ? (RegExp.$1.length > 2 ? "\u661f\u671f" : "\u5468") : "") + week[this.getDay() + ""]);
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    }

    //2011-02-03 03:05          ToFullDateTime
    Date.prototype.toFullDateTime = function () {
        return this.format("yyyy-MM-dd HH:mm");
    }

    //02-03 03:05           ToShortDateTime
    Date.prototype.toShortDateTime = function () {
        return this.format("MM-dd HH:mm");
    }
    //02-03        ToShortDate
    Date.prototype.toShortDate = function () {
        return this.format("MM-dd");
    }

    //2011-02-03                   ToFullDate
    Date.prototype.toFullDate = function () {
        return this.format("yyyy-MM-dd");
    }

    //03:05:33              ToFullTime
    Date.prototype.toFullTime = function () {
        return this.format("HH:mm:ss");
    }

    //03:05              ToShortTime
    Date.prototype.toShortTime = function () {
        return this.format("HH:mm");
    }

    //星期六                ToDayOfWeek
    Date.prototype.toDayOfWeek = function () {
        return this.format("EEE");
    }

    //2011-02-03    星期六   ToFullDateAndDay
    Date.prototype.toFullDateAndDay = function () {
        return this.format("yyyy-MM-dd EEE");
    }

    //2011-02-03 09:00 星期六       ToFullDateAndDay
    Date.prototype.toFullDateTimeAndDay = function () {
        return this.format("yyyy-MM-dd HH:mm EEE");
    }

    //刚刚,x天前 之类                  ToNiceTime
    Date.prototype.toNiceTime = function () {
        var now = new Date();
        var totalSecond = Math.floor((now - this) / 1000);
        if (totalSecond < 60) {
            return "刚刚";
        }

        var totalMinute = Math.floor(totalSecond / 60);
        if (totalMinute < 60) {
            return totalMinute + " 分钟前";
        }

        var dayDiff = Math.floor(totalMinute / 60 / 24);
        var yearDiff = Math.floor(dayDiff / 365);
        if (dayDiff == 0) {
            var totalHour = Math.floor(totalMinute / 60);
            return totalHour + " 小时前";
        }
        if (dayDiff == 1) {
            return "昨天 " + this.format("HH:mm");
        }
        if (dayDiff == 2) {
            return "前天 " + this.format("HH:mm");
        }
        if (yearDiff == 0) {
            return this.toShortDateTime();
        }
        return this.toFullDateTime();
    }


    //OnlyDateDiff
    Date.prototype.OnlyDateDiff = function () {
        var now = new Date(
            new Date().getFullYear(),
            new Date().getMonth(),
            new Date().getDate(),
            0,
            0
        );
        
        var diff = this.getTime() - now.getTime();

        var diffMap =
        {
            dayDiff: Math.round(diff / 86400000),
            monthDiff: Math.round(diff / (86400000 * 30)),
            yearDiff: Math.round(diff / (86400000 * 30 * 12))
        };

        if (diffMap.dayDiff == 0)
            return "今天";

        if (diffMap.monthDiff == 0)
            return diffMap.dayDiff > 0 ?
                "大约 " + diffMap.dayDiff + " 天后"
                : "大约 " + -diffMap.dayDiff + " 天前";

        if (diffMap.yearDiff == 0)
            return diffMap.monthDiff > 0 ?
                "大约 " + diffMap.monthDiff + " 月后"
                : "大约 " + -diffMap.monthDiff + " 月前";

        return diffMap.yearDiff > 0 ?
            "大约 " + diffMap.yearDiff + " 年后"
            : "大约 " + -diffMap.yearDiff + " 年前";

    }

    //DateDiff
    Date.prototype.DateDiff = function () {
        var now = new Date();
        var diff = this.getTime() - now.getTime();

        var diffMap =
        {
            secondDiff: Math.round(diff / 1000),
            hourDiff: Math.round(diff / 3600000),
            dayDiff: Math.round(diff / 86400000),
            monthDiff: Math.round(diff / (86400000 * 30)),
            yearDiff: Math.round(diff / (86400000 * 30 * 12))
        };


        if (diffMap.hourDiff == 0)
            return diffMap.secondDiff > 0 ?
                "马上"
                : "刚刚";
        if (diffMap.dayDiff == 0)
            return diffMap.hourDiff > 0 ?
                "大约 " + diffMap.hourDiff + " 小时后"
                : "大约 " + -diffMap.hourDiff + " 小时前";
        if (diffMap.monthDiff == 0)
            return diffMap.dayDiff > 0 ?
                "大约 " + diffMap.dayDiff + " 天后"
                : "大约 " + -diffMap.dayDiff + " 天前";
        if (diffMap.yearDiff == 0)
            return diffMap.monthDiff > 0 ?
                "大约 " + diffMap.monthDiff + " 月后"
                : "大约 " + -diffMap.monthDiff + " 月前";

        return diffMap.yearDiff > 0 ?
            "大约 " + diffMap.yearDiff + " 年后"
            : "大约 " + -diffMap.yearDiff + " 年前";
        
    }




    // String转换成Date格式
    String.prototype.toDateTime = function () {
        if (this.indexOf(".") > 0) {
            var data = this.split('.');
            return new Date(data[0], data[1] - 1, data[2]);
        }
        else if (this.indexOf("-") > 0) {
            var data = this.split('-');
            return new Date(data[0], data[1] - 1, data[2]);
        }
        else if (this.indexOf("/") > 0) {
            var data = this.split('/');
            return new Date(data[2], data[0] - 1, data[1]);

        }
    }


    //星期六                ToDayOfWeek
    Date.prototype.toDayOfWeek = function () {
        return this.format("EEE");
    }



    // 获得本周末的默认时间
    Date.prototype.WeekendTime= function () {
        var day = this.getDay();
        
        if (day != 6)
            return new Date(
                this.getFullYear(),
                this.getMonth(),
                this.getDate() + 6 - day,
                10
            )
        else 
            return new Date(
                this.getFullYear(),
                this.getMonth(),
                this.getDate() + 1,
                10
            )
    }




    Date.prototype.dateAdd = function (interval, number) {
        var d = this;
        var k = { 'y': 'FullYear', 'q': 'Month', 'm': 'Month', 'w': 'Date', 'd': 'Date', 'h': 'Hours', 'n': 'Minutes', 's': 'Seconds', 'ms': 'MilliSeconds' };
        var n = { 'q': 3, 'w': 7 };
        eval('d.set' + k[interval] + '(d.get' + k[interval] + '()+' + ((n[interval] || 1) * number) + ')');
        return d;
    }

    Date.prototype.dateDiff = function (interval, objDate2) {
        var d = this, i = {}, t = d.getTime(), t2 = objDate2.getTime();
        i['y'] = objDate2.getFullYear() - d.getFullYear();
        i['q'] = i['y'] * 4 + Math.floor(objDate2.getMonth() / 4) - Math.floor(d.getMonth() / 4);
        i['m'] = i['y'] * 12 + objDate2.getMonth() - d.getMonth();
        i['ms'] = objDate2.getTime() - d.getTime();
        i['w'] = Math.floor((t2 + 345600000) / (604800000)) - Math.floor((t + 345600000) / (604800000));
        i['d'] = Math.floor(t2 / 86400000) - Math.floor(t / 86400000);
        i['h'] = Math.floor(t2 / 3600000) - Math.floor(t / 3600000);
        i['n'] = Math.floor(t2 / 60000) - Math.floor(t / 60000);
        i['s'] = Math.floor(t2 / 1000) - Math.floor(t / 1000);
        return i[interval];
    }


    Date.prototype.toFullChineseDateFormat = function () {
        var yy = this.getFullYear();
        var mon = this.getMonth() + 1;
        var da = this.getDate();
        var day = this.getDay();

        var date = yy + "年";
        if (mon < 10) date += '0';
        date += mon + "月";
        if (da < 10) date += '0';
        date += da + "日";
        var date = date + "&nbsp星期" + "日一二三四五六".charAt(day);
        return date;
    }
    Date.prototype.toFullTimeFormat = function () {

        var hh = this.getHours();
        var mm = this.getMinutes();
        var ss = this.getTime() % 60000;
        ss = (ss - (ss % 1000)) / 1000;
        var clock = hh + ':';
        if (mm < 10) clock += '0';
        clock += mm + ':';
        if (ss < 10) clock += '0';
        clock += ss;

        return clock;
    }


    Date.prototype.toFullTimeFormatWithoutSec = function () {

        var hh = this.getHours();
        var mm = this.getMinutes();
        var ss = this.getTime() % 60000;
        ss = (ss - (ss % 1000)) / 1000;
        var clock = hh + ':';
        if (mm < 10) clock += '0';
        clock += mm;

        return clock;
    }

    Date.prototype.toEpoch = function () {
        if (!this) return null;
        return Math.round(this.getTime() / 1000);
    };
    Date.fromEpoch = function (epoch) {
        if (!epoch) return null;
        return new Date(epoch * 1000);
    };

    Date.fromTicks = function (ticks) {
        if (!ticks) return null;
        return new Date(ticks);
    };

    Date.prototype.toTicks = function () {
        if (!this) return null;
        return Math.round(this.getTime());
    };
    Date.prototype.getDifferDayCount = function (targetDate) {
        return Math.abs((this - targetDate) / 86400000);
    };
    Date.prototype.getOnlyDate = function () {
        var yyyy = this.getFullYear();
        var MM = this.getMonth();
        var dd = this.getDate();
        return new Date(yyyy, MM, dd);
    };

    Date.prototype.addDays = function (n) {
        this.setDate(this.getDate() + 0 + n);
        return this;
    }
    Date.prototype.addMonths = function (n) {
        this.setMonth(this.getMonth() + 0 + n);
        return this;
    }



    // String转换成Date格式
    String.prototype.toDate = function () {
        if (this.indexOf(".") > 0) {
            var data = this.split('.');
            return new Date(data[0], data[1] - 1, data[2]);
        }
        else if (this.indexOf("-") > 0) {
            var data = this.split('-');
            return new Date(data[0], data[1] - 1, data[2]);
        }
        else if (this.indexOf("/") > 0) {
            var data = this.split('/');
            return new Date(data[2], data[0] - 1, data[1]);
        }
    }

})
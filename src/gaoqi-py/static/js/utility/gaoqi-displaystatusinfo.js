﻿/// displayStatusInfo
define(function (require, exports, module) {
    $.displayStatusInfo = function (statusInfo, autoHide) {
        if (autoHide == null) {
            autoHide = true;
        }

        var status = statusInfo.Status;
        var info = statusInfo.Info;

        var panelDom = $('<div class="display-status-info snap-status-info" >')
        .append(info)
        .appendTo('body')
        .animate({"bottom": "+=100px"}, "slow");


        if (autoHide) {
            setTimeout(function () {
                panelDom.animate({"bottom": "-=100px"}, "slow").remove();
            }
            , 3000);
        }

    }
});




﻿define(function () {
    String.prototype.getQueryString = function (str) {
        var rs = new RegExp("(^|)" + str + "=([^\&]*)(\&|$)", "gi").exec(this), tmp;

        if (tmp = rs) {
            return tmp[2];
        }

        return "";
     } 
    
    String.prototype.htmlEncode = function () {
        var temp = document.createElement ("div");
        (temp.textContent != null) ? (temp.textContent = this) : (temp.innerText = this);
        var output = temp.innerHTML;
        temp = null;
        return output;
    }

    String.prototype.htmlDecode = function () {
        var temp = document.createElement("div");
        temp.innerHTML = this;
        var output = temp.innerText || temp.textContent;
        temp = null;
        return output;
    }

    String.prototype.trim = function () {
        return this.replace(/(^\s*)|(\s*$)/g, "");
    }

    String.prototype.changeLineToBr= function () {
        var string = this.replace(/\n/g,'<br />');
        return string;
    }

    String.prototype.changeBrToLine= function () {
        var re = /(<br \/><br\/>|<br>|<BR>|<BR\/>)/g;
        var string = this.replace(re, "\n");

        return string;
    }

    ///字符串匹配
    String.prototype.startWith = function (str) {
        if (str == null || str == "" || this.length == 0 || str.length > this.length)
            return false;
        if (this.substr(0, str.length) == str)
            return true;
        else
            return false;
        return true;
    }

})

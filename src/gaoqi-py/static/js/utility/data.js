﻿/****** mock数据 ********/
define(function (require, exports, module) {
    //是否是 活动的 发起者
    var _mock_hasPemission = true;
    //活动邀请列表
//    var _mock_user_invitation=
//        [{
//            invitation_id: '1',
//            sender: { profile_image_url: '/static/images/0.jpg', username: '王昊' },
//            activity: { text: '一起吃面条', activity_id: '11111'},
//        }, {
//            invitation_id: '1',
//            sender: { profile_image_url: '/static/images/0.jpg', username: '王昊' },
//            activity: { text: '一起吃面条,吃完面条再庆祝媳妇跑了' ,activity_id: '2222'},
//        }];
    var _mock_user_invitation=
    [{
       "created_at": 1391828873,
       "activity_id": '4eacd99b9c3d1a0f2c000001',
       "activity_name": "去崇明岛！",
       "inviter": {
         "username": "恩恩杨恩恩杨恩恩杨恩恩杨",
         "user_id": '"4eacd97c9c3d1a0f2c000000'
      }
    } ];

    //通知列表
    var _mock_user_latest=
        [{
            creat_at: '1316403559',
            sender: { profile_image_url: '/static/images/0.jpg', username: '王昊' },
            content: '1在"一起吃面条"活动中 给您留言了。',
            activity_id: '2222',
            latest_id: '1'
        }, {
            creat_at: '1316403560',
            sender: { profile_image_url: '/static/images/0.jpg', username: '王昊' },
            content: '2在"一起吃面条"活动中 给您留言了。',
            activity_id: '11111',
            latest_id: '2'
        },{
            creat_at: '1316403561',
            sender: { profile_image_url: '/static/images/0.jpg', username: '王昊' },
            content: '3在"一起吃面条"活动中 给您留言了。',
            activity_id: '2222',
            latest_id: '3'
        }, {
            creat_at: '1316403562',
            sender: { profile_image_url: '/static/images/0.jpg', username: '王昊' },
            content: '4在"一起吃面条"活动中 给您留言了。',
            activity_id: '11111',
            latest_id: '4'
        },{
            creat_at: '1316403563',
            sender: { profile_image_url: '/static/images/0.jpg', username: '王昊' },
            content: '5在"一起吃面条"活动中 给您留言了。',
            activity_id: '2222',
            latest_id: '5'
        }, {
            creat_at: '1316403564',
            sender: { profile_image_url: '/static/images/0.jpg', username: '王昊' },
            content: '6在"一起吃面条"活动中 给您留言了。',
            activity_id: '11111',
            latest_id: '6'
        },{
            creat_at: '1316403565',
            sender: { profile_image_url: '/static/images/0.jpg', username: '王昊' },
            content: '7在"一起吃面条"活动中 给您留言了。',
            activity_id: '2222',
            latest_id: '7'
        }, {
            creat_at: '1316403566',
            sender: { profile_image_url: '/static/images/0.jpg', username: '王昊' },
            content: '8在"一起吃面条"活动中 给您留言了。',
            activity_id: '11111',
            latest_id: '8'
        }];
    //一个会员
    var _userCard = {
        profile_image_url: "/static/images/1.jpg",
        description: "我就是我，我就是我我就是我我就是我！",
        username: "我就是我"
    }
    //正在搞的活动 列表
    var _ongoingActivityList = [{
        activity_id: '11111',
     
        owner: {
            profile_image_url: "/static/images/1.jpg",
            username: "王昊",
        },
        
        //todo:
        is_mine: true,
        is_confirmed: true,

        text: "庆祝老婆跑了！ 大家来吃面条！！！",
        plan_at: "1316443559", 
        created_at : "1316403559", 
        detail_text: "",
        
        status: 'ongoing', 
        visibility: 'private',
        
        participators: [{
            username: '陈陈',
            profile_image_url: '/static/images/2.jpg',
            status: 'Confirmed'                            
        },
        {
            username: '黄冠',
            profile_image_url: '/static/images/3.jpg',
            status: 'Unconfirmed'                            
        }],
        
        comments: [{
            sender: {
                username: '黄冠',
                profile_image_url: '/static/images/3.jpg'
            },
            created_at: '1316403559',
            text: '2680 "QQ手机"拿回家'
        },{ sender: {
                username: '陈陈',
                profile_image_url: '/static/images/1.jpg'
            },
            created_at: '1316403559',
            text: '2680 "QQ手机"拿回家'
        }]
    },{
        activity_id: '2222',
     
        owner: {
            profile_image_url: "/static/images/2.jpg",
            username: "陈陈",
        },
        
        //todo:
        is_mine: false,
        is_confirmed: false,

        text: "王昊老婆跑了！ 大家来吃面条！！！",
        plan_at: "1316463559", 
        created_at : "1316413559", 
        detail_text: "穷背景男孩和有钱家女孩无法相爱,在告别前他拜托她最后一件事,陪在他身边一盒烟时间,她答应了.于是他一边抽烟,一边回忆和她相爱的时光.后来他成了香烟厂老板,做出装有过滤嘴的香烟,香烟的名字“mar...",
        
        status: 'past', 
        visibility: 'public',
        
        participators: [{
            username: '陈陈',
            profile_image_url: '/images/2.jpg',
            status: 'Confirmed'                            
        },
        {
            username: '黄冠',
            profile_image_url: '/images/3.jpg',
            status: 'Confirmed'                            
        }],
        
        comments: [{
            sender: {
                username: '黄冠',
                profile_image_url: '/images/3.jpg'
            },
            created_at: '1316403559',
            text: '2680 "QQ手机"拿回家'
        }]
    }]
    //以搞定的活动 列表
    _finishedActivityList =  [{
        activity_id: '333',
     
        owner: {
            profile_image_url: "/images/2.jpg",
            username: "陈陈",
        },
        
        //todo:
        is_mine: false,
        is_confirmed: false,

        text: "JS SDK for OAuth2.0是线上JS SDK的升级版本",
        plan_at: "1316463559", 
        created_at : "1316413559", 
        detail_text: "穷背景男孩和有钱家女孩无法相爱,在告别前他拜托她最后一件事,陪在他身边一盒烟时间,她答应了.于是他一边抽烟,一边回忆和她相爱的时光.后来他成了香烟厂老板,做出装有过滤嘴的香烟,香烟的名字“mar...",
        
        status: 'Ongoing', 
        visibility: 'Open',
        
        participators: [{
            username: '陈陈',
            profile_image_url: '/images/2.jpg',
            status: 'Confirmed'                            
        },
        {
            username: '黄冠',
            profile_image_url: '/images/3.jpg',
            status: 'Confirmed'                            
        }],
        
        comments: [{
            sender: {
                username: '黄冠',
                profile_image_url: '/images/3.jpg'
            },
            created_at: '1316403559',
            text: '2680 "QQ手机"拿回家'
        }]
    }]


    
    //返回数据
    var mock_data = {
        hasPemission: _mock_hasPemission,
        invitations: _mock_user_invitation,
        notifications: _mock_user_latest,
        userCard: _userCard,
        ongoingActivityList: _ongoingActivityList,
        finishedActivityList: _finishedActivityList
    };

   

    return mock_data;
});
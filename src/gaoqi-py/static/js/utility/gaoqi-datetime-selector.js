﻿define(function (require, exports, module) {
    require('utility/datetime');
    var currentDay = new Date();

    var source = {
        monthList: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月']
        , dayList: ['日', '一', '二', '三', '四', '五', '六']
        , ampmText: ['上午', '下午']
        , hourText: '小时'
        , minText: '分钟'
    }



    var selectDateTime = {
        _datetime: currentDay.WeekendTime(),
        _diffDom: null,
        _dateonly: true,


//        publick methods
        initDateTime:function(datetime){
            this._datetime =  datetime;
        },
        initSelector: function (dateDom, timeDom, diffDom) {
            if(timeDom){
                this._dateonly = false;
                this._initTime(timeDom);
            }

            if(dateDom)
                this._initDate(dateDom);

            if(diffDom)
                this._diffDom = diffDom;

        },
        setDateDiff: function () {
            if(this._dateonly)
                $(this._diffDom).html(this._datetime.toDayOfWeek() + " " + " " + this._datetime.OnlyDateDiff());
            else
                $(this._diffDom).html(this._datetime.toDayOfWeek() + " " + " " + this._datetime.DateDiff());
        },
        getDateTime: function () {
            return this._datetime;
        },

//        pravite methods
        _initDate: function (dateDom) {
            var $dom = $(dateDom);
            var self = this;

            $dom
                .val(self._datetime.toFullDate())
                .datepicker({
                    dayNamesMin: source.dayList,
                    monthNames: source.monthList,
                    
                    minDate: currentDay,

                    onSelect: function (dateText, inst) {
                        var date = $dom.datepicker('getDate');

                        self._datetime = new Date(
                            self._datetime.getFullYear(),
                            date.getMonth(),
                            date.getDate(),
                            self._datetime.getHours(),
                            self._datetime.getMinutes()
                        );

                        $dom.val(date.toFullDate());

                        self.setDateDiff();
                    }
                });
        },
        _initTime: function (timeDome) {
            var $dom = $(timeDome);
            var self = this;

            $dom
                .val(self._datetime.toShortTime())
                .timepicker({
                    hourText: source.hourText, // Display text for hours section
                    minuteText: source.minText, // Display text for minutes link
                    amPmText: source.ampmText, // Display text for AM PM

                    minutes: {
                        starts: 0,                // First displayed minute
                        ends: 50,                 // Last displayed minute
                        interval: 10               // Interval of displayed minutes
                    },
                    onSelect: function () {
                        var hour = $dom.timepicker('getHour');
                        var minute = $dom.timepicker('getMinute');

                        self._datetime = new Date(
                            self._datetime.getFullYear(),
                            self._datetime.getMonth(),
                            self._datetime.getDate(),
                            hour,
                            minute
                        );
                        var minuteStr = minute == 0 ? '00' : minute;

                        $dom.val(hour + ":" + minuteStr);

                        self.setDateDiff();
                    }
                });

        }
    }

    return selectDateTime;
});
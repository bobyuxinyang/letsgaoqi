﻿define(function (require, exports, module) {
    var	escapeable = /["\\\x00-\x1f\x7f-\x9f]/g,
        meta = {
            '\b': '\\b',
            '\t': '\\t',
            '\n': '\\n',
            '\f': '\\f',
            '\r': '\\r',
            '"' : '\\"',
            '\\': '\\\\'
        };

    $.quoteString = function( string ) {
        if ( string.match( escapeable ) ) {
                return '"' + string.replace( escapeable, function( a ) {
                        var c = meta[a];
                        if ( typeof c === 'string' ) {
                                return c;
                        }
                        c = a.charCodeAt();
                        return '\\u00' + Math.floor(c / 16).toString(16) + (c % 16).toString(16);
                }) + '"';
        }
        return '"' + string + '"';
    };

    var _toJSON = function (o) {
        if (typeof (JSON) == 'object' && JSON.stringify)
            return JSON.stringify(o);

        var type = typeof (o);

        if (o === null)
            return "null";

        if (type == "undefined")
            return undefined;

        if (type == "number" || type == "boolean")
            return o + "";

        if (type == "string")
            return $.quoteString(o);

        if (type == 'object') {
            if (typeof o.toJSON == "function")
                return _toJSON(o.toJSON());
            if (o.constructor === Date)
                return o.toEpoch();
            if (o.constructor === Array) {
                var ret = [];
                for (var i = 0; i < o.length; i++)
                    ret.push(_toJSON(o[i]) || "null");

                return "[" + ret.join(",") + "]";
            }

            var pairs = [];
            for (var k in o) {
                var name;
                var type = typeof k;

                if (type == "number")
                    name = '"' + k + '"';
                else if (type == "string")
                    name = $.quoteString(k);
                else
                    continue;  //skip non-string or number keys

                if (typeof o[k] == "function")
                    continue;  //skip pairs where the value is a function.

                var val = _toJSON(o[k]);

                pairs.push(name + ":" + val);
            }

            return "{" + pairs.join(", ") + "}";
        }
    };


    var _ToObject = function (obj) {
        return eval('(' + obj + ')');
    };

    var _AsyncMethod = function (url, methodType) {
        return function returnMethod(callback, args, beforeCallback, afterCallback) {
            if (beforeCallback) beforeCallback();

            if (methodType && methodType.toLowerCase() == 'post')
                $.post(
                    url,
                    args,
                     function (e) {
                         callback(e);
                         if (afterCallback) afterCallback();
                     });

            else
                $.getJSON(
                        url,
                        args,
                        function (e) {
                            callback(e);
                            if (afterCallback) afterCallback();
                        });
        };
    };
    var _SyncMethod = function (url, methodType) {
        return function returnMethod(callback, args, beforeCallback, afterCallback) {
            if (beforeCallback) beforeCallback();

            if (methodType && methodType.toLowerCase() == 'post') {
                $.ajax({
                    url: url,   //接收页面
                    type: 'post',      //POST方式发送数据
                    async: false,      //ajax同步
                    data: args,
                    dataType: 'json',
                    success: function (e) {
                        callback(e);
                        if (afterCallback) afterCallback();
                    }
                });
            } else {
                $.ajax({
                    url: url,
                    type: 'get',
                    async: false,
                    data: args,
                    dataType: 'json',
                    success: function (e) {
                        callback(e);
                        if (afterCallback) afterCallback();
                    }
                });
            }
        };
    };




    return {
        toJSON: _toJSON,
        AsyncMethod: _AsyncMethod,
        SyncMethod: _SyncMethod
     
    };

});
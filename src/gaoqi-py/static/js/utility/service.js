﻿
/*
Service: 适用于管理所有服务器端的Rest接口的静态对象
serviceList: 包含了Rest接口的地址，方法，名称
InitService: 该方法会为每一个serviceItem生成4个方法
example:
Item: { name: "GetCurrentUser", url: "/core/GetCurrentUser", method: "get" },
Method: 
异步调用
GetCurrentUser
同步调用
GetCurrentUser_Sync
异步调用，使用缓存
GetCurrentUser_Cache
同步调用，使用缓存
GetCurrentUser_CacheSync

memo: 带缓存的方法只有当Rest的Method是Get的时候才能发生
*/

define(function (require, exports, module) {
    var utility = require('utility/utility');

    var service;
    service = GetService();
    module.exports = service;

    function GetService() {
        var Service = {};
        Service.Cache = {};
        Service.serviceList = [
              { name: 'signin', url: "/user/signin", method: "post" }
            , { name: 'signout', url: "/user/signout", method: "post" }
            , { name: 'connect_sinaweibo', url: "/connect/sinaweibo", method: "post" }
            , { name: 'reg_user', url: '/signup/reg_user', method: 'post' }
            , { name: 'get_active_mail_info_Sycn', url: '/signup/active_mail_info', method: 'get'}


            , { name: 'get_current_user', url: '/user/current_user', method: 'get' }
            , { name: 'update_activity', url: '/activities/update', method: 'post' }
            , { name: 'get_user_now', url: '/activities/user_now', method: 'get' }
            , { name: 'get_user_before', url: '/activities/user_before', method: 'get' }
            , { name: 'get_activity_detail', url: '/activities/activity_detail', method: 'get' }
            , { name: 'get_simple_activity', url: '/activities/simple_activity', method: 'get' }
            
            , { name: 'update_detail_text', url: '/activities/update_detail_text', method: 'post' }
            , { name: 'update_text', url: '/activities/update_text', method: 'post' }
            , { name: 'update_plan_at', url: '/activities/update_plan_at', method: 'post' }
            , { name: 'new_comment', url: '/activities/new_comment', method: 'post' }
            , { name: 'invite_more', url: '/activities/invite_more', method: 'post' }
            , { name: 'new_comment_reply', url: '/activities/new_comment_reply', method: 'post' }
            , { name: 'get_comment_replies', url: '/activities/get_comment_replies', method: 'post' }
            



            , { name: 'confirm', url: '/activities/confirm', method: 'post' }
            , { name: 'interest', url: '/activities/interest', method: 'post' }
            , { name: 'quit', url: '/activities/quit', method: 'post' }

            , { name: 'has_before_activity', url: '/activities/has_before_activity', method: 'post'}

            , { name: 'find_sinaweibo_friends', url: '/connect/sinaweibo/find_friends', method: 'post'}
            
            , { name: 'email_chk', url: '/signup/email_chk', method: 'post'}
            , { name: 'get_sinaweibo_userinfo', url: '/connect/sinaweibo/userinfo', method: 'get'}

            , { name: 'get_invitations', url: '/notification/invitations', method: 'get'}

            , { name: 'get_all_activity_tags', url: '/all_tags', method: 'get'}
            , { name: 'update_user_activity_tags', url: '/user/choose_tags', method: 'post' }
            , { name: 'get_my_activity_tags', url: '/user/my_tags', method: 'get'}
            , { name: 'choose_one_tag', url: '/user/choose_one_tag', method: 'post'}

            , { name: 'get_friends_tags', url: '/user/friends_tags', method: 'get'}


            /* todo:
            发送用户选择tags的微博 send_tags_weibo
            post方法
            input：
                {weibo_content： weiboContent}
             */
            , { name: 'send_tags_weibo', url: '/user/send_tags_weibo', method: 'post'}
        ];


        Service.InitService = function () {
            function GetCacheKey(serviceItem, args) {
                var argStr = args ? utility.toJSON(args) : "";
                return serviceItem.name + argStr;
            }
            function GenerateSyncMethod(serviceItem) {
                return function (callback, args, message, beforeCallBack, afterCallBack) {
                    var returnResult;
                    var cacheKey = GetCacheKey(serviceItem, args);
                    utility.SyncMethod(serviceItem.url, serviceItem.method)(function (result) {
                        Service.Cache[cacheKey] = result;
                        returnResult = result;
                    }, args, beforeCallBack, afterCallBack);


                    if (callback && callback != null)
                        callback(returnResult);
                    return returnResult;
                }
            }
            function GenerateAsyncMethod(serviceItem) {
                return function (callback, args, beforeCallBack, afterCallBack) {
                    var cacheKey = GetCacheKey(serviceItem, args);
                    utility.AsyncMethod(serviceItem.url, serviceItem.method)(function (result) {
                        Service.Cache[cacheKey] = result;
                        callback(result);
                    }, args, beforeCallBack, afterCallBack);
                }
            }
            function GenerateCacheSyncMethod(serviceItem) {
                return function (callback, args, message, beforeCallBack, afterCallBack) {
                    var cacheKey = GetCacheKey(serviceItem, args);
                    if (!Service.Cache[cacheKey] || Service.Cache[cacheKey] == null) {
                        Service[serviceItem.name + "_Sync"](function (result) { }, args, message, beforeCallBack, afterCallBack);
                    }
                    if (callback && callback != null) callback(Service.Cache[cacheKey]);
                    return Service.Cache[cacheKey];
                }
            }
            function GenerateCacheMethod(serviceItem) {
                return function (callback, args, beforeCallBack, afterCallBack) {
                    var cacheKey = GetCacheKey(serviceItem, args);
                    if (!Service.Cache[cacheKey] || Service.Cache[cacheKey] == null) {
                        Service[serviceItem.name](function (result) {
                            callback(result);
                        }, args, beforeCallBack, afterCallBack);
                    }
                    else
                        callback(Service.Cache[cacheKey]);
                }
            }

            for (var i = 0; i < Service.serviceList.length; i++) {
                var item = Service.serviceList[i];
                Service[item.name + "_Sync"] = GenerateSyncMethod(item);
                Service[item.name] = GenerateAsyncMethod(item);
                if (item.method == "get") {
                    Service[item.name + "_CacheSync"] = GenerateCacheSyncMethod(item);
                    Service[item.name + "_Cache"] = GenerateCacheMethod(item);

                }
            }
        };

        Service.InitService();
        return Service;
    };

});


define(function(require){
    return {
        getUrlParam: function (key) {
            var locString = String(window.document.location.href);
            return locString.getQueryString(key);
        }
    }
})
﻿// popContent
/////////////////////////////////////////////
define(function (require, exports, module) {
    (function ($, undefined) {
        // define the name of the UI component
        var ui_name = "artTxtCount";


        // initialize object
        var at = $.artTxtCount = {

            // default values
            defaults: {
                max: Number.MAX_VALUE
        },

        setDefaults: function (defaultOptions) {
            this.defaults = $.extend({}, this.defaults, defaultOptions);
        }
    };

    $.fn.artTxtCount = function (options) {

        // method calling
        if (typeof options == 'string') {
            var args = Array.prototype.slice.call(arguments, 1),
			res;
            this.each(function () {
                var data = $.data(this, ui_name);
                if (data) {
                    var meth = data[options];
                    if (meth) {
                        var r = meth.apply(this, args);
                        if (res === undefined) {
                            res = r;
                        }
                    }
                }
            });
            if (res !== undefined) {
                return res;
            }
            return this;
        }

        // initialize options
        options = $.extend({}, at.defaults, options);




        this.each(function () {
            var _self = this;

            $(this).bind('keyup change', artTxtCount);
            
            // initialize callback
            for (var key in options) {
                var value = options[key];
                if (typeof value == 'function') {
                    //remover callbacks binded before
                    $(_self).unbind(key);
                    // bind callbacks
                    $(_self).bind(key, value);
                }
            }


            /* Private Methods
            -----------------------------------------------------------------------------*/
            function artTxtCount() {
                var val = $(this).val().length;
                            
                var disabled = {
                    on: btnOnCallback,
                    off: btnOffCallback
                };
                var count = {
                    among: textAmongCallback,
                    over: textOverCallback
                };

                if (val == 0) {
                    disabled.off();
                    count.among(options.max - val);
                }
                else {
                    if(val <= options.max){
                        disabled.on();
                        count.among(options.max - val);
                    }
                    else{
                        disabled.off();
                        count.over(val - options.max);
                    }
                };
            };
        

            /* Callback Triggers
            -----------------------------------------------------------------------------*/
            function btnOnCallback() {
                $(_self).trigger('btnOnCallback');
            }
            function btnOffCallback() {
                $(_self).trigger('btnOffCallback');
            }
            function textAmongCallback(count) {
                $(_self).trigger('textAmongCallback', count);
            }
            function textOverCallback(count) {
                $(_self).trigger('textOverCallback', count);
            }
            /* Public Methods
            -----------------------------------------------------------------------------*/

            var publicMethods = {
                option: function (name, value) {
                    if (value === undefined) {
                        return options[name];
                    }
                    options[name] = value;
                }
            

                // @other public methods
            };

            $.data(this, ui_name, publicMethods);

        });

        return this;
    };


})(jQuery);

})
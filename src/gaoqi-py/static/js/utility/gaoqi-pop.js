﻿// popContent
/////////////////////////////////////////////
define(function (require, exports, module) {
    (function ($, undefined) {
        // define the name of the UI component
        var ui_name = "invitationContent"

        // initialize object
        var ic = $.popContent = {

            // default values
            defaults: {
        },

        setDefaults: function (defaultOptions) {
            this.defaults = $.extend({}, this.defaults, defaultOptions);
        }
    };

    $.fn.popContent = function (options) {

        // method calling
        if (typeof options == 'string') {
            var args = Array.prototype.slice.call(arguments, 1),
			res;
            this.each(function () {
                var data = $.data(this, ui_name);
                if (data) {
                    var meth = data[options];
                    if (meth) {
                        var r = meth.apply(this, args);
                        if (res === undefined) {
                            res = r;
                        }
                    }
                }
            });
            if (res !== undefined) {
                return res;
            }
            return this;
        }

        // initialize options
        options = $.extend({}, ic.defaults, options);



        //只取前4条 Notification 显示出来
        var count = 3;

        var invitationTmp = '<li class="gaoqi-pop-li" id="${activity_id}">' +
//                            '<div class="pop-info" id="${invitation_id}">' +
                            '<div class="pop-info">' +
                                  '<div class="gaoqi-pop-img">' +
                                      '<img class="img-pop-user" src="${inviter.profile_image_url}" />' +
                                  '</div>' +
                                  '<div class="gaoqi-pop-info" >' +
                                      '<a class="invi-info">${inviter.username}  邀请你参加 "${activity_name}" 活动。</a>' +
                                  '</div>' +
                              '</div>' +
                          '</li>' +
                          '<hr class="gaoqi-pop-hr">';


        var notificationTmp = '<li class="gaoqi-pop-li" id="${activity_id}">' +
                                  '<div class="pop-info"  id="${latest_id}">' +
                                      '<div class="gaoqi-pop-img">' +
                                          '<img src="${sender.profile_image_url}" class="img-pop-user" />' +
                                      '</div>' +
                                      '<div class="gaoqi-pop-info">' +
                                          '<a class="noti-info">${sender.username}  ${content}</a>' +
                                          '<a class="alink-ignore">• 知道了</a>' +
                                      '</div>' +
                                  '</div>' +
                              '</li>' +
                              '<hr class="gaoqi-pop-hr">';


        this.each(function () {
            var _self = this;

            if (options.type == "invitation")
                showinvitations(_self);
            else
                showNotifications(_self);


            // initialize callback
            for (var key in options) {
                var value = options[key];
                if (typeof value == 'function') {
                    //remover callbacks binded before
                    $(_self).unbind(key);
                    // bind callbacks
                    $(_self).bind(key, value);
                }
            }


            /* Private Methods
            -----------------------------------------------------------------------------*/
            function showinvitations(dom) {
                removerContainer();

                var newContent = getContainer(dom)
                    .append('<div class="gaoqi-pop-arow arow-invitation">&nbsp;</div>');

                var tmp = invitationTmp;

                $.template("invitationTemplate", tmp);
                $.tmpl("invitationTemplate", options.data)
                 .appendTo($('.gaoqi-pop-ul'));

                initInvitationEvent();

                newContent.show();
            };


            function showNotifications(dom) {
                removerContainer();

                var newContent = getContainer(dom)
                    .append('<div class="gaoqi-pop-arow arow-noti">&nbsp;</div>');

                var tmp = notificationTmp;
                var data = options.data.slice(0, count);

                $.template("notiTemplate", tmp);
                $.tmpl("notiTemplate", data)
                 .appendTo($('.gaoqi-pop-ul'));

                $(newContent).append('<button class="btn" id="ignoreAll">知道了</button>');

                initNotificationEvent();

                newContent.show();

            };

            function removerContainer() {
                if ($('.gaoqi-pop-container').length > 0)
                    $('.gaoqi-pop-container').remove();
            }

            function getContainer(dom) {
                return $('<div class="gaoqi-pop-container"/>')
                            .append('<ul class="gaoqi-pop-ul">')
                            .appendTo($(dom))
                            .hide()
            }

            function initInvitationEvent() {
                $('.invi-info').click(function () {
                    var acti_id = $(this).parents('.gaoqi-pop-li').attr('id');
                    var invi_id = $(this).parents('.pop-info').attr('id');

                    invitationClickCallback(acti_id, invi_id);

                    return false;
                });
            }

            function initNotificationEvent() {
                $('.noti-info').click(function () {
                    var acti_id = $(this).parents('.gaoqi-pop-li').attr('id');
                    var noti_id = $(this).parents('.pop-info').attr('id');

                    notificationClickCallback(acti_id, noti_id);

                    return false;
                });

                $('.alink-ignore').click(function () {
                    var noti_id = $(this).parents('.pop-info').attr('id');

                    notificationIgnoreCallback(noti_id);

                    if ($('.gaoqi-pop-container li').length == 1)
                        $('.gaoqi-pop-container').remove();

                    $(this).parents('li').next().remove();
                    $(this).parents('li').remove();

                    return false;
                });

                $('#ignoreAll').click(function () {
                    var noti_id_list = [];
                    var pop = $('.gaoqi-pop-container .pop-info');

                    pop.each(function () {
                        noti_id_list.push($(this).attr('id'))
                    })

                    notificationIgnoreCallback(noti_id_list.join(','));

                    $('.gaoqi-pop-container').remove();

                    return false;
                });
            }

            /* Callback Triggers
            -----------------------------------------------------------------------------*/
            function invitationClickCallback(acti_id, invi_id) {
                $(_self).trigger('invitationClickCallback', acti_id, invi_id);
            };

            function notificationClickCallback(acti_id, noti_id) {
                $(_self).trigger('notificationClickCallback', acti_id, noti_id);
            };

            function notificationIgnoreCallback(noti_id_list) {
                $(_self).trigger('notificationIgnoreCallback', noti_id_list);
            };

            /* Public Methods
            -----------------------------------------------------------------------------*/

            var publicMethods = {
                option: function (name, value) {
                    if (value === undefined) {
                        return options[name];
                    }
                    options[name] = value;
                }
//                ,
//                close: function () {
//                    if ($('.gaoqi-pop-container').length > 0)
//                        $('.gaoqi-pop-container').fadeOut().remove();
//                }



                // @other public methods
            };

            $.data(this, ui_name, publicMethods);

        });

        return this;
    };


})(jQuery);

$(function () {
    document.onclick = function (e) {
        var e = e ? e : window.event;
        var tar = e.srcElement || e.target;

        if ($(tar).parents('.nav-remind').length == 0) {
            $('.gaoqi-pop-container').fadeOut().remove();
        }
    };
});
})
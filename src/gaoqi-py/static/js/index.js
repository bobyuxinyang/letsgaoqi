﻿define(function (require) {
    var utility = require('utility/utility');
    var service = require('utility/service');


    require('utility/string');
    require('utility/datetime');

    require('utility/gaoqi-displaystatusinfo');
    require('utility/gaoqi-artTxtCount');
    
    var BegActivityView = require('module/dialog/beg-activity/beg-activity.view');
    var GaoqiActivityView = require('module/dialog/gaoqi-activity/gaoqi-activity.view');

    var Views = {
          participatorListView: null
        , topbarView: null
        , activity_navView: null
        , mineView: null
        , friendsView: null
    };


    function getCurrentUser(){
        window.Current_user = null;
        service.get_current_user_Sync(function (e) {
            window.Current_user = e;
            console.log('my id: ' + window.Current_user['_id']);
        });
    }

    function bindTopbar() {
        var TopbarView = require('view/topbar.view');
        require('model/topbar');

        Views.topbarView = new TopbarView({ model: new Topbar() });
        $('.topbar-index').append(Views.topbarView.render().el);

        if (Views.topbarView.model.get('invitations').length > 0) {
            Views.topbarView.showinvitation();
        }
    }

    function bindMyTags() {
        var MytagsView = require('module/mytags/mytags.view');
        require('module/mytags/mytags');

        Views.mytagsView = new MytagsView( { model: new Mytags() } );
        $('.tips-userinfo').append(Views.mytagsView.el);
    }

    function bindGaoqiBtn() {
        var GaoqiBtnView = require('module/gaoqi-btn/gaoqi-btn.view');
        Views.gaoqiBtnView = new GaoqiBtnView();
        $('.tips-userinfo').append(Views.gaoqiBtnView.el);

    }

    function bindMine(){
        console.log('bind mine');
        
        var MineView = require('module/activity-container/activity-containers.view');
        require('module/activity-container/activity-containers');
        
        Views.mineView = new MineView( {model: new AcitivityContainerList(null, 'mine')} );
        Views.mineView.render();
        $('#mine').html(Views.mineView.el);
    }

    function bindFriends(){
        console.log('bind friends');
        
        var FriendsView = require('module/activity-container/activity-containers.view');
        require('module/activity-container/activity-containers');
        
        Views.friendsView = new FriendsView( {model: new AcitivityContainerList(null, 'friends')} );
        Views.friendsView.render();
        $('#friends').html(Views.friendsView.el);
    }


//    function bindUserCard() {
//        var UserCardView = require('view/usercard.view');
//        require('model/usercard');
//
//        Views.userCardView = new UserCardView({ model: new UserCard() });
//        $('.tips-userinfo').append(Views.userCardView.el);
//    }
//
//    function bindFriendsTags(){
//        var FriendsTagsView = require('module/friends-tags/friends-tags.view');
//        require('module/friends-tags/friends-tags');
//        Views.friendsTagsView = new FriendsTagsView({model: new FriendsTags() });
//        $('.main').html(Views.friendsTagsView.el);
//    }
//
//    function bindMyActivities(){
//        var MyView = require('module/my/my.view');
//        Views.myView = new MyView();
//        $('.main').html(Views.myView.render().el);
//    }

//    function bindNotifications() {
//        console.log('todo: notis');
//    }





    function initEvent() {
        $(document).bind('mytagsChanged', function () {
            Views.mytagsView.refresh();
        });


        //求活动
        $(document).bind('begActivity', function () {
            var begActivityView = new BegActivityView();
            $(begActivityView.render().el)
                .fadeIn();
        });

        //查看活动详细
        $(document).bind('gotoActivity',function(id){
            window.location.href = "/activity?id=" + id;
        });

        //求活动完成刷新自己的列表
        $(document).bind('newRequestedCallback', bindMine);

        //搞起活动
        $(document).bind('gaoqiActivity', function (e, args) {
            var activities = [{
               'activity_id': 1,
               'created_at': new Date().toEpoch() - 80000,
               'text': '咦咦咦咦咦咦咦咦咦咦咦咦',
               'plan_at': new Date().toEpoch() + 30000
            },{
               'activity_id': 2,
               'created_at': new Date().toEpoch() - 100000,
               'text': '啦啦啦啦啦啦',
               'plan_at': new Date().toEpoch() + 60000
            }];

            args.activities = activities;
            
            //把自己参加的活动列表 增添进 args
            //todo: 从服务器获得自己的活动列表
            //args.activities = service.get_user_now_Sync();

            var gaoqiActivityView  = new GaoqiActivityView(args);
            $(gaoqiActivityView.render().el)
                .fadeIn();
        });


//        $(document).bind('friendsTagsChanged', function () {
//            Views.friendsTagsView.refresh();
//        });


        $(window).scroll(function() {
            if($(window).scrollTop() >= 100)
                $('#toolBackTo').fadeIn(300);
            else
                $('#toolBackTo').fadeOut(300);
        });

      }



    function initPage() {
        getCurrentUser();

        bindTopbar();
        bindGaoqiBtn();
        bindMyTags();

        bindMine();
        bindFriends();

//        bindFriendsTags();
//        bindUserCard();
//        bindNotifications();
    }



    $(function () {
        initPage();
        initEvent();
    })

})

define(function (require) {
    var utility = require('utility/utility');
    var service = require('utility/service');
    var url = require('utility/url');
    require('utility/string');


    function initPage(){
        // get activity id
        var activityId = url.getUrlParam('id');

        var SimpleActivityView = require('view/simple-activity.view');
        require('model/simple-activity');

        var simpleActivityView = new SimpleActivityView({
            model: new SimpleActivity(null, activityId)
        });

        $('#simple-activity').append(simpleActivityView.el);


    }

    function initEvent(){
        
    }

    $(function () {
        initPage();
        initEvent();
    });

});

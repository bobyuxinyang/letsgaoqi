define(function (require) {
    var utility = require('utility/utility');
    var url = require('utility/url');
    require('utility/string');
    var service = require('utility/service');

    var Views = [];

    function updateBtnSubmitStatus() {
        var len = Views.tagSelView.getChoosedTags().length
        if (len == 0) {
            $('#btn-submit').addClass('disabled');
        }else{
            $('#btn-submit').removeClass('disabled');
        }
    }


    function initPage() {
        var TagSelView = require('module/tag-sel/tag-sel.view');
        require('module/tag-sel/activity-tags');
        Views.tagSelView = new TagSelView( {model: new activity_tags()} );

        Views.tagSelView.chooseTagsChangedCallback = function (tags) {
            if (tags.length == 0) {
                str = '⋯⋯'
            } else {
                var str = ' ';
                $(tags).each(function (index, item) {
                    str += item.name + '，';
                });
                str = str.substring(0, str.length - 1);
            }
            $('#content').find('h3').find('.strong').html(str);

            updateBtnSubmitStatus();
        };

        $('#tag-selector').html(Views.tagSelView.el);

        updateBtnSubmitStatus();
        
    }

    
    function initEvent(){
        $('#btn-submit').click(function (){
            var choosedTags = Views.tagSelView.getChoosedTags();
            if (choosedTags.length == 0) return;

            //发微博
            var isSinaWeiboChecked = $('#check-send').is(':checked');
            var weiboContent = $('#weibo-content').html().replace(/<\/?.+?>/g,"");
            if(isSinaWeiboChecked)
                service.send_tags_weibo(function () {
                }, { weibo_content : weiboContent})

            //提交tags
            service.update_user_activity_tags(function () {
                window.location = '/';
            }, { tags : JSON.stringify(choosedTags)})
        });
    }

    $(function () {
        initPage();
        initEvent();
    });

});

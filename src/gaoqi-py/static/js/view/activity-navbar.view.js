define(function (require) {
    var service = require('utility/service');
    var btn_tabs = {};

    return Backbone.View.extend({
        //... is a list tag.
        tagName: "ul",
        template: _.template($("#tpl-activity-navbar").html()),
        initialize: function () {
            $(this.el).prop('id', 'tab-time');
            _.bindAll(this, 'render');
        },
        render: function () {
            var $self = this;
            $(this.el).html(this.template());

            var has_before = service.has_before_activity_Sync();
            if (!has_before) {
                $(this.el).find('#tab-before').hide();
            }

            $(this.el).find('#tab-now').click(function () {
                btn_tabs['before'].removeClass('current');
                btn_tabs['now'].addClass('current');
                $(document).trigger('now');
            });
            $(this.el).find('#tab-before').click(function () {
                btn_tabs['now'].removeClass('current');
                btn_tabs['before'].addClass('current');
                $(document).trigger('before');
            });

            btn_tabs['before'] = $(this.el).find('#tab-before');
            btn_tabs['now'] = $(this.el).find('#tab-now');
            return this;
        }
    });
});
﻿define(function (require) {
    var service = require('utility/service');
    
    var ParticipatorListView = require('view/participatorlist.view');
    var ConfirmedParticipatorListView = require('view/confirmedParticipatorlist.view');
    var MessageListView = require('module/comment/comment-list.view');
    var StatusView = require('view/status.view');

    var TitleEditView = require('module/dialog/titleedit.view');
    var DetailEditView = require('module/dialog/detailedit.view');
    var TimeEditView = require('module/dialog/timeedit.view');
    var InviteFriendsView = require('module/dialog/invite-friends/usersel.view');
    
    var OverlayView = require('module/dialog/overlay.view');

    require('model/participatorlist');
    require('model/confirmedParticipatorlist');
    require('module/comment/comment-list');
    require('model/status');
    
    require('utility/datetime');
    require('utility/string');
    require('utility/gaoqi-artTxtCount');

    var current_user;
    service.get_current_user_Sync(function (e) {
        current_user = e;
    });
    
    return Backbone.View.extend({
        tagName: "div",
        template: _.template($("#activity-detail").html()),
        events: {
            "mouseover .activity-lab": "showEdit",
            "mouseout .activity-lab": "hideEdit",

            "click #edit-title": "editTitle",
            "click #edit-time": "editTime",
            "click #edit-detail": "editDetail",
            "click #addDetail": "editDetail",
            "click .activity-invite": 'editInviteFriends',

            "click #btn-confirm": "confirm",
            "click #link-confirm": "confirm",
            "click #btn-maybe": "interest",
            "click #link-quit": "exit",
            

            "click #btn-comment": "comment"
        },

        statusView: null,
        confirmedParticipatorListView: null,
        participatorListView: null,
        messageListView: null,
        overlayView: null,

        checkMytatus: function (status) {
            var $self = this;

//            status = 'making-decision';

            switch(status){
                case "in-activity":
                    $self.changeToConfirmedStatus();

                    break;
                case "making-decision":
                    $self.changeToInterestedStatus();

                    break;
                case "no-reply":
                    $self.changeToNoReplyStatus();
                    
                    break;
            }
        },

        checkIsDetailEmpty: function (detail) {
            if (detail.htmlEncode().trim().length == 0)
                        $(this.el).find('#addDetail').show();
                   else
                       $(this.el).find('#addDetail').hide();

        },



        renderStatus: function (status, visibility) {
            this.statusView = new StatusView({
                model: new Status({
                    status: status
                    , visibility: visibility
                })
            });

            this.statusView.render();
            //$(this.el).find('.activity-status').html(this.statusView.el);
        },

        renderParticipators: function(participators){
            var confirmedParticipators = _.filter(participators,function(item){
                return item.status == "in-activity";
            });

            var unconfirmedParticipators = _.reject(participators, function(item){
                return item.status == "in-activity";
            });
            

            this.renderConfirmedParticipators(confirmedParticipators);
            this.renderUnconfirmedParticipators(unconfirmedParticipators );
            this.renderTips();
        },

        renderConfirmedParticipators: function (participators) {
            this.confirmedParticipatorListView = new ConfirmedParticipatorListView({
                model: new ConfirmedParticipatorList(participators)
            });

            this.confirmedParticipatorListView.render();
            $(this.el)
                .find('#confirmedParticipatorList')
                .html(this.confirmedParticipatorListView.el);
        },

        renderUnconfirmedParticipators: function (participators) {
            this.participatorListView = new ParticipatorListView({
                model: new ParticipatorList(participators)
            });

            this.participatorListView.render();
            $(this.el)
                .find('#unconfirmedParticipatorList')
                .html(this.participatorListView.el);
        },

         renderTips:function(){
            var _self = this;

            $(_self.el).find('.icon').each(function(index) {
                var user_id = $(this).data('user_id');
                var activity_id = _self.model.get('_id');

                Tipped.create(
                    this
                    , '/user/tip'
                    , {
                        ajax: {
                            data: { activity_id: activity_id, user_id: user_id }
                            , type: 'post'
                        }
                        , hook: 'bottomleft'
                        , skin: 'light'
                    }
                );
            });
          },


        renderMessages: function (messages) {
            var $self = this;
            $self.messageListView = new MessageListView({
                model: new MessageList( messages )
            });

            $self.messageListView.render();
            $($self.el)
                .find('#feedList')
                .html($self.messageListView.el);

            //when the replies of one comment changed
            $self.messageListView.model.bind('new-reply', function(reply, comment_id){
                $self.model.changeComments(comment_id);
            });
        },


        renderOverlay: function(){
            var $self = this;
            $self.overlayView.render();
        },


        render: function () {
            var $self = this;

            $($self.el).html(this.template($self.model.toJSON()));

            $($self.el).find('#newComment').artTxtCount({
                btnOnCallback: function () {
                    $($self.el).find('#btn-comment').attr('disabled', false);
                },
                btnOffCallback: function () {
                    $($self.el).find('#btn-comment').attr('disabled', true);
                }
            });

            $self.overlayView = new OverlayView();

            $self.checkMytatus($self.model.get('my-status'));
            $self.checkIsDetailEmpty($self.model.get('detail_text'));

//            $self.renderStatus($self.model.get('status'), $self.model.get('visibility'));
            $self.renderParticipators($self.model.get('participators'));
            $self.renderMessages($self.model.get('comments'));

            return $self;
        },


        initialize: function (arg) {
            _.bindAll(this, 'render', 'renderMessages', 'renderParticipators');
            
            $(this.el).addClass('activity-content');

            this.model.bind('getAll', this.render);
            this.model.bind('invitedFriends', this.renderParticipators);
            this.model.bind('user-confirmed', this.renderParticipators);
            this.model.bind('user-interested', this.renderParticipators);
            this.model.bind('new_message', this.renderMessages)

            this.model.getAll();
        },






        showEdit: function (event) {
            var $self = $(event.currentTarget);
            $self.find('.icon-edit').css({ display: 'inline-block' });
        },
        hideEdit: function (event) {
            var $self = $(event.currentTarget);
            $self.find('.icon-edit').hide();
        },
        
        
        editTitle: function () {
            var $self = this;
            $self.renderOverlay();

            var titleEditView = new TitleEditView();
            titleEditView.editedCallback = function(title){
                $self.model.editTitle(title);
                $('#activityTitile').html(title.htmlEncode());
            }

            $(titleEditView.render($self.model.get('text')).el)
                .appendTo('body')
                .fadeIn()
                .find('textarea')
                .select();
        },
        editTime: function () {
            var $self = this;
            $self.renderOverlay();

            var timeEditView = new TimeEditView();
            timeEditView.editedCallback = function(starTime){
                var timeStr = starTime.toFullDateTimeAndDay() + "  " + starTime.DateDiff()
                $self.model.editTime(starTime.toEpoch(), timeStr);
                $('#activityTime').html(timeStr.htmlEncode());
            }

            $(timeEditView.render($self.model.get('plan_at')).el)
                .appendTo('body')
                .fadeIn();
        },
        editDetail: function () {
            var $self = this;
            $self.renderOverlay();

            var detailEditView = new DetailEditView();
            detailEditView.editedCallback = function(detail){
                $self.model.editDetail(detail);
                $self.checkIsDetailEmpty(detail);

                $('#activityDetail').html(detail.changeLineToBr());
            }

            $(detailEditView.render($self.model.get('detail_text')).el)
                .appendTo('body')
                .fadeIn()
                .find('textarea')
                .select();
        },

        editInviteFriends: function () {
            var $self = this;
            $self.renderOverlay();

            var inviteFriendsView = new InviteFriendsView();
            $(inviteFriendsView.render($self.model.toJSON()).el)
                .appendTo('body')
                .fadeIn();

            inviteFriendsView.inviteFriendsCallback = function (weibo_users, isSendWeiboInvitation, inviteText) {
                $self.sinweiboSelectorCallback(weibo_users, isSendWeiboInvitation, inviteText);
            };
            $('#txt-sinaweibo-search').focus();
        },


        confirm: function () {
            var $self = this;

            $self.model.confirm();
            $self.changeToConfirmedStatus();
        },

        interest: function(){
            var $self  = this;

            $self.model.interest();
            $self.changeToInterestedStatus();
        },

        exit: function () {
            var $self = this;

            $( "#dialog-confirm" ).dialog({
			    resizable: false,
                height:'auto',
                modal: true,
                buttons: {
                    "确定": function() {
                        $(this).dialog( "close" );

                        $self.model.quit();
                    },
                    "取消": function() {
                        $( this ).dialog( "close" );
                    }
                }
		    });
        },
        
        comment: function () {
            var self = this;
            var newComment = {
                sender: {
                    sender_id: current_user['_id'],
                    username: current_user['username'],
                    profile_image_url:current_user['profile_image_url']
                },
                created_at: new Date().toEpoch(),
                text: $('#newComment').val().htmlEncode(),
                replies_count: 0
            };

            self.model.comment(newComment);

            $('#newComment').val('');
            $('#btn-comment').attr('disabled', true);
        },


        sinweiboSelectorCallback:function(weibo_users, isSendWeiboInvitation, inviteText) {
            var $self = this;
            var list = _.map(weibo_users, function (weibo_user) {
                return { user: weibo_user
                       , status: 'no-reply'
                }
            });

            $self.model.addParticipators(list, isSendWeiboInvitation, inviteText);
        },


        /* btn status */
        changeToConfirmedStatus: function(){
            var $self = this;
            $($self.el).find('.active-btn').remove();
            $($self.el).find('#beSure').show();
            $($self.el).find('#maybe').hide();
            $($self.el).find('#link-quit').show();
            $($self.el).find('#link-confirm').hide();
        },
        changeToInterestedStatus: function(){
            var $self = this;
            $($self.el).find('.active-btn').remove();
            $($self.el).find('#maybe').show();
            $($self.el).find('#beSure').hide();
            $($self.el).find('#link-confirm').show();
            $($self.el).find('#link-quit').show();
        },
        changeToNoReplyStatus: function(){
            var $self = this;
            $($self.el).find('.active-btn').show();
            $($self.el).find('#link-quit').show();
        }
    });
});
﻿define(function (require) {
    require('utility/gaoqi-pop');

    return Backbone.View.extend({
        //... is a list tag.
        tagName: "div",
        template: _.template($("#tpl-topbar").html()),
        events: {
//            "click #home": 'changePageToHome',
//            "click #my": 'changePageToMy',
            "click #logo": 'changePageToHome',
            
            "click #txt-invitation": 'showinvitation',
            "click #invite-count": 'showinvitation',
//            "click #txt-notification": 'showNotification',
            "click #txt-exit": 'signout'
        },
        initialize: function () {
            $(this.el).addClass('container');

            _.bindAll(this, 'render');
            this.model.bind('getAll', this.render);
            this.model.getAll();
            
        },
        render: function () {
            var $self = this;
            $($self.el).html($self.template($self.model.toJSON()));

            if ($self.model.get('invitations').length == 0) {
                $($self.el).find('#invite-count').hide();
                $($self.el).find('#txt-invitation').prop('title', '还没有来自朋友的活动邀请');
            }
            
//            if ($self.model.get('notifications').length == 0) {
//                $($self.el).find('#invite-count').hide();
//            }

            return this;
        },

        changeLiStatus: function(li){
            $(li).parents('ul').find('li').removeClass('active');
            $(li).addClass('active');
        },

        changePageToHome: function (e) {
            this.changeLiStatus(e.currentTarget);
            $(document).trigger('go-to-home');
        },
        
        showinvitation: function () {
            var $self = this;
            var invitations = $self.model.get('invitations');

            if(invitations.length == 0) return;

            $($self.el).find('.nav-remind').popContent({
                data: invitations,
                type: 'invitation',
                invitationClickCallback: function (e, acti_id, invi_id) {
                    $self.model.checkInvitation(invi_id);
                    $self.changePageToDetail(acti_id);
                }
            });
        },

        
//        changePageToMy: function (e) {
//            this.changeLiStatus(e.currentTarget);
//            $(document).trigger('go-to-my');
//        },

//        changePageToDetail: function(id){
//            window.location.href = "/activity?id=" + id;
//        },

//        reduceNotiCount: function(count){
//            var dom  = $(this.el).find('#remind-count');
//            var notiCount = dom.html();
//            notiCount = notiCount - count < 0? 0 : notiCount - count;
//
//            if(notiCount== 0)
//                dom.hide();
//
//            dom.html(notiCount);
//        },



//        showNotification: function () {
//            var $self = this;
//            var notifications = $self.model.get('notifications');
//
//            if(notifications.length == 0) return;
//
//            $('.nav-remind').popContent({
//                data: notifications,
//                type: 'notification',
//                notificationClickCallback: function (e, acti_id, noti_id) {
//                    $self.model.ignoreNotification(noti_id);
//                    $self.changePageToDetail(acti_id);
//                },
//
//                notificationIgnoreCallback: function (e, noti_id_list) {
//                    var list = noti_id_list.split(',');
//                    var length = list.length;
//
//                    $self.reduceNotiCount(length);
//                    $self.model.ignoreNotification(list);
//                }
//            });
//        },

        signout: function () {
            this.model.signout();
        },
        
        remove: function () {
            $(this.el).remove();
        },
        clear: function () {

        }
    });
});
﻿define(function (require) {
    var utility = require('utility/utility.js');
    var service = require('utility/service');

    require('model/usercard');

    return Backbone.View.extend({
        tagName: "div",
        template: _.template($("#tpl-user-card").html()),
        events: {
        },
        render: function () {
            $(this.el).html(this.template(this.model.toJSON()));

            return this;
        },
        initialize: function (arg) {
            _.bindAll(this, 'render');

            $(this.el).addClass('btn-holer');
            this.model.bind('getAll', this.render);
            this.model.getAll();
        }
    });

});
﻿define(function (require) {
    return Backbone.View.extend({
        //... is a list tag.
        tagName: "div",
        template: _.template($("#activity-confirmed-participator").html()),
        events: {
    },
    initialize: function () {
        $(this.el).addClass('avatar_grid_confirmed');
        
        _.bindAll(this, 'render');
        this.model.bind('change', this.render);
        this.model.view = this;
    },
    render: function () {
        $(this.el).html(this.template(this.model.toJSON()));
        $(this.el).find('.icon').data('user_id', this.model.get('user_id'));
        
        return this;
    },
    remove: function () {
        $(this.el).remove();
    },
    clear: function () {

    }
});
});
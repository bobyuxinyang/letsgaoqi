﻿define(function (require) {
    require('model/status');

    return Backbone.View.extend({
        tagName: "div",
        template: _.template($("#activity-status").html()),
        events: {
            'mouseenter #activityOpenInfo': 'showOpenInfo',
            'mouseenter #activityStatusInfo': 'showStatusInfo',

            'mouseleave #activityOpenInfo': 'hideOpenConbox',
            'mouseleave #activityStatusInfo': 'hideStausConbox',

            'click #changeAvailability': 'changeAvailability',
            'click #changeVisibily': 'changeVisibily'
        },
        render: function () {
            $(this.el).html(this.template(this.model.toJSON()));

            return this;
        },

        changeAvailability: function () {
            this.model.changeAvailability();
            this.render();
        },
        changeVisibily: function () {
            this.model.changeVisibility();
            this.render();
        },

        delay: 800,
        timer: null,

        showOpenInfo: function () {
            var self = this;

            self.timer = setTimeout(function () {
                $(self.el).find('#activityOpenInfo').addClass('status-hover');
                $(self.el).find('#OpenConbox').show();
            }, self.delay);
        },

        showStatusInfo: function () {
            var self = this;

            self.timer = setTimeout(function () {
                $(self.el).find('#activityStatusInfo').addClass('status-hover');
                $(self.el).find('#StatusConbox').show();
            }, self.delay);

        },

        hideOpenConbox: function () {
            var self = this;

            $(self.el).find('#activityOpenInfo').removeClass('status-hover');
            $(self.el).find('#OpenConbox').hide();

            if (self.timer)
                clearTimeout(self.timer);
        },

        hideStausConbox: function () {
            var self = this;

            $(self.el).find('#activityStatusInfo').removeClass('status-hover');
            $(self.el).find('#StatusConbox').hide();
            
            if (self.timer)
                clearTimeout(self.timer);
        },

        initialize: function (arg) {
            _.bindAll(this, 'render');
            this.model.bind(this.render);
        }
    });

});
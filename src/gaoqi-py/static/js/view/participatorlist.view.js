﻿define(function (require) {
    var ParticipatorView = require('view/participator.view');

    return Backbone.View.extend({
        tagName: "div",
        events: {

    },

    initialize: function () {
        $(this.el).addClass('participators-list');
        
        _.bindAll(this, 'addOne', 'render');
        this.model.bind('getAll', this.render);
    },
    render: function () {
        this.model.each(this.addOne);
        return this;
    },
    addOne: function (e) {
        var view = new ParticipatorView({ model: e });
        var element = view.render().el;
        $(this.el).append(element);
    },

    clear: function (e) {

    }

});

});
﻿define(function (require) {
    var utility = require('utility/utility');
    var url = require('utility/url');
    require('utility/string');
    
    function bindTopbar() {
        var TopbarView = require('view/topbar.view');
        require('model/topbar');

        var topbarView = new TopbarView({ model: new Topbar() });
        topbarView.render();
        $('.topbar').append(topbarView.el);

        $(document).bind('go-to-home', function () {
            window.location = '/';
        });
    }


    function bindActivity(id) {
        var ActivityDetailView = require('view/activitydetail.view');
        require('model/activitydetail');

        var activityDetailView = new ActivityDetailView({
            model: new ActivityDetail(null, id)
        });

        $('#activity-container').append(activityDetailView.el);
    }


    function initPage() {
        var id = url.getUrlParam('id');

        bindTopbar();
        bindActivity(id);
    }

    function initEvent(){
        $(window).scroll(function() {
            if($(window).scrollTop() >= 100)
                $('#toolBackTo').fadeIn(300);
            else
                $('#toolBackTo').fadeOut(300);
        });
    };

    $(function () {
        initPage();
        initEvent();
    });

});

﻿define(function (require) {

    var service = require('utility/service');

    function signin(){
        var email = $('#txt-email').val();
        var pwd = $('#txt-pwd').val();

        service.signin(function (e) {
            if (e.Status == "Success") {
                window.location = "/";
            } else {
                alert('登录失败');
            }
        }, { email: email, password: pwd });
    }
    
    $(function () {
        $('#txt-pwd').keydown(function(e){
            if(e.keyCode == '13')
                signin();
        });

        $('#btn-signin').click(function () {
            signin();
        });

    })
});

define(function (require) {
    var ActivityTagView = require('module/tag-sel/activity-tag.view');
    return Backbone.View.extend({
        tagName: "div",
        template: '<ul class="taglist clearfix"></ul>',
        events: {
        },
        initialize: function () {
            var $self = this;
            $(this.el).prop('class','tag-sel');

            _.bindAll(this, 'addOne', 'render', 'chooseTagsChanged', 'getChoosedTags');
            this.model.bind('getAll', this.render);
            this.model.bind('choosed-tags-changed', this.chooseTagsChanged)
        },
        render: function () {
            $(this.el).html(this.template);

            this.model.each(this.addOne);
            return this;
        },
        addOne: function (e) {
            _.each(this.getChoosedTags(), function (item) {
                if (item.name == e.get('name')){
                    e.set({isChoosed: true});
                }
            });
            var activityTagView = new ActivityTagView({ model: e});
            $(this.el).find('.taglist').append(activityTagView.render().el);
        },

        getChoosedTags: function () {
            return this.model.getChoosedTags();
        },

        chooseTagsChanged: function (tags) {
            this.chooseTagsChangedCallback(tags);
        },
        setChoosedTags: function (models) {
            this.model.setChoosedTags(models)
        },

        chooseTagsChangedCallback: function (tags){
            
        }
    });
    
});
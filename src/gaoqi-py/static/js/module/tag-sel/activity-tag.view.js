define(function (require) {

    return Backbone.View.extend({
        tagName: "li",
        events: {
            'click a': 'chooseTag'
        },

        initialize: function () {
            _.bindAll(this, 'render', 'chooseTag', 'chooseStatusChanged');
            this.model.bind('choose-status-changed', this.chooseStatusChanged);
        },
        render: function () {
            var $aTag = $('<a/>');
            $aTag.html(this.model.get('name'));
            $(this.el).append($aTag);

            if (this.model.get('isChoosed')) {
                this.chooseStatusChanged(this, true);
            }
            return this;
        },

        chooseTag: function () {
            this.model.toggleChoose();
        },

        chooseStatusChanged: function (model, isChoosed) {
            if (isChoosed){
                $(this.el).addClass('selected');
            }else{
                $(this.el).removeClass('selected');
            }
        }
    });
});
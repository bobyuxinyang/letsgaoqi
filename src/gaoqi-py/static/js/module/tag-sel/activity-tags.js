define(function(require){
    var service = require('utility/service');
    require('module/tag-sel/activity-tag');
    
    window.activity_tags = Backbone.Collection.extend({
        model: activity_tag,

        choosedTags: [],

        initialize: function () {
            this.getAll();
            this.bind('choose-status-changed', this.chooseStatusChanged);
        },
        comparator: function (e) {
            return e.get("index");
        },
        getAll: function () {
            var $self = this;
            service.get_all_activity_tags(function (e) {
                $self._reset();
                $self.add(e);
                $self.trigger('getAll');
            });
        },
        chooseStatusChanged: function(model, isChoosed) {
            var $self = this;
            if (isChoosed) {
                this.choosedTags.push(model.attributes);
                var len = this.choosedTags.length;
                this.choosedTags[len - 1].order = len;
            }else{
                var i = 0;
                _.each(this.choosedTags, function (item) {
                    if (item.name == model.attributes.name) {
                        i = _.indexOf($self.choosedTags, item);
                    }
                });
                this.choosedTags.splice(i, 1);
            }
            this.trigger('choosed-tags-changed', this.choosedTags);
        },
        getChoosedTags: function () {
            return this.choosedTags;
        },
        setChoosedTags: function (models){
            var $self = this;
            this.choosedTags = [];

            _.each(models, function (model) {
                $self.choosedTags.push(model.attributes);
            });
        }
    });
})
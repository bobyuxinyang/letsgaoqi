define(function (require) {

    window.activity_tag = Backbone.Model.extend({
        initialize: function () {
            
        },
        toggleChoose: function () {
            var isChoosed = !this.get('isChoosed');
            this.set({isChoosed: isChoosed});
            this.trigger('choose-status-changed', this, isChoosed);
        }

    });
});
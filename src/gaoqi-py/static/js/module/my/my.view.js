define(function (require) {
    var Views = [];

    return Backbone.View.extend({
        tagName: "div",
        events: {
        },
        initialize: function () {
            var $self = this;
            $(this.el).prop('id', 'my');
            $(document).bind('now', function () {
                $($self.el).find('.activity-list').remove();
                $self.bindActivitiesOnging();
            });
            $(document).bind('before', function () {
                $($self.el).find('.activity-list').remove();
                $self.bindActivitiesFinished();
            });
        },
        render: function () {
            $(this.el).empty();
            this.bindActivitiesNavBar();
            this.bindActivitiesOnging();
            return this;
        },
        bindActivitiesNavBar: function() {
            var ActivityNavBarView = require('view/activity-navbar.view');

            Views.activity_navView = new ActivityNavBarView();
            $(this.el).append(Views.activity_navView.render().el);
        },
        bindActivitiesOnging: function() {
            var ActivitiesView = require('module/activity-card/activity-card-list.view');
            require('module/activity-card/activity-card-list');

            // AjaxLoad.AjaxLoadBegin();

            Views.now_activitiesView = new ActivitiesView({ model: new AcitivityList(null, 'ongoing') });
            $(this.el).append(Views.now_activitiesView.render().el);

            // AjaxLoad.AjaxLoadEnd();
        },
        bindActivitiesFinished: function() {
            var ActivitiesView = require('module/activity-card/activity-card-list.view');
            require('module/activity-card/activity-card-list');

            // AjaxLoad.AjaxLoadBegin();

            Views.before_activitiesView = new ActivitiesView({ model: new AcitivityList(null, 'finished') });

            $(this.el).append(Views.before_activitiesView.render().el);

            // AjaxLoad.AjaxLoadEnd();
        }


    });
    
});
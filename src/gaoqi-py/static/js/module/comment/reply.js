﻿define(function (require) {
    require('utility/datetime');
    var service = require('utility/service');

    window.Reply = Backbone.Model.extend({
        initialize: function () {
            var dateTime = Date.fromEpoch(this.get('created_at'));
            var timeSpan = dateTime.toNiceTime();
            this.set({'created_at_str': timeSpan });
        }
    });
});

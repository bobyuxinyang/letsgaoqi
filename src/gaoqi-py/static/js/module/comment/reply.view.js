﻿define(function (require) {
    require('module/comment/comment-list');

    return Backbone.View.extend({
        //... is a list tag.
        tagName: "dl",
        template: _.template($("#comment-reply-content").html()),
        events: {
        },

    initialize: function () {
        var $self = this;

        $(this.el).addClass('feed');

        _.bindAll(this, 'render');
        this.model.bind('change', this.render);
        this.model.view = this;
    },
    render: function () {
        $(this.el).html(this.template(this.model.toJSON()));

        return this;
    },
   
    remove: function () {
        $(this.el).remove();
    },
    clear: function () {

    }
});
});
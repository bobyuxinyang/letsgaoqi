﻿define(function (require) {
    require('utility/datetime');
    var service = require('utility/service');
    
    window.Message = Backbone.Model.extend({
        initialize: function () {
            var dateTime = Date.fromEpoch(this.get('created_at'));
            var timeSpan = dateTime.toNiceTime();
            this.set({'created_at_str': timeSpan });

            var replies_count = this.get('replies_count');
            if(replies_count > 0 )
                this.set({'replies_count_str': '(' + replies_count + ')'});
            else
                this.set({'replies_count_str': ''});
        },
        reply: function(newreply){
            var _self = this;
            var comment_id = _self.get('comment_id');
            var replies_count = _self.get('replies_count');
            var sender_id = newreply.sender.sender_id;
            var text = newreply.text;

            service.new_comment_reply(function (e) {
                _self.set({'replies_count': replies_count + 1});
                _self.trigger('new-reply', newreply, comment_id);
                
                $.displayStatusInfo(e);
            }, { comment_id: comment_id, sender_id: sender_id, text: text})
        }
    });
});

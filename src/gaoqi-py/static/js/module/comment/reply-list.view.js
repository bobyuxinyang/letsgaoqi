﻿define(function (require) {
    var ReplyView = require('module/comment/reply.view');

    return Backbone.View.extend({
        tagName: "div",
        events: {

    },
    initialize: function (args) {

        _.bindAll(this, 'addOne', 'render');
        this.model.bind('getAll', this.render);
    },

    render: function () {
        this.clear();

        this.model.each(this.addOne);
        return this;
    },
    addOne: function (e) {
        var view = new ReplyView({ model: e });
        var element = view.render().el;
        $(this.el).append(element);
    },

    clear: function (e) {
        $(this.el).empty();
    }

});

});
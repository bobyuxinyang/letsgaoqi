﻿define(function (require) {
    var MessageView = require('module/comment/comment.view');

    return Backbone.View.extend({
        tagName: "div",
        events: {

    },
    initialize: function () {
        _.bindAll(this, 'addOne', 'render');
        this.model.bind('getAll', this.render);
    },
    render: function () {
        this.clear();

        this.model.each(this.addOne);
        return this;
    },
    addOne: function (e) {
        var view = new MessageView({ model: e });
        var element = view.render().el;
        $(this.el).append(element);
    },

    clear: function (e) {
        $(this.el).empty();
    }

});

});
﻿define(function (require) {
    var service = require('utility/service');
    
    require('module/comment/comment');

    window.MessageList = Backbone.Collection.extend({
        model: Message,
        comparator: function (e) {
            return - e.get("creat_at");
        },
        initialize: function (model, status) {
               
        }
    });
});
﻿define(function (require) {
    require('module/comment/comment-list');
    require('module/comment/reply-list');

    var ReplyListView = require('module/comment/reply-list.view');
    var service = require('utility/service');

    var current_user;
    service.get_current_user_Sync(function (e) {
        current_user = e;
    });


    return Backbone.View.extend({
        //... is a list tag.
        tagName: "dl",
        template: _.template($("#comment-content").html()),
        events: {
            "click .reply-comment": "showreply"
            , "click .btn-reply": "reply"
        },

        replyListView: null,

        initialize: function () {
            var $self = this;

            $(this.el).addClass('feed clearfix');

            _.bindAll(this, 'render', 'replyCallback');
            this.model.bind('new-reply', this.replyCallback);
            this.model.view = this;
        },
        render: function () {
            var $self = this;

            $($self.el).html($self.template($self.model.toJSON()));

            $($self.el).find('.reply-holder').artTxtCount({
                btnOnCallback: function () {
                    $($self.el).find('.btn').attr('disabled', false);
                },
                btnOffCallback: function () {
                    $($self.el).find('.btn').attr('disabled', true);
                }
            });

            return $self;
        },

        isDisplayed: false,

        renderRepliesView: function(){
            var _self = this;
            var $holder = $(_self.el).find('.repeat');
            var id = _self.model.get('comment_id');

            if(!id) return false;
            _self.replyListView= new ReplyListView({
                model: new ReplyList (null, id)
            });
            _self.replyListView.render();

            $holder.find('#replyList').html(_self.replyListView.el);
        },

        showreply: function(){
            var _self = this;
            var $holder = $(_self.el).find('.repeat').slideDown()

            if(!_self.isDisplayed){
                $holder.slideDown()
                _self.renderRepliesView();
                _self.isDisplayed = true;
            }else{
                $holder.slideUp();
                _self.isDisplayed = false;
            }
        },

        reply: function(){
            var $self = this;

            var $holder = $($self.el).find('.reply-holder');
            var newReply = {
                sender: {
                    sender_id: current_user['_id'],
                    username: current_user['username'],
                    profile_image_url:current_user['profile_image_url']
                },
                created_at: new Date().toEpoch(),
                text: $holder.val().htmlEncode()
            };

            $self.model.reply(newReply);
            $holder.val('');
        },
        replyCallback: function(newReply){
            var _self = this;
            var model= _self.replyListView.model;
            model.add(newReply);
            var count = model.models.length;
            _self.replyListView.render();

            $(_self.el).find('.reply-comment').html('回复' + '(' + count + ')')
        },

        remove: function () {
            $(this.el).remove();
        },
        clear: function () {

        }
    });
});
﻿define(function (require) {
    var service = require('utility/service');
    
    require('module/comment/reply');

    window.ReplyList = Backbone.Collection.extend({
        model: Reply ,
        comparator: function (e) {
            return - e.get("creat_at");
        },
        initialize: function (model, id) {
            this.getAll(id);
        },
        getAll: function (id) {
            var _self = this;

            service.get_comment_replies(function(replies ){
                _self.add(replies);
                _self.trigger("getAll");
            },{comment_id : id});
        }
    });

});
define(function(require){
    var service = require('utility/service');
    require('module/mytags/mytag');

    window.Mytags = Backbone.Collection.extend({
        model: mytag,

        initialize: function (model) {
            //如果传入tags models 直接用
            //如果不传入tags models 就去服务器获取注册时选得所有tags
            if(!model) this.getAll();
            else this.bindModelRemoveEvent();
        },
        comparator: function (e) {
            return -e.get("order");
        },
        getAll: function () {
            var $self = this;

            $.ajaxSetup({cache: false});

            service.get_my_activity_tags(function (e) {
                $self._reset();
                $self.add(e);
                $self.trigger('getAll');

                $self.bindModelRemoveEvent();

                $.ajaxSetup({cache: true});
            });
        },
        
        bindModelRemoveEvent: function(){
            //为每个model 绑定 删除事件
            var _tags = this;

            for (var i in _tags.models) {
                var tag = _tags.models[i];
                
                tag.bind('removeCallback', function (cid) {
                    var model = _tags.getByCid(cid);
                    _tags.remove(model);
                });
               
            }
        },

        getTagsList: function(){
            var orderList = new Array();
            for(var key in this.models){
                var model = this.models[key];
                orderList.push(model.get('order'));
            }

            return orderList;
        }
    });
})
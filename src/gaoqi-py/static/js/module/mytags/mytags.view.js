//views for 注册时以及修改自己兴趣时的tags
//collection: mytags
//model: mytag
define(function (require) {
    var ChooseTagsView = require('module/dialog/choose-tags.view');
    var MyTagView = require('module/mytags/mytag.view');

    return Backbone.View.extend({
        tagName: "div",
        events: {
            'click a.more': 'btnMoreClick'
        },

        template: '<div class="my-tag-title"><span class="title">我感兴趣的活动主题</span><a class="more">选择更多...</a></div><ul class="my-tag-list"></ul>',
        initialize: function () {
            _.bindAll(this, 'addOne', 'render');
            this.model.bind('getAll', this.render);
//            this.model.bind('choose-status-changed', this.render);
        },
        render: function () {
            $(this.el).html(this.template);
            this.model.each(this.addOne);

            return this;
        },
        addOne: function (e) {
            var mytagView = new MyTagView({ model: e });
            $(this.el).find('.my-tag-list').append(mytagView.render().el);
        },
        refresh: function (e) {
            this.model.getAll();
        },
        btnMoreClick: function () {
            var $self = this;

            var chooseTagsView = new ChooseTagsView({model: $self.model});
            $(chooseTagsView.render().el)
                           .fadeIn();
        }
    });

});
define(function (require) {

    return Backbone.View.extend({
        tagName: "li",
        events: {
        },
        template: _.template('<span class="label tag"><%= name %></span>'),

        initialize: function () {
            _.bindAll(this, 'render');
        },
        render: function () {
            $(this.el).append(this.template(this.model.toJSON()));

            return this;
        }

    });
});
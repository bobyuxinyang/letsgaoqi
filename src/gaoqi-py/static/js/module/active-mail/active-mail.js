define(function (require) {
    require('utility/datetime');
    var service = require('utility/service');

    window.ActiveMail = Backbone.Model.extend({
        initialize: function () {
            var $self = this;
            service.get_active_mail_info_Sycn(function (info) {
                if (info && info.email) {
                    $self.attributes = info;
                    $self.trigger('getAll');
                } else {
                    window.location = "/";
                }
            });
        }
    });

});

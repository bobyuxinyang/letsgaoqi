define(function (require) {
    var service = require('utility/service');

    require('utility/datetime');
    require('utility/string');


    return Backbone.View.extend({
        tagName: "div",
        template: _.template($("#tpl-active-mail").html()),

        render: function () {
            var $self = this;

            $($self.el).html(this.template($self.model.toJSON()));
            return $self;
        },

        initialize: function (arg) {
            var $self = this;
            _.bindAll($self, 'render');
            $self.model.bind('getAll', $self.render);
        }
    });

});
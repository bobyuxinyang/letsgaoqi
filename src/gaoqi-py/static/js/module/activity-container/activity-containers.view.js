/**
 * Created by PyCharm.
 * User: meteor1211
 * Date: 11-11-24
 * Time: 下午3:40
 * To change this template use File | Settings | File Templates.
 */

define(function (require) {
    var ActivityContainerView = require('module/activity-container/activity-container.view');

    return Backbone.View.extend({
        tagName: "div",
        events: {
        },

        initialize: function () {
            _.bindAll(this, 'addOne', 'render');
            this.model.bind('getMine', this.render);
            this.model.bind('getFrirends', this.render);
        },
        render: function () {
            this.model.each(this.addOne);

            return this;
        },
        addOne: function (e) {
            var view = new ActivityContainerView({ model: e });
            var element = view.render().el;
            $(this.el).append(element);
        },

        clear: function (e) {

        }

    });

});

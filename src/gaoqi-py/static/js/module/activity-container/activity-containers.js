/**
 * Created by PyCharm.
 * User: meteor1211
 * Date: 11-11-24
 * Time: 下午3:40
 * To change this template use File | Settings | File Templates.
 */

define(function (require) {
    var service = require('utility/service');
    require('module/activity-container/activity-container');

    window.AcitivityContainerList = Backbone.Collection.extend({
        model: ActivityContainer,

        comparator: function (e) {

        },
        initialize: function (model, status) {
            status == 'mine' ? this.getMine() : this.getFrirends();
        },
        getMine: function () {
            //todo:获得我 求得活动 以及 我参加的活动
            
//            var mine = {
//                user:{
//                        "username": "\u6768\u946b\u8bdaBruce",
////                        "_id": "4ecc6973a2da1e0ba4000001",
//                        "_id": "4ecce89e15ab44032c000001",
//                        "profile_image_url": "http://tp1.sinaimg.cn/1862940640/50/5613480639/1"
//                },
//                activity_list:  [{
//                   'activity_id': 1,
//                   'created_at': new Date().toEpoch() - 30000,
//                   'text': '哇哇哇哇哇哇哇哇哇哇哇哇哇哇哇哇哇哇哇哇哇哇',
//                   'plan_at': new Date().toEpoch() + 30000,
//                   'participators':[{
//                       'update_at': new Date().toEpoch() - 70000,
//                       'user': {
//                           '_id': 11, 'username': '王昊',  'description': 1, 'profile_image_url': 'http://tp3.sinaimg.cn/1930080130/50/5609852091/0', 'weibo_id': '1930080130'
//                       }
//                   },
//                   {
//                       'update_at': new Date().toEpoch() - 60000,
//                       'user': {
//                           '_id': 11, 'username': '王昊',  'description': 1, 'profile_image_url': 'http://tp2.sinaimg.cn/1310915297/50/5614164220/0', 'weibo_id': '1310915297'
//                       }
//                   }],
//                    tags: [{
//                        'order': 2, 'name': '逛公园'
//                    }, {
//                        'order': 3, 'name': '听讲座'
//                    }],
//                   'comment_count': '3'
//                },
//                {
//                   'activity_id': 1,
//                   'created_at': new Date().toEpoch() - 80000,
//                   'text': '哼哼哼哼哼哼哼哼哼哼哼哼哼哼哼哼哼哼哼哼哼哼哼',
//                   'plan_at': new Date().toEpoch() + 30000,
//                   'participators':[{
//                       'update_at': new Date().toEpoch() - 20000,
//                       'user': {
//                           '_id': 11, 'username': '王昊',  'description': 1, 'profile_image_url': 'http://tp3.sinaimg.cn/1930080130/50/5609852091/0', 'weibo_id': '1930080130'
//                       }
//                   },
//                   {
//                       'update_at': new Date().toEpoch() - 40000,
//                       'user': {
//                           '_id': 99, 'username': '王昊',  'description': 1, 'profile_image_url': 'http://tp2.sinaimg.cn/1310915297/50/5614164220/0', 'weibo_id': '1310915297'
//                       }
//                   }],
//                    tags: [{
//                        'order': 2, 'name': '梦游'
//                    }, {
//                        'order': 3, 'name': '踢足球'
//                    },{
//                        'order': 4, 'name': '摄影'
//                    }],
//                    'comment_count': '5'
//                }],
//                requested_list: [{
//                    'activity_id': 1,
//                    'created_at': new Date().toEpoch() - 3000,
//                    'text': '啊啊啊啊啊啊啊啊啊',
//                    'plan_at': new Date().toEpoch() + 3000,
//                    'participators':[{
//                        'update_at': new Date().toEpoch() - 2000,
//                        'user': {
//                            '_id': '4ecce89e15ab44032c000001', 'username': '陈陈',  'description': 1, 'profile_image_url': 'http://tp1.sinaimg.cn/1862940640/50/5613480639/1', 'weibo_id': '1930080130'
//                        }
//                    },
//                    {
//                        'update_at': new Date().toEpoch() - 4000,
//                        'user': {
//                            '_id': '4ecce89e15ab44032c000005', 'username': '黄光',  'description': 1, 'profile_image_url': 'http://tp2.sinaimg.cn/1310915297/50/5614164220/0', 'weibo_id': '1310915297'
//                        }
//                    }],
//                    tags: [{
//                        'order': 2, 'name': '逛街'
//                    }, {
//                        'order': 3, 'name': '吃饭'
//                    },{
//                        'order': 4, 'name': '看电影'
//                    }]
//
//                },
//                {
//                    'activity_id': 1,
//                    'created_at': new Date().toEpoch() - 8000,
//                    'text': '呀呀呀哎呀呀呀呀',
//                    'plan_at': new Date().toEpoch() + 3000,
//                    'participators':[{
//                        'update_at': new Date().toEpoch() - 2000,
//                        'user': {
//                            '_id': '4ecce89e15ab44032c000001', 'username': 'meizi',  'description': 1, 'profile_image_url': 'http://tp1.sinaimg.cn/1862940640/50/5613480639/1', 'weibo_id': '1930080130'
//                        }
//                    },
//                    {
//                        'update_at': new Date().toEpoch() - 4000,
//                        'user': {
//                            '_id': 66, 'username': '妹纸',  'description': 1, 'profile_image_url': 'http://tp2.sinaimg.cn/1310915297/50/5614164220/0', 'weibo_id': '1310915297'
//                        }
//                    }],
//                    tags: [{
//                        'order': 2, 'name': '骑车'
//                    }, {
//                        'order': 3, 'name': '徒步'
//                    },{
//                        'order': 4, 'name': '随便逛逛'
//                    }]
//
//                }]
//            };
            var mine = {
                user:{
                        "username": "\u6768\u946b\u8bdaBruce",
                        "_id": window.Current_user['_id'],
                        "profile_image_url": "http://tp1.sinaimg.cn/1862940640/50/5613480639/1"
                },
                activity_list:  [],
                requested_list:[]
            }


            //将自己感兴趣的和参加的活动Json 转成 Array
            if(mine instanceof Object && !mine instanceof Array)
                mine = [mine];

            this.add(mine);
            this.trigger('getMine');
        },
        getFrirends: function () {
            //todo:获得我朋友们 求得活动 以及 我参加的活动

            var friends = [{
                user:{
                        "username": "迪susan",
                        "_id": "4ecc6973a2da1e0ba4000003",
                        "profile_image_url": "http://tp2.sinaimg.cn/1890984777/50/5604155970/01"
                },
                activity_list:  [{
                   'activity_id': 1,
                   'created_at': new Date().toEpoch() - 80000,
                   'text': '咦咦咦咦咦咦咦咦咦咦咦咦',
                   'plan_at': new Date().toEpoch() + 30000,
                   'participators':[{
                       'update_at': new Date().toEpoch() - 20000,
                       'user': {
                           '_id': window.Current_user['_id'], 'username': '王昊',  'description': 1, 'profile_image_url': 'http://tp3.sinaimg.cn/1930080130/50/5609852091/0', 'weibo_id': '1930080130'
                       }
                   },
                   {
                       'update_at': new Date().toEpoch() - 40000,
                       'user': {
                           '_id': 11, 'username': '王昊',  'description': 1, 'profile_image_url': 'http://tp2.sinaimg.cn/1310915297/50/5614164220/0', 'weibo_id': '1310915297'
                       }
                   }],
                    tags: [{
                        'order': 2, 'name': '梦游'
                    }, {
                        'order': 3, 'name': '踢足球'
                    },{
                        'order': 4, 'name': '摄影'
                    }],
                    'comment_count': '5'
                }],
                requested_list: [{
                    'activity_id': 1,
                    'created_at': new Date().toEpoch() - 8000,
                    'text': '咔咔咔咔咔咔咔咔咔',
                    'plan_at': new Date().toEpoch() + 3000,
                    'participators':[{
                        'update_at': new Date().toEpoch() - 2000,
                        'user': {
                            '_id': window.Current_user['_id'], 'username': '王昊',  'description': 1, 'profile_image_url': 'http://tp3.sinaimg.cn/1930080130/50/5609852091/0', 'weibo_id': '1930080130'
                        }
                    },
                    {
                        'update_at': new Date().toEpoch() - 4000,
                        'user': {
                            '_id': 55, 'username': '王昊',  'description': 1, 'profile_image_url': 'http://tp2.sinaimg.cn/1310915297/50/5614164220/0', 'weibo_id': '1310915297'
                        }
                    }],
                    tags: [{
                        'order': 2, 'name': '骑车'
                    }, {
                        'order': 3, 'name': '徒步'
                    },{
                        'order': 4, 'name': '随便逛逛'
                    }]

                }]
            },
            {
                user:{
                        "username": "Cherry_if茹",
                        "_id": "4ecc6973a2da1e0ba4000002",
                        "profile_image_url": "http://tp2.sinaimg.cn/1979743057/50/5608384915/0"
                },
                activity_list:  [{
                   'activity_id': 1,
                   'created_at': new Date().toEpoch() - 80000,
                   'text': '呢呢呢呢呢呢呢呢',
                   'plan_at': new Date().toEpoch() + 30000,
                   'participators':[{
                       'update_at': new Date().toEpoch() - 20000,
                       'user': {
                           '_id': 33, 'username': '王昊',  'description': 1, 'profile_image_url': 'http://tp3.sinaimg.cn/1930080130/50/5609852091/0', 'weibo_id': '1930080130'
                       }
                   }],
                    tags: [{
                        'order': 2, 'name': '梦游'
                    }, {
                        'order': 3, 'name': '踢足球'
                    },{
                        'order': 4, 'name': '摄影'
                    }],
                    'comment_count': '5'
                }],
                requested_list: []
            },{
                user:{
                        "username": "Cherry_if茹",
                        "_id": "4ecc6973a2da1e0ba4000002",
                        "profile_image_url": "http://tp2.sinaimg.cn/1979743057/50/5608384915/0"
                },
                activity_list:  [],
                requested_list: [{
                    'activity_id': 1,
                    'created_at': new Date().toEpoch() - 8000,
                    'text': '咔咔咔咔咔咔咔咔咔',
                    'plan_at': new Date().toEpoch() + 3000,
                    'participators':[{
                        'update_at': new Date().toEpoch() - 2000,
                        'user': {
                            '_id': 44, 'username': '王昊',  'description': 1, 'profile_image_url': 'http://tp3.sinaimg.cn/1930080130/50/5609852091/0', 'weibo_id': '1930080130'
                        }
                    },
                    {
                        'update_at': new Date().toEpoch() - 4000,
                        'user': {
                            '_id': 55, 'username': '王昊',  'description': 1, 'profile_image_url': 'http://tp2.sinaimg.cn/1310915297/50/5614164220/0', 'weibo_id': '1310915297'
                        }
                    }],
                    tags: [{
                        'order': 2, 'name': '骑车'
                    }, {
                        'order': 3, 'name': '徒步'
                    },{
                        'order': 4, 'name': '随便逛逛'
                    }]

                }]
            }];
            
            this.add(friends);
            this.trigger('getFrirends');
        }
    });
});

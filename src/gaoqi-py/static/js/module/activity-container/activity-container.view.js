/**
 * Created by PyCharm.
 * User: meteor1211
 * Date: 11-11-24
 * Time: 下午3:40
 * To change this template use File | Settings | File Templates.
 */

define(function (require) {
    var service = require('utility/service');
        
    var RequestedListView = require('module/requested-card/requested.list.view');
    require('module/requested-card/requested.list');

    var ActivitiesListView = require('module/activity-card/activity-card-list.view');
    require('module/activity-card/activity-card-list');

    var EmptyView = require('module/requested-card/empty.view');

    return Backbone.View.extend({
        //... is a list tag.
        tagName: "div",
        template: _.template($("#tpl-activity-container").html()),
        events: {
        },
        Views: {
            my_requested_view: null
            , my_activities_view: null
        },
        
        initialize: function () {
            this.isMine = this.model.get('user')['_id'] == window.Current_user['_id'];

            _.bindAll(this, 'render');
            this.model.bind('change', this.render);
            this.model.view = this;
        },

        bindMyRequestedList: function(){
            var models =  this.model.get('requested_list');
            if(models.length == 0) {
                if(this.isMine){
                    var emptyView = new EmptyView();
                    $(this.el).find('.demand').html(emptyView.render().el)
                }else{
                    $(this.el).find('.demand').remove();
                }

                return;
            };

            var view = this.Views.my_requested_view = new RequestedListView({
                model: new RequestedList(models)
            });

            $(this.el).find('.demand').append(view.render().el);
        },

        bindMyActivitiesList: function(){
            var models =  this.model.get('activity_list');
            if(models.length == 0) {
                $(this.el).find('.participated').remove();
                return;
            };

            var view = this.Views.my_activities_view = new ActivitiesListView({
                model: new AcitivityList(models)
            });

            $(this.el).find('.participated').append(view.render().el);
        },
        render: function () {
            $(this.el).html(this.template(this.model.toJSON()));

            this.bindMyRequestedList();
            this.bindMyActivitiesList();

            //remove some btn that 'i' can not do
            if(this.isMine){
                $(this.el).find('.btn-demand, .btn-join, .btn-requested').remove();
            }

            return this;
        },
        
        remove: function () {
            $(this.el).remove();
        },
        clear: function () {

        }
    });
});

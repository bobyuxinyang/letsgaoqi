﻿define(function (require) {
    var ParticipatorListView = require('view/participatorlist.view');
    var TagsView = require('module/activity-tags/activity-detail-tags.view');

    require('model/participatorlist');
    require('module/mytags/mytags');

    return Backbone.View.extend({
        //... is a list tag.
        tagName: "div",
        template: _.template($("#activity-content").html()),
        events: {
            "click a": "showActivity"
            , 'click  .btn-join': 'askForJoin'
        },
        initialize: function () {
            var $self = this;

            $(this.el).addClass('pop-content clearfix');

            _.bindAll(this, 'render');
            this.model.bind('change', this.render);
            this.model.view = this;
        },

        renderTags: function(){
            var tagsView = new TagsView(
            {
               model: new Mytags(
                   this.model.get('tags')
               )
            });

            tagsView.render();
            $(this.el).find('.activity-tags').append(tagsView.el);

        },

        renderParticipators: function(){
            var participatorListView = new ParticipatorListView(
                {
                    model: new ParticipatorList(
                        this.model.get('participators')
                    )
                });

            participatorListView.render();
            $(participatorListView.el).insertBefore($(this.el).find('.participators-info'));
        },


        //是否已经参加该活动
        checkIsIn: function(){
            var i_am_in = false;

            var participators = this.model.get('participators');
            participators = _.filter(participators, function(item){
                return item.user['_id'] == window.Current_user['_id'];
            });

            if(participators.length > 0){
                console.log(this.model.get('text') + " 我参加了");

                i_am_in = true;
                $(this.el).find('.btn-join, .btn-requested').remove();
            }

            return i_am_in;
        },


        //是否已经申请参加该活动
        checkIsRequested: function(){
            //todo:判断是否申请参加
            have_requested = (new Date().toEpoch() % 2 == 0) ? true : false;

            if(have_requested){
                console.log(this.model.get('text') + " 我申请参加了");

                $(this.el).find('.btn-join').remove();
                $(this.el).find('.btn-requested').show();
            }

        },

        render: function () {
            $(this.el).html(this.template(this.model.toJSON()));

            this.renderTags();
            this.renderParticipators();

            var i_am_in = this.checkIsIn();
            if(! i_am_in) this.checkIsRequested();

            return this;
        },

        showActivity: function () {
            $(document).trigger('gotoActivity', this.model.get('activity_id'));
        },

        askForJoin: function(e){
            this.model.askForJoin();

            var btn = e.currentTarget;
            $(btn).hide();
            $(btn).next().show();
        },

        remove: function () {
            $(this.el).remove();
        },
        clear: function () {

        }
    });
});
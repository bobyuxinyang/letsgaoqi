﻿define(function (require) {
    require('utility/gaoqi-displaystatusinfo');
    require('utility/datetime');

    var stautsMap = {
        ongoing: {
            StatusClass: 'icon-face-smile',
            StatusDes: '正在搞'
        },
        past: {
            StatusClass: 'icon-face-smile',
            StatusDes: '搞定了'
        },
        canceled: {
            StatusClass: 'icon-face-sad',
            StatusDes: '不搞了'
        },
        private: {
            OpenClass: 'icon-lock',
            OpenDes: '不公开的'
        },
        public: {
            OpenClass: 'icon-unlock',
            OpenDes: '公开的'
        }
    };

    window.Activity = Backbone.Model.extend({
        initialize: function () {
            this.setTimeInfo();
//            this.setStatusInfo();
//            this.setOpenInfo();
            this.setParticipatorInfo();
        },
        setTimeInfo: function () {
            var starTime = Date.fromEpoch(this.get('plan_at'));
            var updateTime = Date.fromEpoch(this.get('created_at'));

            this.set({
                'plan_at_str': starTime.toFullDateTimeAndDay() + "  " + starTime.DateDiff()
                , 'created_at_str': updateTime.toFullDateTimeAndDay() + "  " + updateTime.DateDiff()
            });
        },
        setStatusInfo: function () {
            var statusStr = this.get('status');

            this.set({ 'available_class': stautsMap[statusStr].StatusClass,
                'available_des': stautsMap[statusStr].StatusDes
            });
        },
        setOpenInfo: function () {
            var status = this.get('visibility');
            
            this.set({ 'visibile_class': stautsMap[status].OpenClass,
                'visibile_des': stautsMap[status].OpenDes
            });
        },
        setParticipatorInfo: function () {
            var participators = this.get('participators');

            var confirmedCount = _.filter(participators, function (participator) {
                return participator.status == 'in-activity';
            }).length;
            var unconfirmedCount = _.reject(participators, function (participator) {
                return participator.status == 'in-activity';
            }).length;

            this.set({ 'confirmed_count': confirmedCount });
            this.set({ 'unconfirmed_count': unconfirmedCount });
        },

        askForJoin: function(){
            //todo: 申请参加
            var activity_id = this.get('activity_id');

            $.displayStatusInfo({Info: '申请成功，请等待审核！'})
        }

    });

});

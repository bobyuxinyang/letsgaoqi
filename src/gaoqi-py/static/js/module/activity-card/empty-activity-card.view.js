define(function (require) {
    var ParticipatorListView = require('view/participatorlist.view');
    require('model/participatorlist');
    var service = require('utility/service');

    return Backbone.View.extend({
        //... is a list tag.
        tagName: "div",
        template: _.template($("#empty-activity-content").html()),
        initialize: function () {
            $(this.el).addClass('activity activity-text');
            _.bindAll(this, 'render');
        },
        render: function () {

            function getEmptyTitle() {
                var titles_array = [
                        '星期天，好好策划一下下周的活动吧'
                      , '星期一，除了努力工作，也策划一下周末计划吧'
                      , '星期二，除了努力工作，也策划一下周末计划吧'
                      , '星期三，还有三天就是周末了，快想想周末的计划吧'
                      , '星期四，还有两天就是周末了，快想想周末的计划吧'
                      , '星期五，明天就是周末了，快想想周末计划吧'
                      , '星期六，好好策划一下下周的活动吧'
                    ];
                var day = new Date().getDay();
                
                return titles_array[day];
            }

            var current_user = service.get_current_user_Sync();

            $(this.el).html(this.template({owner: current_user}));

            $(this.el).find('.activity-empty-title').html(getEmptyTitle());
            return this;
        }
    });
});
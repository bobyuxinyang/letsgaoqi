﻿define(function (require) {
    var ActivityView = require('module/activity-card/activity-card.view');
    var service = require('utility/service');

    return Backbone.View.extend({
        tagName: "div",
        events: {
        },

        initialize: function () {
            _.bindAll(this, 'addOne', 'render');
        },
        render: function () {
            this.model.each(this.addOne);

            return this;
        },
        addOne: function (e) {
            var view = new ActivityView({ model: e });
            var element = view.render().el;
            $(this.el).append(element);
        },

        clear: function (e) {

        }

    });

});
/**
 * Created by PyCharm.
 * User: meteor1211
 * Date: 11-11-24
 * Time: 下午2:04
 * To change this template use File | Settings | File Templates.
 */

//views for 活动对应的 tags
//collection: mytags
//model: mytag
define(function (require) {
    var tagView = require('module/activity-tags/activity-detail-tag.view');

    var service = require('utility/service');

    return Backbone.View.extend({
        tagName: "div",
        events: {
        },

        initialize: function () {
            _.bindAll(this, 'addOne', 'render');
            this.model.bind('getAll', this.render);
        },
        render: function () {
            this.model.each(this.addOne);

            return this;
        },
        addOne: function (e) {
            var view = new tagView({ model: e });
            var element = view.render().el;
            $(this.el).append(element);
        },

        clear: function (e) {

        }

    });

});


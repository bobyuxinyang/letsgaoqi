/**
 * Created by PyCharm.
 * User: meteor1211
 * Date: 11-11-24
 * Time: 下午2:03
 * To change this template use File | Settings | File Templates.
 */
define(function (require) {

    return Backbone.View.extend({
        tagName: "span",
        events: {
        },
        template: _.template('<%= name %>'),

        initialize: function () {
            $(this.el).addClass('tag');
            _.bindAll(this, 'render');
        },
        render: function () {
            $(this.el).append(this.template(this.model.toJSON()));

            return this;
        }

    });
});
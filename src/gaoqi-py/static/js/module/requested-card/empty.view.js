define(function (require) {
    var service = require('utility/service');

    return Backbone.View.extend({
        //... is a list tag.
        tagName: "div",
        template: _.template($("#empty-content").html()),
        events:{
            'click .activity-beg': 'begActivity'
        },
        initialize: function () {
            _.bindAll(this, 'render');
        },
        render: function () {
            $(this.el).html(this.template());
            return this;
        },
        begActivity: function(){
            $(document).trigger('begActivity');
        }
    });
});
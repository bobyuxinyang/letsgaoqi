/**
 * Created by PyCharm.
 * User: meteor1211
 * Date: 11-11-24
 * Time: 上午11:26
 * To change this template use File | Settings | File Templates.
 */
define(function (require) {
    var ParticipatorListView = require('view/participatorlist.view');
    var TagsView = require('module/activity-tags/activity-detail-tags.view');

    require('model/participatorlist');
    require('module/mytags/mytags');


    return Backbone.View.extend({
        tagName: "div",
        template: _.template($("#requested-content").html()),
        events: {
            'click .btn-gaoqi': 'gaoqi'
            , 'click .btn-demand': 'request'
        },
        initialize: function () {
            _.bindAll(this, 'render', 'renderParticipators');
            this.model.bind('change', this.render);
            this.model.bind('addParticipatorCallback', this.renderParticipators);
            
            this.model.view = this;
        },

        renderTags: function(){
            var tagsView = new TagsView(
            {
               model: new Mytags(
                   this.model.get('tags')
               )
            });

            tagsView.render();
            $(this.el).find('.activity-tags').append(tagsView.el);

        },

        renderParticipators: function(){
            var participatorListView = new ParticipatorListView(
                {
                    model: new ParticipatorList(
                        this.model.get('participators')
                    )
                });

            participatorListView.render();

            var holder = $(this.el);
            $(holder).find('.participators-list').remove();
            $(participatorListView.el).insertBefore(holder.find('.participators-info'));
        },


        //是否已经申请“同求”该活动
        checkIsRequested: function(){
            var participators = this.model.get('participators');

            participators = _.filter(participators, function(item){
               return item.user['_id'] == window.Current_user['_id'];
            });

            if(participators.length > 0){
                console.log(this.model.get('text') + " 我求");

                $(this.el).find('.btn-demand').remove();
                $(this.el).find('.btn-requested').show();
            }
        },
        
        render: function () {
            $(this.el).html(this.template(this.model.toJSON()));

            this.renderTags();
            this.renderParticipators();

            this.checkIsRequested();

            return this;
        },


        addTip: function(btn){
            var offset =$(btn).offset();
            var tip = $('<div/>');

            tip.html('+1')
               .addClass("circle__popup_green circle__popup")
               .css({
                   left: offset.left,
                   top: offset.top}
               )
               .appendTo('body');

            setTimeout(function () {
                tip.addClass('circle__popup_phase1');
                tip._timer = setTimeout(function () {
                    tip.addClass('circle__popup_phase2');
                    setTimeout(function () {
                        tip.remove();tip = null;
                    }, 1000);}
                , 1000);}
            , 10);
        },

        gaoqi: function(){
            var _self = this;

            var args = new Object();
            args['participators'] = _self.model.get('participators');
            args['text'] = _self.model.get('text');
            args['plan_at'] = _self.model.get('plan_at');
            args['tags'] = _self.model.get('tags');

            $(document).trigger('gaoqiActivity', args);
        },

        request: function(e){
            this.model.addParticipator();

            var btn = e.currentTarget
            this.addTip(btn);
            $(btn).hide();
            $(btn).next().show();
        },
        
        remove: function () {
            $(this.el).remove();
        },
        clear: function () {

        }
    });
});
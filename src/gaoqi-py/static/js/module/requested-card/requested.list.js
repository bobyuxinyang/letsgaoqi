/**
 * Created by PyCharm.
 * User: meteor1211
 * Date: 11-11-24
 * Time: 上午11:26
 * To change this template use File | Settings | File Templates.
 */
define(function (require) {
    var service = require('utility/service');
    require('module/requested-card/requested');

    window.RequestedList = Backbone.Collection.extend({
        model: Requested,

        comparator: function (e) {
            return e.get("update_at");
        },
        initialize: function (model) {

        }
    });
});
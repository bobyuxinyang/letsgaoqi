/**
 * Created by PyCharm.
 * User: meteor1211
 * Date: 11-11-24
 * Time: 上午11:25
 * To change this template use File | Settings | File Templates.
 */
define(function (require) {
    require('utility/datetime');
    
    window.Requested = Backbone.Model.extend({
        initialize: function () {
            this.setTimeInfo();
        },
        setTimeInfo: function () {
            var starTime = Date.fromEpoch(this.get('plan_at'));
            var updateTime = Date.fromEpoch(this.get('created_at'));

            this.set({
                'plan_at_str': starTime.toFullDateAndDay() + "  " + starTime.DateDiff()
                , 'created_at_str': updateTime.toFullDateTimeAndDay() + "  " + updateTime.DateDiff()
            });
        },
        addParticipator: function(){
            //todo:同求
            var activity_id = this.get('activity_id');

            //refresh
            var myself = window.Current_user;
            var newParticipator = new Object();

            newParticipator.update_at = new Date().toEpoch();
            newParticipator.user = {
                '_id': myself['_id'],
                'profile_image_url': myself['profile_image_url'],
                'username': myself['username'],
                'weibo_id': myself['weibo_id']
            };
            
            var participators = this.get('participators');
            participators.push(newParticipator);
            
            this.set({'participators': participators});
            this.trigger('addParticipatorCallback');
        }
    });

});

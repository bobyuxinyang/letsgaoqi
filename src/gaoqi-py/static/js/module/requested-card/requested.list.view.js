/**
 * Created by PyCharm.
 * User: meteor1211
 * Date: 11-11-24
 * Time: 上午11:26
 * To change this template use File | Settings | File Templates.
 */

define(function (require) {
    var RquestedView = require('module/requested-card/requested.view');
    var service = require('utility/service');

    return Backbone.View.extend({
        tagName: "div",
        events: {
        },

        initialize: function () {
            _.bindAll(this, 'addOne', 'render');
            
        },
        render: function () {
            this.model.each(this.addOne);

            return this;
        },
        addOne: function (e) {
            var view = new RquestedView({ model: e });
            var element = view.render().el;
            $(this.el).append(element);
        },

        clear: function (e) {

        }

    });

});

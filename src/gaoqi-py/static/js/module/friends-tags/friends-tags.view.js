define(function (require) {
    require('module/friends-tags/friends-tag');
    var FriendsTagView = require('module/friends-tags/friends-tag.view');

    return Backbone.View.extend({
        tagName: "div",
        //template: _.template($("#tpl-friends-tags").html()),
        template: '<h1>朋友们对这些事情感兴趣</h1><ul class="tag-list"></ul>',
        events: {
        },
        initialize: function () {
            $(this.el).prop('id', 'friends-tags');

            _.bindAll(this, 'addOne', 'render');
            this.model.bind('getAll', this.render);
        },
        render: function () {
            $(this.el).html(this.template);
            this.model.each(this.addOne);
            return this;
        },
        addOne: function (e) {
            var $el = $(this.el);
            var tagView = new FriendsTagView({model: e});
            $el.find('.tag-list').append(tagView.render().el);
        },
        refresh: function () {
            this.model.getAll();
        }
    });
});
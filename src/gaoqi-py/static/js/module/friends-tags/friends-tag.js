define(function (require) {

    var service = require('utility/service');
    window.friends_tag = Backbone.Model.extend({
        initialize: function () {

        },
        chooseTag: function (status) {
            var _self = this;
            service.choose_one_tag(function (e) {
                    if (e.Status == 'Success') {
                        $(document).trigger('friendsTagsChanged');
                        $(document).trigger('mytagsChanged');
                        return;
                    }
                window.location = '/';
                }, {tags: JSON.stringify({
                                            name: _self.get('tagname'), status: status
                                        })
               });
        }
    });
});
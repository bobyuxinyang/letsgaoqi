define(function (require) {


    return Backbone.View.extend({
        tagName: "li",
        template: _.template(
            '<div class="tag-title">' +
                '<span class="tag-label tag-active-label"><%= tagname %></span>' +
                // '<a class="tag-normal">我也感兴趣</a>' +
                '<a class="tag-active">有活动叫上我</a>' +
            '</div>' +
            '<div class="tag-act">' +
                '<a class="btn-new-activity">搞起活动!</a>' +
            '</div>' +
            '<div class="active members-list">' +
                '<span class="title">他们最近想参加活动(<%= active_count %>)</span>' +
                '<ul class="active-members-list"></ul>' +
            '</div>' +
            '<div class="normal members-list">' +
                '<span class="title">他们对这个活动感兴趣(<%= normal_count %>)</span>' +
                '<ul class="normal-members-list"></ul>' +
            '</div>'
        ),
        activeFriendTemplate: _.template(
            '<li>' +
                '<a class="name"><img class="icon" width="48" height="48" src="<%= profile_image_url %>"><br><span class="name"><%= username %></span></a>' +
            '</li>'
        ),
        normalFriendTempalte: _.template(
            '<li>' +
                '<a class="name"><img class="icon" width="36" height="36" src="<%= profile_image_url %>" /></a>' +
            '</li>'
        ),
        showAllTemplate: '<li class="show-all"><a class="btn-show-all">显示全部...</a></li>',
        closeAllTempalte: '<li class="close-all"><a class="btn-show-all">收起</a></li>',
        activeMaxCount: 5,
        normalMaxCount: 10,
        events: {
            
        },

        initialize: function () {
            var $self = $(this);
            var $el = $(this.el);
            _.bindAll(this, 'render' );
            $el.addClass('tag');
        },

        render: function () {
            var _self = this;
            var $el = $(this.el);
            $el.html(this.template(this.model.toJSON()));

            $act = $el.find('.tag-act');
            $act.find('.btn-new-activity').click( function () {
                $(document).trigger('newActivity');
            });

            this.renderStatusLabel();

            this.renderFriendsList(_self.activeFriendTemplate, 'active', _self.activeMaxCount, _self.activeMaxCount);
            this.renderFriendsList(_self.normalFriendTempalte, 'normal', _self.normalMaxCount, _self.normalMaxCount);

            return this;
        },

        renderStatusLabel: function () {
            var _self = this;
            var $el = $(this.el);
            $title = $el.find('.tag-title');
            var myStatus = this.model.get('my_status');
            var labelClassed = {
                '0': 'tag-normal-label',
                '1': 'tag-active-label',
                '-1': 'tag-none-label'
            };
            $title.find('.tag-label').addClass(labelClassed[myStatus]);
            if (myStatus == 1) {
                $title.find('.tag-active').remove();
                $title.find('.tag-normal').remove();
            }
            if (myStatus == 0) {
                $title.find('.tag-normal').remove();
            }

            $title.find('.tag-active').click(function () {
                _self.model.chooseTag(1);
            });

            $title.find('.tag-normal').click(function () {
                _self.model.chooseTag(0);
            });

        },
        renderFriendsList: function (template, type, maxCount, renderCount) {
            var _self = this;
            var $el = $(this.el);

            var list = this.model.get(type);
            var $memberlist = $el.find('.' + type + '-members-list');
            $memberlist.empty();
            if (list.length > 0) {
                var index = 0;
                _.each(list, function (friend) {
                    if (index < renderCount) {
                        if (index != 0 && index % maxCount == 0) {
                            $memberlist.append('</br>');
                        }
                        _self.renderFriend($memberlist,template, friend);
                    }
                    index ++;
                });

                if (index > renderCount) {
                    $showAll = $(_self.showAllTemplate);
                    $memberlist.append($showAll);
                    $showAll.click(function () {
                        _self.renderFriendsList(template, type, maxCount, 99999);
                    });
                }

                if (renderCount > maxCount) {
                    $closeAll = $(_self.closeAllTempalte);
                    $memberlist.append($closeAll);
                    $closeAll.click(function () {
                        _self.renderFriendsList(template, type, maxCount, maxCount);
                    });
                }

            } else {
                $el.find('.' + type).remove();
            }
        },

        renderFriend: function (memberlist, template, friendModel) {
            var $friend = template(friendModel);
            memberlist.append($friend);
        }

    });
});
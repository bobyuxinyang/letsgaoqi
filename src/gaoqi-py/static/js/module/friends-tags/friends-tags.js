define(function (require) {
    var service = require('utility/service');
    require('module/friends-tags/friends-tag');

    window.FriendsTags = Backbone.Collection.extend({
        initialize: function () {
            this.getAll();
        },
        model: friends_tag,
        getAll: function () {
            var $self = this;

//            var mockdata = {
//                tags: [
//                    {
//                        tagname: '爬山',
//                        active_count: 20,
//                        normal_count: 15,
//                        my_status: 1,  // 我把这个tag列为有空叫我
//                        active: [
//                              {'username': '\u5546\u514b\u4f1f', 'weibo_id': 1805678935, 'user_id': '66666', 'update_at': 1320722282, 'profile_image_url': 'http://tp4.sinaimg.cn/1805678935/50/5598837336/1'}
//                            , {'username': '\u738b\u56fd\u794e', 'weibo_id': 1044856997, 'user_id': '77777', 'update_at': 1320722282, 'profile_image_url': 'http://tp2.sinaimg.cn/1044856997/50/5596850838/1'}
//                            , {'username': '\u5218\u5fd9\u7684\u5f88', 'weibo_id': 2294476291, 'user_id': '88888', 'update_at': 1320722282, 'profile_image_url': 'http://tp4.sinaimg.cn/2294476291/50/5615508966/1'}
//                            , {'username': 'XDash', 'weibo_id': 1649750430, 'user_id': '99999', 'update_at': 1320722282, 'profile_image_url': 'http://tp3.sinaimg.cn/1649750430/50/5615079347/1'}
//                            , {'username': '\u65b9\u55bbmichael', 'weibo_id': 1824717200, 'user_id': '111110', 'update_at': 1320722282, 'profile_image_url': 'http://tp1.sinaimg.cn/1824717200/50/5613869299/1'}
//                            , {'username': '\u6768\u5927\u603b\u7ba1', 'weibo_id': 1650780645, 'user_id': '122221', 'update_at': 1320722282, 'profile_image_url': 'http://tp2.sinaimg.cn/1650780645/50/5604647403/1'}
//                            , {'username': '\u7f8e\u56e2\u7f51\u738b\u5174', 'weibo_id': 1616192700, 'user_id': '133332', 'update_at': 1320722282, 'profile_image_url': 'http://tp1.sinaimg.cn/1616192700/50/1274177755/1'}
//                        ],
//                        normal: [
//                              {'username': '\u7eaf\u94f6V', 'weibo_id': 1134424202, 'user_id': '22222', 'update_at': 1320722282, 'profile_image_url': 'http://tp3.sinaimg.cn/1134424202/50/5611794146/1'}
//                            , {'username': 'ooyor', 'weibo_id': 1897964810, 'user_id': '33333', 'update_at': 1320722282, 'profile_image_url': 'http://tp3.sinaimg.cn/1897964810/50/5613485277/1'}
//                            , {'username': '\u805a\u670b\u7f51', 'weibo_id': 2326273650, 'user_id': '44444', 'update_at': 1320722282, 'profile_image_url': 'http://tp3.sinaimg.cn/2326273650/50/5609114605/1'}
//                            , {'username': '\u98de\u9c7c', 'weibo_id': 1597341101, 'user_id': '55555', 'update_at': 1320722282, 'profile_image_url': 'http://tp2.sinaimg.cn/1597341101/50/5611245448/1'}
//                            , {'username': '\u5546\u514b\u4f1f', 'weibo_id': 1805678935, 'user_id': '66666', 'update_at': 1320722282, 'profile_image_url': 'http://tp4.sinaimg.cn/1805678935/50/5598837336/1'}
//                            , {'username': '\u738b\u56fd\u794e', 'weibo_id': 1044856997, 'user_id': '77777', 'update_at': 1320722282, 'profile_image_url': 'http://tp2.sinaimg.cn/1044856997/50/5596850838/1'}
//                        ]
//                    },
//                    {
//                        tagname: '唱卡拉OK',
//                        active_count: 0,
//                        normal_count: 4,
//                        my_status: 0,  // 我只是感兴趣
//                        active: [],
//                        normal: [
//                              {'username': '\u7aa6\u5f3afromsmth', 'weibo_id': 2101784751, 'user_id': '144443', 'update_at': 1320722282, 'profile_image_url': 'http://tp4.sinaimg.cn/2101784751/50/5607519936/1'}
//                            , {'username': '\u8bb8\u6590', 'weibo_id': 1711672842, 'user_id': '155554', 'update_at': 1320722282, 'profile_image_url': 'http://tp3.sinaimg.cn/1711672842/50/5601254143/0'}
//                            , {'username': '\u51ef\u9e4f\u534e\u76c8KPCB\u5b98\u65b9\u5fae\u535a', 'weibo_id': 1980878051, 'user_id': '166665', 'update_at': 1320722282, 'profile_image_url': 'http://tp4.sinaimg.cn/1980878051/50/1298362663/0'}
//                            , {'username': '\u94c1\u8def\u4e0a\u6d77\u7ad9', 'weibo_id': 1917205532, 'user_id': '177776', 'update_at': 1320722282, 'profile_image_url': 'http://tp1.sinaimg.cn/1917205532/50/1296027581/1'}
//                            , {'username': '\u8bb8\u6590', 'weibo_id': 1711672842, 'user_id': '155554', 'update_at': 1320722282, 'profile_image_url': 'http://tp3.sinaimg.cn/1711672842/50/5601254143/0'}
//                            , {'username': '\u51ef\u9e4f\u534e\u76c8KPCB\u5b98\u65b9\u5fae\u535a', 'weibo_id': 1980878051, 'user_id': '166665', 'update_at': 1320722282, 'profile_image_url': 'http://tp4.sinaimg.cn/1980878051/50/1298362663/0'}
//                            , {'username': '\u94c1\u8def\u4e0a\u6d77\u7ad9', 'weibo_id': 1917205532, 'user_id': '177776', 'update_at': 1320722282, 'profile_image_url': 'http://tp1.sinaimg.cn/1917205532/50/1296027581/1'}
//                            , {'username': '\u8bb8\u6590', 'weibo_id': 1711672842, 'user_id': '155554', 'update_at': 1320722282, 'profile_image_url': 'http://tp3.sinaimg.cn/1711672842/50/5601254143/0'}
//                            , {'username': '\u51ef\u9e4f\u534e\u76c8KPCB\u5b98\u65b9\u5fae\u535a', 'weibo_id': 1980878051, 'user_id': '166665', 'update_at': 1320722282, 'profile_image_url': 'http://tp4.sinaimg.cn/1980878051/50/1298362663/0'}
//                            , {'username': '\u94c1\u8def\u4e0a\u6d77\u7ad9', 'weibo_id': 1917205532, 'user_id': '177776', 'update_at': 1320722282, 'profile_image_url': 'http://tp1.sinaimg.cn/1917205532/50/1296027581/1'}
//                            , {'username': '\u8bb8\u6590', 'weibo_id': 1711672842, 'user_id': '155554', 'update_at': 1320722282, 'profile_image_url': 'http://tp3.sinaimg.cn/1711672842/50/5601254143/0'}
//                            , {'username': '\u51ef\u9e4f\u534e\u76c8KPCB\u5b98\u65b9\u5fae\u535a', 'weibo_id': 1980878051, 'user_id': '166665', 'update_at': 1320722282, 'profile_image_url': 'http://tp4.sinaimg.cn/1980878051/50/1298362663/0'}
//                            , {'username': '\u94c1\u8def\u4e0a\u6d77\u7ad9', 'weibo_id': 1917205532, 'user_id': '177776', 'update_at': 1320722282, 'profile_image_url': 'http://tp1.sinaimg.cn/1917205532/50/1296027581/1'}
//                        ]
//                    },
//                    {
//                        tagname: '桌游',
//                        active_count: 4,
//                        normal_count: 0,
//                        my_status: -1,  // 我不感兴趣
//                        active: [
//                              {'username': '\u5546\u514b\u4f1f', 'weibo_id': 1805678935, 'user_id': '66666', 'update_at': 1320722282, 'profile_image_url': 'http://tp4.sinaimg.cn/1805678935/50/5598837336/1'}
//                            , {'username': '\u738b\u56fd\u794e', 'weibo_id': 1044856997, 'user_id': '77777', 'update_at': 1320722282, 'profile_image_url': 'http://tp2.sinaimg.cn/1044856997/50/5596850838/1'}
//                            , {'username': '\u5218\u5fd9\u7684\u5f88', 'weibo_id': 2294476291, 'user_id': '88888', 'update_at': 1320722282, 'profile_image_url': 'http://tp4.sinaimg.cn/2294476291/50/5615508966/1'}
//                            , {'username': 'XDash', 'weibo_id': 1649750430, 'user_id': '99999', 'update_at': 1320722282, 'profile_image_url': 'http://tp3.sinaimg.cn/1649750430/50/5615079347/1'}
//                        ],
//                        normal: []
//                    }
//                ]
//            };
//
//            this.add( mockdata.tags);
//
//            setTimeout(function() {
//                $self.trigger('getAll');
//            }, 50);

            var $self = this;

            $.ajaxSetup({cache: false});
            service.get_friends_tags(function (e) {
                $self._reset();
                $self.add(e.tags);
                $self.trigger('getAll');

                $.ajaxSetup({cache: true});
            });
        }
    });
});

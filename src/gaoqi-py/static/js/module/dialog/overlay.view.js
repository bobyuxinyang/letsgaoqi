﻿define(function (require) {
    var selector = require('utility/gaoqi-datetime-selector');

    return Backbone.View.extend({
        //... is a list tag.
        tagName: "div",
        el: '<div id="overlay" class="overlay"></div>',
        title: null,
        isClickToClose: false,
        initialize: function (isClickToClose) {
            if (isClickToClose) {
                this.isClickToClose = isClickToClose;
            }
        },
        render: function () {
            var $self = this;
            if ($('#overlay').length == 0){
                $($self.el).appendTo('body').fadeIn('slow');
            }
            if ($self.isClickToClose) {
                $('#overlay').click(function () {
                    $self.hideOverlay();
                });
            }
        },
        hide: function () {
            var $self = this;
            if ($('#overlay').length > 0){
                $('#overlay').fadeOut('slow');

                setTimeout(function () {
                    $('#overlay').remove();
                }, 200);
            }
        }
    });
});
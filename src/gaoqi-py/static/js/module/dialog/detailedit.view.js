﻿define(function (require) {
    var OverlayView = require('module/dialog/overlay.view');
    
    return Backbone.View.extend({
        //... is a list tag.
        tagName: "div",
        template: _.template($("#detail-edit-container").html()),
        events: {
          'click .close': 'remove'
          ,'click #editDetail': 'updateDetail'
        },
        overlayView: new OverlayView(),
        title: null,
        initialize: function () {
            _.bindAll(this, 'render');
            
        },
        render: function (detail) {
            var $self = this;
            var $el = $(this.el);
            $el.html(this.template());
            $el.find('.txt-detail-dialog').val(detail.changeLineToBr().htmlDecode());

            $el.addClass('popup-block').appendTo('body');
            $el.css({
                  'top': (document.documentElement.clientHeight - $el.height()) / 3  + 'px',
                  'left': (document.documentElement.clientWidth - $el.width()) / 2 + 'px'
                });
            return this;
        },
        
        updateDetail: function(){
            var detail = $(this.el).find('.txt-detail-dialog').val().htmlEncode();
            this.editedCallback(detail);
            this.remove();
        },
        editedCallback: function(detail){
        },
        remove: function () {
            var $self = this;
            $($self.el).remove();
            $self.overlayView.hide();
        },
        clear: function () {

        }
    });
});
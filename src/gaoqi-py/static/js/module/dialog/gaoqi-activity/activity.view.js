/**
 * Created by PyCharm.
 * User: meteor1211
 * Date: 11-11-24
 * Time: 下午6:39
 * To change this template use File | Settings | File Templates.
 */

define(function (require) {
    return Backbone.View.extend({
        //... is a list tag.
        tagName: "li",
        template: _.template('<p><%= text %></p><span><%= plan_at_str %></span>'),
        events: {
        
        },
        initialize: function () {
            $(this.el).addClass('normal');
            
            _.bindAll(this, 'render');

            this.model.bind('change', this.render);
            this.model.view = this;
        },
        render: function () {
            $(this.el).html(this.template(this.model.toJSON()));
            
            return this;
        },

        remove: function () {
            $(this.el).remove();
        },
        clear: function () {

        }
    });
});

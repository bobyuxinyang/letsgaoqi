/**
 * Created by PyCharm.
 * User: meteor1211
 * Date: 11-11-24
 * Time: 下午6:39
 * To change this template use File | Settings | File Templates.
 */
define(function (require) {
    var ActivityView = require('module/dialog/gaoqi-activity/activity.view');

    return Backbone.View.extend({
        tagName: "ul",
        events: {
            'click li.new': 'create'
            ,'click li.normal': 'invite'
        },

        initialize: function () {
            _.bindAll(this, 'addOne', 'render');
            this.model.bind('getAll', this.render);
        },
        render: function () {
            this.model.each(this.addOne);

            //add new
            var $new = $('<li class="new"><p>创建新活动...</p></li>');
            $new.appendTo($(this.el));
            
            return this;
        },
        addOne: function (e) {
            var view = new ActivityView({ model: e });
            var element = view.render().el;
            $(this.el).append(element);
        },

        create: function(){
            this.trigger('newActivityCallback');
        },

        id_selected: null,
        invite: function(e){
            //activity_id
            var li = $(e.currentTarget);
            var model = this.model.models[li.index()];

            this.id_selected = model.get('activity_id');
            
            this.trigger('inviteCallback');
        },
        
        getSelectedAcitityId: function(){
            return this.id_selected;
        },
        clear: function (e) {

        }
    });

});
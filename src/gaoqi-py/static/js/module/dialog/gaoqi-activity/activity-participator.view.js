/**
 * Created by PyCharm.
 * User: meteor1211
 * Date: 11-11-24
 * Time: 下午5:36
 * To change this template use File | Settings | File Templates.
 */


define(function (require) {
    return Backbone.View.extend({
        //... is a list tag.
        tagName: "div",
        template: _.template('<div class="avatar_grid_small">' +
                                '<img src="<%= user.profile_image_url %>" style="width:36px; height:36px"/>' +
                             '</div>' +
                             '<div class="user_name"><%= user.username %></div>'),
        events: {

        },
        initialize: function () {
            $(this.el).addClass('invite_grid selected').attr({'title': '点击取消选中'});

            _.bindAll(this, 'render');
            this.model.bind('change', this.render);
            this.model.view = this;
        },
        render: function () {
            $(this.el).html(this.template(this.model.toJSON()));
            $(this.el).data('is_selected', true);

            return this;
        },
       
        remove: function () {
            $(this.el).remove();
        },
        clear: function () {

        }
    });
});

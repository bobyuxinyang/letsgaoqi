/**
 * Created by PyCharm.
 * User: meteor1211
 * Date: 11-11-24
 * Time: 下午5:36
 * To change this template use File | Settings | File Templates.
 */

//views for "我来搞起 邀请的朋友"
//collection: participatorlist
//model: Participator
define(function (require) {
    var ParticipatorView = require('module/dialog/gaoqi-activity/activity-participator.view');

    return Backbone.View.extend({
        tagName: "div",
        events: {
             'click .invite_grid': 'select'
        },

        initialize: function () {
            _.bindAll(this, 'addOne', 'render');
            this.model.bind('getAll', this.render);
        },
        render: function () {
            this.model.each(this.addOne);
            return this;
        },
        addOne: function (e) {
            var view = new ParticipatorView({ model: e });
            var element = view.render().el;
            $(this.el).append(element);
        },

        select:function(e){
            var target = $(e.currentTarget);
            var is_selected = target.data('is_selected');

            if(is_selected){
                target.removeClass('selected');
                target.data('is_selected', false);

                this.model.removeOne(target.index());
            }
            else {
                target.addClass('selected');
                target.data('is_selected', true);

                this.model.addOne(target.index());
            }
        },
        clear: function (e) {
            
        },

        getParticipators: function(){
            var list = this.model.getParticipators();

            return list;
        }


    });

});
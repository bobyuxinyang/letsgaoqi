/**
 * Created by PyCharm.
 * User: yxch
 * Date: 11-11-23
 * Time: 下午9:00
 * To change this template use File | Settings | File Templates.
 */
define(function (require) {
    var selector = require('utility/gaoqi-datetime-selector');
    var service = require('utility/service');

    var OverlayView = require('module/dialog/overlay.view');
    var ParticipatorsView = require('module/dialog/gaoqi-activity/activity-participators.view');
    var ActivitiesView = require('module/dialog/gaoqi-activity/activities.view');
    
    require('model/participatorlist');
    require('module/activity-card/activity-card-list');

    return Backbone.View.extend({
        tagName: "div",
        template: _.template($("#tpl-gaoqi-activity").html()),
        events: {
            'click .close': 'remove'
        },
        overlayView: new OverlayView(),



        views: {
            participatorsView: null,
            activitiesView: null
        },
        models:{
            participators : [],
            activities: [],
            tags: [],
            text: '',
            plan_at: ''
        },


        
        initialize: function (args) {
            this.models.participators =  args['participators'];
            this.models.activities =  args['activities'];
            this.models.text = args['text'];
            this.models.plan_at = args['plan_at'];
            this.models.tags = args['tags'];
            
            _.bindAll(this, 'render');
        },

        renderParticipators: function(){
            var _self = this;
            var participatorsView = this.views.participatorsView = new ParticipatorsView({
                model: new ParticipatorList(_self.models.participators)
            });
            participatorsView.render();
            $(this.el).find('.friends-list').html(participatorsView.el);
        },


        renderActivities: function(){
            var _self = this;
            var activitiesView = _self.views.activitiesView  = new ActivitiesView({
                model: new AcitivityList(_self.models.activities)
            });
            activitiesView.render();
            $(_self.el).find('.activities-list').append(activitiesView.el);

            //邀请好友参加自己参加的活动
            _self.views.activitiesView.bind('inviteCallback', function(){
                //get participators
                var idList = _self.views.participatorsView.getParticipators();
                var activity_id = _self.views.activitiesView.getSelectedAcitityId();

                alert(idList);
                alert(activity_id)

                _self.inviteFriends(idList, activity_id);

                _self.remove();
            });


            //邀请好友参加新的活动
            _self.views.activitiesView.bind('newActivityCallback', function(){
                var idList = _self.views.participatorsView.getParticipators();

                alert(idList);
                alert(_self.models.text);
                alert(_self.models.plan_at);
                alert(_self.models.tags);

                _self.createNewActivity();

                _self.remove();
            });

        },


        inviteFriends: function(idList, activity_id){
            //todo： 邀请参加已有活动
        },
        createNewActivity: function(){
            //todo: 邀请参加新活动
        },

        render: function () {
            var $self = this;
            var $el = $(this.el);
            $el.html(this.template());

            $self.overlayView.render();

            $el.addClass('popup-block')
                .width('600')
                .appendTo('body');

            $el.css({
                  'top': (document.documentElement.clientHeight - $el.height()) / 5  + 'px',
                  'left': (document.documentElement.clientWidth - $el.width()) / 2 + 'px'
                });


            $self.renderParticipators();
            $self.renderActivities();

            return this;
        },

        remove: function () {
            var $self = this;
            $($self.el).remove();
            $self.overlayView.hide();
        },
        clear: function () {

        }
    });
});
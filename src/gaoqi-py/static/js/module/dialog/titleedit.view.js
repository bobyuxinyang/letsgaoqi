﻿define(function (require) {
    var OverlayView = require('module/dialog/overlay.view');
    return Backbone.View.extend({
        //... is a list tag.
        tagName: "div",
        template: _.template($("#title-edit-container").html()),
        events: {
          'click .close': 'remove'
          ,'click #editTitle': 'updateTitle'
        },
        title: null,
        overlayView: new OverlayView(),
        initialize: function () {
            _.bindAll(this, 'render');
            
        },
        render: function (title) {
            var $self = this;
            var $el = $(this.el);
            $el.html(this.template());
            $el.find('.txt-title-dialog').val(title);

            $el.find('.txt-title-dialog').artTxtCount({
                btnOnCallback: function () {
                    $('#editTitle').attr('disabled', false);
                },
                btnOffCallback: function () {
                    $('#editTitle').attr('disabled', true);
                }
            });
            
            $el.addClass('popup-block').appendTo('body');
            $el.css({
                  'top': (document.documentElement.clientHeight - $el.height()) / 3  + 'px',
                  'left': (document.documentElement.clientWidth - $el.width()) / 2 + 'px'
                });
            return this;
        },
        
        updateTitle: function(){
            var title = $(this.el).find('.txt-title-dialog').val().htmlEncode().trim();
            this.editedCallback(title);
            this.remove();
        },
        editedCallback: function(title){
        },
        remove: function () {
            var $self = this;
            $($self.el).remove();
            $self.overlayView.hide();
        },
        clear: function () {

        }
    });
});
define(function (require) {
    var OverlayView = require('module/dialog/overlay.view');
    var selector = require('utility/gaoqi-datetime-selector');
    var service = require('utility/service');

    return Backbone.View.extend({
        tagName: "div",
        template: _.template($("#tpl-new-activity").html()),
        events: {
            'click .close': 'remove',
            'click #gaoqi-main': 'btnGaoqiMainCallback'
        },
        overlayView: new OverlayView(),
        initialize: function () {
            console.log('initialize new activity');
            _.bindAll(this, 'render');
        },
        render: function (title) {
            console.log('new activity');
            var $self = this;
            var $el = $(this.el);
            $el.html(this.template());

            $self.overlayView.render();

            $el.find('#txt-title')
                .focusin(function () {
                    $el.find('#txt-count-remind').show();
                })
                .focusout(function () {
                    $el.find('#txt-count-remind').hide();
                })
                .artTxtCount({
                    max: 50,
                    btnOnCallback: function () {
                        $el.find('#gaoqi-main').attr('disabled', false)
                                        .removeClass('disabled');
                    },
                    btnOffCallback: function () {
                        $el.find('#gaoqi-main').attr('disabled', true)
                                        .addClass('disabled')
                    },
                    textAmongCallback: function (e, count) {
                        var countDom = '<span class="count-among">{count}</span>'.replace('{count}', count);
                        $el.find('#txt-count-remind').html("请文明发言，还可以输入 " + countDom + " 字");
                    },
                    textOverCallback: function (e, count) {
                        var countDom = '<span class="count-over">{count}</span>'.replace('{count}', count);
                        $el.find('#txt-count-remind').html("请文明发言，已经超过 " + countDom + " 字");
                    }
                });

            selector.initSelector(
                $el.find('#startDate'),
                $el.find('#startTime'),
                $el.find('#txt-dateDiff'));
            selector.setDateDiff();

            $el.addClass('popup-block')
                .width('600')
                .appendTo('body');
            $el.css({
                  'top': (document.documentElement.clientHeight - $el.height()) / 5  + 'px',
                  'left': (document.documentElement.clientWidth - $el.width()) / 2 + 'px'
                });
            
            return this;
        },

        remove: function () {
            var $self = this;
            $($self.el).remove();
            $self.overlayView.hide();
        },
        clear: function () {

        },
        focus: function(text) {
            var $el = $(this.el);
            if (text) {
                $el.find('#txt-title').val(text).select();
            }
            $el.find('#txt-title').focus();
        },

        update: function () {
            var $self = this;
            var $el = $(this.el);
            var participators = [];
            var params = {
                text: $el.find('#txt-title').val().htmlEncode()
                    , plan_at: selector.getDateTime().toEpoch()
                    , invitePeople: JSON.stringify(participators)
            };
            service.update_activity(function (e) {
                if (e.Status == 'Success') {
                    var url = window.location.href = "/activity?id=" + e.Data;
                    $.redirectAndDisplayInfo(url, e)
                }
            }, params);

        },



        btnGaoqiMainCallback: function () {
            var $self = this;
            var $el = $(this.el);

            $el.find('#gaoqi-main').attr('disabled', true)
                                   .addClass('disabled');
            $self.update();
        }
    });
});
define(function (require) {
    return Backbone.View.extend({
        //... is a list tag.
        tagName: "div",
        template: _.template('<div class="delete"> × </div><div class="title"><%= name %></div>'),
        events: {
            'click .delete': 'remove'
        },
        initialize: function () {
            $(this.el).addClass('tag');

            _.bindAll(this, 'render');
            this.model.bind('change', this.render);
            this.model.view = this;
        },
        render: function () {
            $(this.el).html(this.template(this.model.toJSON()));

            return this;
        },
        remove: function () {
            $(this.el).remove();
            this.model.remove();
        }
    });
});
/**
 * Created by PyCharm.
 * User: meteor1211
 * Date: 11-11-23
 * Time: 下午6:16
 * To change this template use File | Settings | File Templates.
 */
define(function (require) {
    var OverlayView = require('module/dialog/overlay.view');
    var selector = require('utility/gaoqi-datetime-selector');
    var service = require('utility/service');

    require('utility/string');
    require('utility/gaoqi-displaystatusinfo');

    var TagsView = require('module/dialog/beg-activity/tag.list.view');
    require('module/mytags/mytags');

    var staticSource = {
        non_conent_text : '还没有神马好想法...'
    };


    return Backbone.View.extend({
        tagName: "div",
        template: _.template($("#tpl-beg-activity").html()),
        events: {
            'click .close': 'remove',
            'click .btn-submit': 'submit',
            'click .form-element': 'focusInTitle',
            'click .no-content': 'focusInTitle'
        },

        overlayView: new OverlayView(),
        tagsView: null,

        initialize: function () {
            _.bindAll(this, 'render');
        },

        renderTags: function(){
            var tagsView = this.tagsView = new TagsView( { model: new Mytags() } );
            $(this.el).find('.tags-list').append(tagsView.el);

        },
        
        render: function () {
            var $self = this;
            var $el = $(this.el);
            $el.html(this.template());
            $self.overlayView.render();

            $self.renderTags();

            $el.addClass('popup-block')
                .width('600')
                .appendTo('body');

            selector.initSelector($el.find('#startDate'), null, $el.find('#txt-dateDiff'));
            selector.setDateDiff();

            $($el).find('#txt-title').artTxtCount({
                btnOnCallback: function () {
                    $(this).parent().prev().css({'visibility': 'hidden'});
                },
                btnOffCallback: function () {
                    $(this).parent().prev().css({'visibility': 'visible'});
                }
            });

            $el.css({
                'top': (document.documentElement.clientHeight - $el.height()) / 5  + 'px',
                'left': (document.documentElement.clientWidth - $el.width()) / 2 + 'px'
            });

            return this;
        },

        focusInTitle:function(e){
            $(this.el).find('#txt-title').focus();
        },


        submit: function(){
            var tagList = this.tagsView.model.getTagsList();
            var plan_at = selector.getDateTime().toEpoch();
            var text = $(this.el).find('#txt-title').val().trim().htmlEncode();
            text = text.length > 0 ? text : staticSource.non_conent_text;
                
            alert(tagList);
            alert(plan_at);
            alert(text);

            this.begActivity();
            this.remove();

            //重新render 自己的活动列表
            $(document).trigger('newRequestedCallback');
        },

        begActivity: function(){
            //todo: 求活动
            $.displayStatusInfo({Info: '求活动成功!!'});
        },

        remove: function(){
            var $self = this;
            $($self.el).remove();
            $self.overlayView.hide();
        },
        clear: function () {

        }
    });
});
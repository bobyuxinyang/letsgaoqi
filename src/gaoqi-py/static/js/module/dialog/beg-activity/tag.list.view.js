//views for 求活动时的tags
//collection: mytags
//model: mytag
define(function (require) {
    var TagView = require('module/dialog/beg-activity/tag.view');

    return Backbone.View.extend({
        tagName: "div",
        events: {

        },

        initialize: function () {
            _.bindAll(this, 'addOne', 'render');
            this.model.bind('getAll', this.render);
        },
        render: function () {
            this.model.each(this.addOne);
            return this;
        },
        addOne: function (e) {
            var view = new TagView({ model: e });
            var element = view.render().el;
            $(this.el).append(element);
        },

        clear: function (e) {

        }

    });

});
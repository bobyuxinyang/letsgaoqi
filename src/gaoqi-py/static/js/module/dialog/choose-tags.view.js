define(function (require) {
    var OverlayView = require('module/dialog/overlay.view');
    var service = require('utility/service');
    var TagSelView = require('module/tag-sel/tag-sel.view');

    require('module/tag-sel/activity-tags');
    require('utility/gaoqi-displaystatusinfo');
    
    return Backbone.View.extend({
        tagName: 'div',
        template: _.template($('#tpl-choose-tags').html()),
        events: {
            'click .close': 'remove',
            'click #btn-submit': 'btnSubmitCallback'
        },
        overlayView: new OverlayView(),
        tagSelView : null,
        initialize:function () {
            _.bindAll(this, 'render');
        },
        remove: function () {
            var $self = this;
            $($self.el).remove();
            $self.overlayView.hide();
        },
        render: function () {
            var $self = this;
            var $el = $(this.el);
            $el.html(this.template());
            $self.overlayView.render();

            // tagSelView
            this.tagSelView = new TagSelView( {model: new activity_tags()} ),
            this.tagSelView.chooseTagsChangedCallback = function (tags) {
                $self.updateBtnSubmit();
            };
            $el.find('.tag-sel-holder').append(this.tagSelView.el);

            this.tagSelView.setChoosedTags($self.model.models);

            // btn-ok
            this.updateBtnSubmit();

            // popup-block
            $el.addClass('popup-block')
                .width('730')
                .appendTo('body');
            $el.css({
                  'top': (document.documentElement.clientHeight - $el.height()) / 5  + 'px',
                  'left': (document.documentElement.clientWidth - $el.width()) / 2 + 'px'
                });
            return this;
        },

        updateBtnSubmit: function () {
            var $self = this;
            var $el = $(this.el);
            var len = this.tagSelView.getChoosedTags().length;

            if (len == 0) {
                $el.find('#btn-submit').addClass('disabled');
            }else{
                $el.find('#btn-submit').removeClass('disabled');
            }
        },
        btnSubmitCallback: function () {
            var $self = this;
            var choosedTags = this.tagSelView.getChoosedTags();
            if (choosedTags.length == 0) return;
            service.update_user_activity_tags(function () {
                $self.remove();

                $.displayStatusInfo({Info: '更新成功！'})
                $(document).trigger('mytagsChanged');
//                $(document).trigger('friendsTagsChanged');
            }, { tags : JSON.stringify(choosedTags)});
        }
    });
})
﻿define(function (require) {
    var selector = require('utility/gaoqi-datetime-selector');
    var OverlayView = require('module/dialog/overlay.view');

    return Backbone.View.extend({
        //... is a list tag.
        tagName: "div",
        template: _.template($("#time-edit-container").html()),
        events: {
          'click .close': 'remove'
          ,'click #editTime': 'updateTime'
        },
        title: null,
        overlayView: new OverlayView(),
        initialize: function () {
            _.bindAll(this, 'render');
            
        },
        render: function (plan_at) {
            var $self = this;
            var $el = $(this.el);
            var dateTime = Date.fromEpoch(plan_at);
            
            $el.html(this.template());
            
            var dateDom = $el.find('#startDate').val(dateTime.toFullDate());
            var timeDom = $el.find('#startTime').val(dateTime.toShortTime());
            var diffDom = $el.find('#txt-dateDiff').html(dateTime.DateDiff());

            selector.datetime = dateTime; 
            selector.initSelector(dateDom, timeDom, diffDom);    

            $el.addClass('popup-block').appendTo('body');
            $el.css({
                  'top': (document.documentElement.clientHeight - $el.height()) / 3  + 'px',
                  'left': (document.documentElement.clientWidth - $el.width()) / 2 + 'px'
                });

            return this;
        },
        
        updateTime: function(){
            var time = selector.getDateTime();
            this.editedCallback(time);
            this.remove();
        },
        editedCallback: function(time){
        },
        remove: function () {
            var $self = this;
            $($self.el).remove();
            $self.overlayView.hide();
        },
        clear: function () {

        }
    });
});
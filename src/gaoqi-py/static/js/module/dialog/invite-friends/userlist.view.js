define(function (require) {
    var UserView = require('module/dialog/invite-friends/user.view');
    

    return Backbone.View.extend({
        tagName: "div",
        events: {
        },
        currentIndex: -1,

        initialize: function () {
            $(this.el).addClass('selector-renderer');

            _.bindAll(this, 'addOne', 'render');
            this.model.bind('getAll', this.render);
        },
        render: function () {
            var $self = this;
            console.log('render userlist');
            $($self.el).empty();
            $self.currentIndex = -1;

            $self.model.each($self.addOne);

            if ($self.model.length > 0) {
                $($self.el).show();
            }else{
                $($self.el).hide();
            }

            

            $('.find-friends').unbind('userHovered').bind('userHovered', function (e, userModel){
                var index = $self.model.models.indexOf(userModel);
                if ($self.currentIndex != -1 && $self.currentIndex != index) {
                    $self.model.models[$self.currentIndex].unActiveRow();
                }
                $self.currentIndex = index;
            });

            return $self;
        },
        addOne: function (e) {
            var view = new UserView({ model: e });
            var element = view.render().el;
            $(this.el).append(element);
        },

        clear: function (e) {

        },

        updateKey: function (newKey) {
            this.model.updateKey(newKey);
        },

        hide: function () {
            $(this.el).hide();
            this.currentIndex = -1;
        },
        isDisplayed:function(){
            return $(this.el).css('display') != 'none'
        },

        pressKeyDown: function () {
            var $self = this;
            var length = $self.model.length;
            if (this.currentIndex != -1) {
                $self.model.models[$self.currentIndex].unActiveRow();
            }
            if (this.currentIndex == -1 || this.currentIndex == length - 1) {
                this.currentIndex = 0;
            }else{
                this.currentIndex ++;
            }
            $self.model.models[$self.currentIndex].activeRow();
        },
        pressKeyUp: function () {
            var $self = this;
            var length = $self.model.length;
            if (this.currentIndex != -1) {
                $self.model.models[$self.currentIndex].unActiveRow();
            }
            if (this.currentIndex == -1 || this.currentIndex == 0) {
                this.currentIndex = length - 1;
            }else{
                this.currentIndex --;
            }
            $self.model.models[$self.currentIndex].activeRow();
        },
        pressKeyEnter: function () {
            var $self = this;
            if ($self.currentIndex == -1) return;
            console.log('press enter at..' + $self.currentIndex);
            $self.model.models[$self.currentIndex].selected();
        }



    });

});
define(function (require) {
    return Backbone.View.extend({
        //... is a list tag.
        tagName: "div",
        template: _.template($("#user-selector-row").html()),
        events: {
            'mouseenter .selector-row': 'activeRow'
            , 'click .selector-row': 'selectUserCallback'
        },
        currentSelIndex: -1,
    initialize: function () {
        _.bindAll(this, 'render');
        this.model.bind('change', this.render);
        this.model.view = this;
    },
    render: function () {
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    },
    activeRow: function(){
        var $self = $(this.el);
        $self.addClass('selector-active');
        $('.find-friends').trigger('userHovered', this.model);
    },
    unActiveRow: function(){
        var $self = $(this.el);
        $self.removeClass('selector-active');
    },
    selectUserCallback: function(){
        $('.find-friends').trigger('userSelected', this.model);
    },
    remove: function () {
        $(this.el).remove();
    },
    clear: function () {

    }
});
});
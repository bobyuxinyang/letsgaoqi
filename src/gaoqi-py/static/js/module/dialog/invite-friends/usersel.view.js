define(function (require) {
    var UserListView = require('module/dialog/invite-friends/userlist.view');
    require('model/userlist');
    var OverlayView = require('module/dialog/overlay.view');

    var service = require('utility/service');

    return Backbone.View.extend({
        //... is a list tag.
        tagName: "div",
        template: _.template($("#user-sel-container").html()),
        events: {
            'click .close': 'remove',
            'focusin #txt-sinaweibo-search': 'sinweiboSearchFocusin',
            'blur #txt-sinaweibo-search': 'sinweiboSearchFocusout',
            'keydown #txt-sinaweibo-search': 'sinweiboSearchKeydown',
            'keydown .find-friends': 'userlistKeydownCallback',
            'click #btn-invite': 'btnInviteFriends'
        },
        activity_id: null,
        activity_name: null,
        friends_choosen: [],
        userListView: null,
        overlayView: null,
        _timer: null,

        initialize: function () {
            var $el = $(this.el);

            $el.prop('id', 'user-sel-container');
            $el.addClass('popup-block').appendTo('body');
            $el.css({
                  'top': (document.documentElement.clientHeight - $el.height()) / 5  + 'px',
                  'left': (document.documentElement.clientWidth - $el.width()) / 2 + 'px'
                });
            _.bindAll(this, 'render');
        },
        render: function (activity) {
            var $self = this;

            $self.activity_id = activity._id;
            $self.activity_name = activity.text;
            $($self.el).html($self.template(activity));


            $self.overlayView = new OverlayView();
            $self.overlayView.render();

            // deal with userListView
            $self.userListView = new UserListView({model: new UserList()});
            $($self.userListView.render().el).appendTo(
                $($self.el).find('.sinaweibo-search')
            ).hide();

            $($self.el).find('.find-friends').bind('userSelected', function(e, userModel){
                $self.userListView.hide();
                $('#txt-sinaweibo-search').val('');
                // console.log('selected: ' + userModel.get('username'));

                var participator = {
                    weibo_id: userModel.get('weibo_id'),
                    username: userModel.get('screen_name'),
                    profile_image_url: userModel.get('profile_image_url')
                }
                $self.sinaweiboUserSelectedCallback(participator);
            });

            $self.friends_choosen = [];
            $self.update_friends_choosen();
            
            return $self;
        },

        sinaweiboUserSelectedCallback: function (participator){
            var $self = this;
            //判断重复,并添加选择一个weibo_user
            var isDup = _.select($self.friends_choosen, function (item) {
                return item.weibo_id == participator.weibo_id
            }).length > 0;

            if (!isDup){
                $self.friends_choosen.push(participator);
                $self.update_friends_choosen();
            }
        },

        getInviteText: function (friends_choosen) {
            var $self = this;
            var result = '大家有空 "';
            result += $self.activity_name;
            result += '" 吧。 ';
            _.each($self.friends_choosen, function (item){
                result += '@' + item.username + ' ';
            });
            return result;
        },

        update_friends_choosen: function () {
            var $self = this;
            if ($self.friends_choosen.length > 0) {

                var friend_avtar_template =
                    '<div class="avatar_grid">' +
                        '<img title="<%= username %> (单击取消选择)" src="<%= profile_image_url %>" style="cursor: pointer;" class="img-participators recognized">' +
                    '</div>';

                $($self.el).find('.friends-choosen')
                    .show()
                    .find('.friends-list').empty();


                _.each($self.friends_choosen, function (item) {
                    var $weibo_user_el = $(_.template(friend_avtar_template)(item));
                    $weibo_user_el.find('.img-participators').data('weibo_id', item.weibo_id);
                    $($self.el).find('.friends-list').append($weibo_user_el);
                });


                // the remove event
                $($self.el).find('.img-participators').click(function (e) {
                    var weibo_id = $(e.currentTarget).data('weibo_id');
                    // 删除选择一个weibo_user
                    var user_select = _.select($self.friends_choosen, function (item) {
                        return item.weibo_id == weibo_id;
                    });
                    if (user_select.length > 0) {
                        var index = $self.friends_choosen.indexOf(user_select[0]);
                        $self.friends_choosen.splice(index, 1);

                        $self.update_friends_choosen();
                    }
                });

                // 生成一段邀请文字
                var invite_text = $self.getInviteText($self.friends_choosen);
                $($self.el).find('.weibo-content').val(invite_text);
                $($self.el).find('.operation').show();
            } else {
                $($self.el).find('.friends-choosen').hide();
                $($self.el).find('.operation').hide();
            }
        },

        userlistKeydownCallback: function(e){
            if(this.userListView.isDisplayed()){
                // press up
                if(e.keyCode == '38') {
                    this.userListView.pressKeyUp();
                }
                // press down
                if(e.keyCode == '40') {
                    this.userListView.pressKeyDown();
                }
                // press enter
                if(e.keyCode == '13'){
                    this.userListView.pressKeyEnter();
                }
            }
        },
        
        renderSinaweiboUsersSelector: function (weibo_users) {
            var _self = this;
            var len = weibo_users.length;
            var $selectorRendered = $(_self.el).find('.selector-renderer');
            if (len > 0) {
                $selectorRendered.show();
            }else{
                $selectorRendered.hide();
            }
        },

        sinweiboSearchFocusin: function () {
            var _self = this;
            var oldVal = '';
            this._timer = setInterval(function () {
                var newVal = $(_self.el).find('#txt-sinaweibo-search').val();
                if (newVal != oldVal) {
                    oldVal = newVal;

                    // console.log('change: ' + newVal);
                    if (newVal.length > 0) {
                        _self.userListView.updateKey(newVal);
                    }else{
                        _self.userListView.hide();
                    }
                }
            }, 30);
        },

        sinweiboSearchFocusout: function () {
            clearInterval(this._timer);
        },

        sinweiboSearchKeydown: function (e) {
            var $self = this;
            // press ESC
            if (e.keyCode == 27) {
                console.log('press esc');
                $self.userListView.hide();
                $('#txt-sinaweibo-search').val('');
            }
        },
        remove: function () {
            var $self = this;
            $self.overlayView.hide();
            $($self.el).remove();

        },
        clear: function () {

        },

        btnInviteFriends: function () {
            var $self = this;
            var isSendWeiboInvitation = $($self.el).find('#check-send').prop('checked');
            var inviteText = $($self.el).find('.weibo-content').val();

            $self.inviteFriendsCallback($self.friends_choosen, isSendWeiboInvitation, inviteText);
            $self.remove();
        },

        // 对外callback
        inviteFriendsCallback: function (weibo_users, isSendWeiboInvitation, inviteText ) {
            
        }

    });
});
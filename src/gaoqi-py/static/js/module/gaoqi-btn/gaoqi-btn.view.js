define( function (require) {

    
    return Backbone.View.extend({
        tagName: 'div',
        events: {
           'click .btn-gaoqi': 'btnGaoqiCallback'
        },
        template: '<div class="btn-holer"><button class="btn success btn-gaoqi">求活动</button></div>',

        initialize: function () {
            $(this.el).html(this.template);
        },

        btnGaoqiCallback: function () {
            $(document).trigger('begActivity');
        }
    });
})
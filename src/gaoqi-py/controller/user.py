#coding:utf-8
import json
from bson.objectid import ObjectId
from pymongo import json_util


import web
import datetime
from settings import db, gearman
from utility import auth
from utility.helper import return_json, render_template
from utility.auth import login_required, current_user_id
from utility import json_util, LT
import json

def get_user(user_id):
    user = db.user.find_one({'_id': ObjectId(user_id)})
    
    return user


def get_simple_user(user_id):
    #todo: get_simple_user
    user = db.user.find_one({'_id': ObjectId(user_id)},
                {'_id': 1, 'username': 1,  'description': 1, 'profile_image_url': 1, 'weibo_id': 1}
           )
    
    return user

def has_active_tags(user_id):
    u = db.user.find_one({"_id": ObjectId(user_id)}, {'tags':1})
    return u and len(u['tags']) > 0


class signin:
    @return_json
    def POST(self):
        i = web.input()
        email = i.email
        pwd = i.password

        user = db.user.find_one({'email_lower': email.lower()})

        # todo: md5 the pwd and check, 判断是否匹配
        if user and auth.md5pwd(pwd) == user['password']:

            #LOG_SIGNIN_NORMAL
            msg = "{user}: signin success".format(user=str(user['_id']))
            LT.TRACE(LT.SIGNIN_NORMAl, msg)
            ##################

            expir = datetime.datetime.now() + datetime.timedelta(days = 2)
            auth.auth_sign(str(user['_id']), expir)

            # update user friends
            fetch = True
            if "last_fetch_weibo_friends_at" in user:
                if datetime.datetime.now() - user['last_fetch_weibo_friends_at'] > datetime.timedelta(days=1):
                    fetch = False
            if fetch:
                gearman.submit_job("send_register_email", str(user['_id']), background=True, wait_until_complete=False)
            
            return json.dumps({'Status': 'Success'})
        else:
            print 'login faild'
            return json.dumps({'Status': 'Error'})

class signout:
    @return_json
    def POST(self):
        auth.auth_signout()
        return json.dumps({'Status': 'Success'})

#class get_user_detail:
#    @return_json
#    def GET(self):
#        pass
    
class get_current_user:
    @return_json
    @login_required
    def GET(self):
        user_id = current_user_id()
        user = get_user(user_id)
        return json.dumps(user, default = json_util.default)

class active:
    def GET(self, code):
        session = web.ctx.session
        u = db.user.find_one({"_id": ObjectId(code)})
        if u is None:
            web.seeother('/')
        if not u['is_signed_up']:
            u['is_signed_up'] = True
            db.user.save(u)
            session.username = u['username']

            expir = datetime.datetime.now() + datetime.timedelta(days = 2)
            auth.auth_sign(str(u['_id']), expir)

            #LOG_ACTIVE_MAILVALIDED
            msg = "{user}: MAIL ACTIVE".format(user=str(u['_id']))
            LT.TRACE(LT.ACTIVE_MAIL_VALIDED, msg)
            #######################
            
            # go and get following_people
            gearman.submit_job("get_following_people", str(u['_id']), background=True, wait_until_complete=False)

        web.seeother("/")

class tip:
    @login_required
    def POST(self):
        i = web.input()

        ret = db.activity.find_one({"_id": ObjectId(i.activity_id)}, {"participators": 1})
        u = filter(lambda x: x['user_id'] == ObjectId(i.user_id), ret['participators'])
        u = u[0]
        ou = db.user.find_one({"_id": ObjectId(i.user_id)})

        if 'invite_by' not in u:
            u['invite_by'] = None
            
        ru = {'status': u['status'],
              'profile_image_url': ou['profile_image_url'],
              'weibo_id': ou['weibo_id'],
              'invite_by': u['invite_by'],
              'username': ou['username']
              }

        return render_template('user_tip.html', u=ru)

class choose_tags:
    @return_json
    @login_required
    def POST(self):
        i = web.input()
        uid = current_user_id()
        newtags = json.loads(i.tags, object_hook = json_util.object_hook)
        u = db.user.find_one({"_id": ObjectId(uid)}, {'tags':1, "friends": 1})
        tags = []

        # update my tags
        for i in newtags:
            found = False
            for old in u['tags']:
                if i['name'] == old['name']:
                    found = True
                    tags.append(old)
                    break
            if not found:
                tags.append({"name": i['name'], "order": i['order'], "status": 0, "update_at": datetime.datetime.now()})

        db.user.update({"_id": ObjectId(uid)}, {"$set": {"tags": tags}})

        ####################################
        # update friend tags
        # use pull instead of push
        ####################################
#        mytags = []
#        for i in newtags:
#            mytags.append(i['name'])
#
#        from time import clock
#        begin = clock()
#
#        for f in u['friends']:
#            # 如果用户不关注我
#            #if f
#            doc = db.friend_tags.find_one({"uid": f})
#
#            # 如果用户没有uid
#            if doc is None:
#                doc = {
#                    'uid': f,
#                    'tags': {}
#                }
#                tags = db.tags.find()
#                for t in tags:
#                    doc['tags'][t['name']] = []
#
#            for tag in doc['tags']:
#                if tag in mytags:
#                    # 如果我关注这个主题，那么添加我
#                    if u['_id'] not in doc['tags'][tag]:
#                        doc['tags'][tag].append(u['_id'])
#                else:
#                    # 如果我不关注此主题，则移除我
#                    if u['_id'] in doc['tags'][tag]:
#                        doc['tags'][tag].remove(u['_id'])
#            db.friend_tags.save(doc)
#
#        end = clock()
#        #print 'time cost', end - begin
#
#        # error: 'friends'
#        LT.TRACE(LT.FRIEND_TAGS, "user {0} with {1} friends cost {2}".format(uid, len(u['friends']), end - begin))
#
        return json.dumps({'Status': 'Success'})

class choose_one_tag:
    @return_json
    @login_required
    def POST(self):
        #TODO: need test
        # NOTICE: only update status, won't delete a tag or insert a tag
        i = web.input()
        uid = current_user_id()
        tag = json.loads(i.tags, object_hook = json_util.object_hook)
        u = db.user.find_one({"_id": ObjectId(uid)}, {'tags':1})

        found = False
        for t in u['tags']:
            if t['name'] == tag['name']:
                t['status'] = tag['status']
                t['update_at'] = datetime.datetime.now()
                found = True
                break
        if not found:
            u['tags'].append({
                'status': tag['status'],
                'update_at': datetime.datetime.now(),
                'name': tag['name'],
                'order': 1
            })

        db.user.update({"_id": ObjectId(uid)}, {"$set": {"tags": u['tags']}})

        return json.dumps({'Status': 'Success'})

class my_tags:
    @return_json
    @login_required
    def GET(self):
        uid = current_user_id()
        u = db.user.find_one({"_id": ObjectId(uid)}, {"tags": 1})

        tags = []
        for t in u['tags']:
            tags.append({"name": t['name'], "order": t['order'], "status": t['status']})

        return json.dumps(tags)
def cmptime(d, x, y):
    if len(d[x['name']]['active']) == 0:
        return 0
    if len(d[y['name']]['active']) == 0:
        return 0
    return cmp(d[x['name']]['active'][0]['update_at'], d[y['name']]['active'][0]['update_at'])

class friends_tags:
    #@login_required
    @return_json
    def GET(self):
        # pull method
        uid = current_user_id()
        me = db.user.find_one({'_id': ObjectId(uid)})

        taglist = {}

        for fid in me['friends']:
            friend = db.user.find_one({'_id': fid})
            for tag in friend['tags']:
                if tag['name'] not in taglist:
                    taglist[tag['name']] = {
                        'active': [],
                        'normal': []
                    }
                item = {
                    'id': friend['_id'],
                    'username': friend['username'],
                    'weibo_id': friend['weibo_id'],
                    'profile_image_url': friend['profile_image_url'],
                    'update_at': tag['update_at']
                }
                if tag['status'] == 1:
                    taglist[tag['name']]['active'].append(item)
                elif tag['status'] == 0:
                    taglist[tag['name']]['normal'].append(item)

        # 补充我自己进去
        my_status = {}
        for tag in me['tags']:
            if tag['name'] not in taglist:
                taglist[tag['name']] = {
                    'active': [],
                    'normal': []
                }
            my_status[tag['name']] = tag['status']
            item = {
                'id': me['_id'],
                'username': me['username'],
                'weibo_id': me['weibo_id'],
                'profile_image_url': me['profile_image_url'],
                'update_at': tag['update_at']
            }
            if tag['status'] == 1:
                taglist[tag['name']]['active'].append(item)
            elif tag['status'] == 0:
                taglist[tag['name']]['normal'].append(item)

        order_list = []
        for tag in taglist:
            taglist[tag]['active'].sort(key=lambda x: x['update_at'], reverse=True)
            taglist[tag]['normal'].sort(key=lambda x: x['update_at'], reverse=True)
            order_list.append({
                'name': tag,
                'normal_count': len(taglist[tag]['normal']),
                'active_count': len(taglist[tag]['active'])
            })

        # 对tag进行排序，先按照一般感兴趣的排，然后是最近很感兴趣，然后是最近很感兴趣的最近的一个人的更新时间
        order_list.sort(key=lambda x: x['normal_count'], reverse=True)
        order_list.sort(key=lambda x: x['active_count'], reverse=True)

        order_list.sort(cmp=lambda x,y: cmptime(taglist, x, y), reverse=True)

        # 放在这儿可以减少一些性能损耗
        order_list = order_list[0:10]

        ret = []
        for tag in order_list:
            if tag['name'] not in my_status:
                me_status = -1
            else:
                me_status = my_status[tag['name']]
            ret.append({
                'tagname': tag['name'],
                'active_count': tag['active_count'],
                'normal_count': tag['normal_count'],
                'active': taglist[tag['name']]['active'],
                'normal': taglist[tag['name']]['normal'],
                'my_status': me_status
            })

        return json.dumps({'tags': ret}, default=json_util.default)

class friends_tags_push:
    @login_required
    @return_json
    def GET(self):
        uid = current_user_id()
        doc = db.friend_tags.find_one({"uid": ObjectId(uid)})

        # if none?
        if doc is None:
            return json.dumps({})

        # calc friend tags
        friends_tags = doc['tags']
        data = {}
        user_list = {} #缓存用户数据
        me = db.user.find_one({'_id': ObjectId(uid)})
        user_list[me['_id']] = me
        order_list = []

        for tag in friends_tags:
            # 对于每一个tag，扩展信息，排序
            data[tag] = {
                'list': [],
                'active': []
            }

            # todo: 判断状态和更新时间
            for user in friends_tags[tag]:
                # 对于每一个用户，获取其信息
                # TODO: need optimize
                if user in user_list:
                    u = user_list[user]
                else:
                    u = db.user.find_one({'_id': user})
                    user_list[user] = u
                if u is None:
                    continue

                item = {
                    'id': u['_id'],
                    'username': u['username'],
                    'weibo_id': u['weibo_id'],
                    'profile_image_url': u['profile_image_url']
                }
                for user_tag in u['tags']:
                    if user_tag['name'] == tag:
                        item['update_at'] = user_tag['update_at']
                        if user_tag['status'] == 1:
                            data[tag]['active'].append(item)
                        elif user_tag['status'] == 0:
                            data[tag]['list'].append(item)

            data[tag]['active'].sort(key=lambda user: user['update_at'], reverse=True)
            data[tag]['list'].sort(key=lambda user: user['update_at'], reverse=True)
            order_list.append({
                "name": tag,
                "list_total": len(data[tag]['list']),
                "active_total": len(data[tag]['active']),
            })

        # 对tag进行排序，先按照一般感兴趣的排，然后是最近很感兴趣，然后是最近很感兴趣的最近的一个人的更新时间
        order_list.sort(key=lambda x: x['list_total'], reverse=True)
        order_list.sort(key=lambda x: x['active_total'], reverse=True)

        order_list.sort(cmp=lambda x,y: cmptime(data, x, y), reverse=True)

        # 放在这儿可以减少一些性能损耗
        order_list = order_list[0:10]

        ret = []
        for item in order_list:
            tag = item['name']
            k = {
                'tagname': item['name'],
                'active_count': len(data[tag]['active']),
                'normal_count': len(data[tag]['list']),
            }
            found = False
            for mytag in me['tags']:
                if tag == mytag['name']:
                    k['my_status'] = mytag['status']
                    found = True
                    break
            if not found:
                k['my_status'] = -1

            # TODO: order of user

            k['active'] = data[tag]['active'].sort(key=lambda user: user['update_at'], reverse=True)
            k['normal'] = data[tag]['list'].sort(key=lambda user: user['update_at'], reverse=True)
            ret.append(k)

        return json.dumps({'tags': ret}, default=json_util.default)

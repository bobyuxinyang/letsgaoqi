# coding:utf-8
import string
import sys
from bson.objectid import ObjectId
from utility import LT

default_encoding = 'utf-8'
if sys.getdefaultencoding() != default_encoding:
    reload(sys)
    sys.setdefaultencoding(default_encoding)


import datetime
import json

import web
from settings import db
from utility.helper import return_json
from utility.auth import login_required, current_user_id, auth_sign

from weibopy.auth import OAuthHandler
from weibopy.api import API
from settings import CONSUME

class sinaweibo_go:
    def GET(self):
        if current_user_id():
            web.seeother('/')

        session = web.ctx.session
        oauth = OAuthHandler(CONSUME['KEY'], CONSUME['SECRET'], web.ctx.get('homedomain')+'/connect/sinaweibo/callback')

        auth_url = oauth.get_authorization_url()
        session.request_token = oauth.request_token

        web.seeother(auth_url)
        pass

class sinaweibo_callback:
    def GET(self):
        """

        """
        session = web.ctx.session

        ins = web.input()
        oauth_verifier = ins.get('oauth_verifier', None)
        request_token = session.get('request_token', None)
        oauth = OAuthHandler(CONSUME['KEY'], CONSUME['SECRET'])

        oauth.request_token = request_token
        access_token = oauth.get_access_token(oauth_verifier)
        oauth.access_token = access_token

        api = API(oauth)
        weibo_user = api.verify_credentials()

        the_user = db.user.find_one({'weibo_id': weibo_user.id, 'is_signed_up': True})

        if the_user:
            # account already exist
            the_user['sinaweibo_access_token'] = str(access_token)
            db.user.save(the_user)

            expir = datetime.datetime.now() + datetime.timedelta(days = 2)
            auth_sign(str(the_user['_id']), expir)


            # 如果记录了activity_id就直接跳转到activity去
            activity_id = session.get('activity_id', None)

            #LOG_SIGNIN_SINA
            msg = "{user}:signin with sinaweibo".format(user=str(the_user['_id']))
            LT.TRACE(LT.SIGNIN_SINA, msg)
            ################

            if activity_id:
                web.seeother('/activity?id=%s' % activity_id)
            else:
                web.seeother('/')
        else:
            # 把weibo信息写入session
            session.sinaweibo_access_token = str(access_token)
            session.weibo_id = weibo_user.id
            session.screen_name = weibo_user.screen_name
            session.profile_image_url = weibo_user.profile_image_url
            session.description = weibo_user.description

            #LOG_REGISTER_SINA
            msg = "weiboid:{id}, screen_name:{sn}, signup with sinaweibo".format(id=weibo_user.id, sn=weibo_user.screen_name)
            LT.TRACE(LT.REGISTER_SINA, msg)
            ##################

            web.seeother('/register')
        pass


class find_sinaweibo_friends:
    @return_json
    @login_required
    def POST(self):
        key = web.input(key=None).key

        result = []
        if key:
            user_id = current_user_id()
            all_users = db.sinaweibo_friends.find_one({'user_id': ObjectId(user_id)})
            if all_users:
                for f in all_users['friends']:
                    if (unicode(f['screen_name']).find(unicode(key)) >= 0) or (unicode(f['pinyin']).find(unicode(key)) >= 0):
                        result.append(f)
        return json.dumps(result[:5])
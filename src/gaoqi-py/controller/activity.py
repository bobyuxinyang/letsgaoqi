#coding:utf-8
import datetime
import json

from bson.objectid import ObjectId
from user import get_simple_user, get_user

import web
from settings import db, HOST_NAME, CONSUME
from utility.helper import return_json, epoch_to_datetime, datetime_to_epoch
from utility.auth import login_required, current_user_id
from utility import json_util, LT

from notification import send_invitation_to_user
from weibopy.api import API
from weibopy.auth import OAuthHandler
from weibopy.oauth import OAuthToken

def get_participators(participators):
    return [{
        'status': participator['status'],
        'update_at': participator['update_at'],
        'user': get_simple_user(participator['user_id']),
    } for participator in participators]
    pass


def get_activity_card(activities, start = 0, index = 0):
    """

    """
    # todo: deal with paginate
    result = []

    for activity in activities:
        result.append({
            'activity_id': str(activity['_id']),
            'created_at': activity['created_at'],
            'text': activity['text'],
            'plan_at': activity['plan_at'],
            'is_available': activity['is_available'],
            'visibility': activity['visibility'],

            'owner': get_simple_user(activity['owner_id']),
            'participators': get_participators(activity['participators']),
            'comment_count': len(activity['comments']),
            'status': _get_activity_status(activity['plan_at'], activity['is_available'], datetime.datetime.now())
        })
    return result


# 往活动里面邀请更多的人
def _invite_people_in_activity(people, activity, is_send_weibo_invitation = False, invite_text = ""):
    
    user_id = current_user_id()
    me = db.user.find_one(
            {'_id': ObjectId(user_id)},
            {'username': 1, 'profile_image_url': 1, 'sinaweibo_access_token': 1}
    )

    data = []
    for single_user in people:
        the_user = db.user.find_one({'weibo_id': single_user['weibo_id']})
        if the_user is None:
            # 添加一个临时用户
            new_user = {
                'is_signed_up': False,
                'weibo_id': single_user['weibo_id'],
                'profile_image_url': single_user['profile_image_url'],
                'username': single_user['username']
            }
            db.user.save(new_user)
            the_user = new_user
        
        # 检查这个参与者是否存在，如果不存在，将the_user作为一个参与者加入活动
        already_in = False
        for paticipator in activity['participators']:
            if paticipator['user_id'] == the_user['_id']:
                already_in = True

        if not already_in:
            activity['participators'].append({
                'user_id': the_user['_id'],
                'status': 'no-reply',
                'update_at': datetime.datetime.now(),
                'weibo_id': the_user['weibo_id'],
                'invite_by': me['username'],
            })
            # send invitations to user
            send_invitation_to_user(activity, the_user, me)
            data.append({'weibo_id': the_user['weibo_id'], 'user_id': str(the_user['_id'])})

        db.activity.save(activity)


    msg = "{aid}:{user}:list={list}".format(aid=activity['_id'], user=str(me['_id']), list=','.join(map(lambda x: str(x['weibo_id']), people)))
    LT.TRACE(LT.ACTIVITY_INVITE, msg)
    # todo: maybe we should not send the invitation immediately...
    if is_send_weibo_invitation:
        final_invitation_text = '%s %s/activity?id=%s #搞起#' % (invite_text, HOST_NAME, str(activity['_id']))
        print final_invitation_text

        #todo send weibo...
        auth = OAuthHandler(CONSUME['KEY'], CONSUME['SECRET'])
        auth.access_token = OAuthToken.from_string(me['sinaweibo_access_token'])

        api = API(auth)
        api.update_status(final_invitation_text)
        
    return data

def _get_activity_status(plan_at, is_available, now):
    if not is_available: return "cancled";
    return "ongoing" if plan_at > now else "past"




class get_user_now:
    @return_json
    @login_required
    def GET(self):
        user_id = current_user_id()
        now = datetime.datetime.now()
        result = get_activity_card(
            db.activity.find(
                    {'plan_at': {'$gt': now},
                     'participators': {'$elemMatch': {'user_id': ObjectId(user_id), 'status': 'in-activity'} }
                    }
                )
            )
        return json.dumps(result, default = json_util.default)

class get_user_before:
    @return_json
    @login_required
    def GET(self):
        user_id = current_user_id()
        now = datetime.datetime.now()
        result = get_activity_card(
            db.activity.find(
                    {'plan_at': {'$lte': now},
                     'participators': {'$elemMatch': {'user_id': ObjectId(user_id), 'status': 'in-activity'} }
                    }
                )
            )
        return json.dumps(result, default = json_util.default)

class update_activity:
    @return_json
    @login_required
    def POST(self):
        i = web.input()
        current_uid = current_user_id()

        # 将自己作为第一个参加者
        new_participators = [{
            'user_id': ObjectId(current_uid),
            'status': 'in-activity',
            'update_at': datetime.datetime.now(),
            }]

        new_activity = {
            'created_at': datetime.datetime.now(),
            'detail_text': '',
            'is_available': True,
            'visibility': 'public',
            'owner_id': ObjectId(current_uid),
            'plan_at': epoch_to_datetime(i.plan_at),
            'text': i.text,
            'participators': new_participators,
            'comments': []
        }

        db.activity.save(new_activity)

        msg = "{user}:{aid}:CREATE".format(user=str(current_uid), aid=new_activity['_id'])
        LT.TRACE(LT.ACTIVITY_NEW, msg)
        return json.dumps({'Status': 'Success', 'Data': str(new_activity['_id'])})


def get_full_comments(comments):
    for comment in comments:
        comment['sender'] = get_simple_user(comment['sender_id'])
        cid = comment['comment_id']
        cp = db.comment_replies.find_one({"comment_id": str(cid)})
        if cp is not None:
            if 'replies' in cp:
                comment['replies_count'] = len(cp['replies'])
        else:
            comment['replies_count'] = 0
    return comments


class get_activity_detail:
    @return_json
    @login_required
    def GET(self):
        activity_id = web.input().activity_id
        activity = db.activity.find_one({'_id': ObjectId(activity_id)})

        user_id = current_user_id()
        me = db.user.find_one(
            {'_id': ObjectId(user_id)},
            {'username': 1, 'profile_image_url': 1}
        )

        activity['owner'] = get_simple_user(activity['owner_id'])
        activity['status'] = _get_activity_status(activity['plan_at'], activity['is_available'], datetime.datetime.now())

        activity['comment_count'] = len(activity['comments'])
        activity['comments'] = get_full_comments(activity['comments'])

        activity['me'] = me

        for participator in activity['participators']:
            participator['user'] = get_simple_user(participator['user_id'])

        return json.dumps(activity, default=json_util.default)

class get_simple_activity:
    @return_json
    def GET(self):
        activity_id = web.input().activity_id
        activity = db.activity.find_one({'_id': ObjectId(activity_id)})
        activity['owner'] = get_simple_user(activity['owner_id'])
        
        return json.dumps(activity, default=json_util.default)


def _remove_invitation(user_id, activity_id):
    db.user.update(
        {'_id': ObjectId(user_id)},
        {'$pull': {'invitations': {'activity_id': ObjectId(activity_id)} }}
    )
    pass



class confirm_activity:
    @return_json
    @login_required
    def POST(self):
        user_id = current_user_id()
        activity_id = web.input().activity_id
        db.activity.update(
                {'_id': ObjectId(activity_id), 'participators.user_id': ObjectId(user_id)},
                {'$set': {'participators.$.status': 'in-activity'}}
        )
        # remove the invitation
        _remove_invitation(user_id, activity_id)

        return json.dumps({'Status': 'Success', 'Info':'已经确认参加'})


class quit_activity:
    @return_json
    @login_required
    def POST(self):
        user_id = current_user_id()
        activity_id = web.input().activity_id
        db.activity.update(
                {'_id': ObjectId(activity_id)},
                {'$pull': {'participators': {'user_id': ObjectId(user_id)} }}
        )
        _remove_invitation(user_id, activity_id)
        return json.dumps({'Status': 'Success'})


class update_plan_at:
    @return_json
    @login_required
    def POST(self):
        plan_at = epoch_to_datetime(web.input().plan_at)
        activity_id = web.input().activity_id
        db.activity.update(
                {'_id': ObjectId(activity_id)},
                {'$set': {'plan_at': plan_at}}
        )

        return json.dumps({'Status': 'Success', 'Info': '活动时间修改完成'})

class update_text:
    @return_json
    @login_required
    def POST(self):
        text = web.input().text
        activity_id = web.input().activity_id
        db.activity.update(
                {'_id': ObjectId(activity_id)},
                {'$set': {'text': text}}
        )
        return json.dumps({'Status': 'Success', 'Info': '活动标题修改完成'})

class update_detail_text:
    @return_json
    @login_required
    def POST(self):
        detail_text = web.input().detail_text
        activity_id = web.input().activity_id
        db.activity.update(
                {'_id': ObjectId(activity_id)},
                {'$set': {'detail_text': detail_text}}
        )
        return json.dumps({'Status': 'Success', 'Info': '活动细节更新完成'})


class invite_more_participators:
    @return_json
    @login_required
    def POST(self):
        i = web.input()
        activity_id = i.activity_id
        people = json.loads(i.invitePeople, object_hook = json_util.object_hook)
        activity = db.activity.find_one({'_id': ObjectId(activity_id)})


        is_send_weibo_invitation = i.is_send_weibo_invitation == 'true'
        invite_text = i.invite_text

        data = _invite_people_in_activity(people, activity, is_send_weibo_invitation, invite_text)
        return json.dumps({'Status': 'Success', 'Info': '邀请已经发送', 'Data': data})

class get_comments:
    @return_json
    @login_required
    def GET(self):
        i = web.input(start_index = 0, count = 10)
        activity_id = i.activity_id
        start_index = i.start_index
        count = i.count
        activity = db.activity.find_one({'_id': ObjectId(activity_id)}, {'comments': {'$slice': [start_index, count]}})
        comments = get_full_comments(activity['comments'])
        return json.dumps(comments, default = json_util.default)

class new_comment:
    @return_json
    @login_required
    def POST(self):
        i = web.input()
        activity_id = i.activity_id
        sender_id = i.sender_id
        text = i.text
        cid = ObjectId()
        new_comment = {
            'created_at': datetime.datetime.now(),
            'sender_id': sender_id,
            'text': text,
            'comment_id': cid
        }
        db.activity.update({'_id': ObjectId(activity_id)}, {'$push': {'comments': new_comment}})
        return json.dumps({'Status': 'Success', 'Info': '添加了留言', 'Data': {'comment_id': str(cid)}})

class new_comment_reply:
    @return_json
    @login_required
    def POST(self):
        i = web.input()
        comment_id = i.comment_id
        text = i.text
        sender_id = i.sender_id
        u = db.user.find_one({'_id': ObjectId(sender_id)})
        c = db.comment_replies.find_one({"comment_id": comment_id})
        if c is None:
            c = {
                'comment_id': comment_id,
                'replies': []
            }
        new_reply = {
            'created_at': datetime.datetime.now(),
            'sender': {
                'sender_id': sender_id,
                'username': u['username'],
                'profile_image_url': u['profile_image_url']
            },
            'text': text
        }
        c['replies'].append(new_reply)
        db.comment_replies.save(c)

        return json.dumps({'Status': 'Success', 'Info': '回复添加成功'})


class get_comment_replies:
    @return_json
    @login_required
    def POST(self):
        comment_id = web.input().comment_id
        c = db.comment_replies.find_one({'comment_id': comment_id})
        if c is None:
            return json.dumps([])
        return json.dumps(c['replies'], default = json_util.default)
    
class has_before_activity:
    @return_json
    @login_required
    def POST(self):
        now = datetime.datetime.now()
        user_id = current_user_id()
        has_activity_before = db.activity.find_one(
            {'plan_at': {'$lt': now},
             'participators': {'$elemMatch': {'user_id': ObjectId(user_id)} }
            }
        ) is not None
        return json.dumps(has_activity_before)

class interest:
    @return_json
    @login_required
    def POST(self):
        user_id = current_user_id()
        activity_id = web.input().activity_id
        db.activity.update(
                {'_id': ObjectId(activity_id), 'participators.user_id': ObjectId(user_id)},
                {'$set': {'participators.$.status': 'making-decision'}}
        )
        # remove the invitation
        _remove_invitation(user_id, activity_id)

        return json.dumps({'Status': 'Success', 'Info':'您对该活动感兴趣'})

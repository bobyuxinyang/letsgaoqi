#coding:utf-8

import web
import datetime
from bson.objectid import ObjectId
from utility.helper import return_json
from settings import db
from utility.auth import login_required, current_user_id
from utility import json_util
import json

__all__ = [
    'send_invitation_to_user'
]


def send_invitation_to_user(activity, the_user, inviter):
    db.user.update({'_id': the_user['_id']},
       {'$push': {'invitations': {
            'activity_id': activity['_id'],
            'activity_name': activity['text'],
            'created_at': datetime.datetime.now(),
            'inviter': {
                'user_id': inviter['_id'],
                'username': inviter['username'],
                'profile_image_url': inviter['profile_image_url']
            }
        }}}
       )
    pass

class get_invitations():
    @login_required
    @return_json
    def GET(self):
        user_id = current_user_id()
        result = db.user.find_one({'_id': ObjectId(user_id)}, {'invitations': 1} )['invitations']
        return json.dumps(result, default = json_util.default)

#coding:utf-8

import web
import datetime
from settings import db,gearman
from utility.helper import return_json, render_template
from utility import auth


import json
from utility import LT

class email_chk:
    @return_json
    def POST(self):
        i = web.input(email="")
        email = i.email
        user = db.user.find_one({'email_lower': email.lower()})
        if not user:
            return json.dumps({'Status': 'Success'})
        else:
            return json.dumps({'Status': 'Error'})
        
    
class pwd_chk:
    @return_json
    def POST(self):
        i = web.input()
        pwd = i.password
        length =  len(pwd)
        if length >=6 and length <= 30:
            return json.dumps({'Status': 'Success'})
        else:
            return json.dumps({'Status': 'Error'})

class reg_user:
    @return_json
    def POST(self):
        session = web.ctx.session

        i = web.input()
        email = i.email
        password = i.password

        weibo_id = session.get('weibo_id', None)
        screen_name = session.get('screen_name', None)
        profile_image_url = session.get('profile_image_url', None)
        description = session.get('description', None)

        new_user = db.user.find_one({'weibo_id': weibo_id})
        if not new_user:
            new_user = {
               "profile_image_url": profile_image_url,
               "username": screen_name,
               "weibo_id": weibo_id,
               "invitations": []
            }
        
        new_user['created_at'] = datetime.datetime.now()
        new_user['email'] = email
        new_user['description'] = description
        new_user['password'] = auth.md5pwd(password)
        new_user['email_lower'] = email.lower()
        new_user["tags"] = []
        new_user["friends"] = []
        new_user['is_signed_up'] = False

        # save weibo_token
        sinaweibo_token = session.get('sinaweibo_access_token', None)
        new_user['sinaweibo_access_token'] = sinaweibo_token

        db.user.save(new_user)

        # send register mail to user via email address
        gearman.submit_job("send_register_email", str(new_user['_id']), background=True, wait_until_complete=False)

        #LOG_ACTIVE_MAIL_SEND
        msg = "{user}:send active mail".format(user=str(new_user['_id']))
        LT.TRACE(LT.ACTIVE_MAIL_SEND, msg)
        ###########################

        session.cur_email = new_user['email']

        return json.dumps({'Status': 'Success'})


    
class active_mail():
    def GET(self):
        #todo: check if need to active_mail, or goto '/'
        session = web.ctx.session
        cu_email = session.get('cur_email', '')
        return render_template('active-mail.html', email=cu_email)

# coding=utf-8
from bson.objectid import ObjectId

from utility.helper import render_template, return_json
from utility.auth import current_user_id,  login_required
from settings import db
import user
import web
import json
from utility import json_util

class index():
    def GET(self):
        user_id = current_user_id()
        if user_id:
            # 看看用户当前有没有“特别想去”的活动主题
            if not user.has_active_tags(user_id):
                u = db.user.find_one({'_id': ObjectId(user_id)})
                return render_template('choose-tags.html', cu = u)
            else:
                return render_template('index.html')
        else:
            return render_template('login.html')

class register():
    def GET(self):

        user_id = current_user_id()
        if user_id:
            web.seeother('/')

        # 确认已经通过了授权，否则授权去
        session = web.ctx.session
        access_token = session.get('sinaweibo_access_token', None)
        weibo_id = session.get('weibo_id', None)
        if not access_token:
            return web.seeother('/')

        # if user existed
        the_user = db.user.find_one({'weibo_id': weibo_id, 'is_signed_up': True})
        if the_user:
            web.seeother('/')

        if access_token:
            u = {
                'weibo_id': weibo_id,
                'username': session.get('screen_name', None),
                'profile_image_url': session.get('profile_image_url', None)
            }
            return render_template('register.html', cu=u)
        # TODO:here is error!
        web.seeother("/")

class activity():
    def GET(self):
        session = web.ctx.session
        
        user_id = current_user_id()
        if user_id:
            return render_template('activity.html')
        else:
            session.activity_id = web.input(id='').get('id')
            return render_template('activity-public.html')
    @login_required
    def POST(self):
        return render_template('activity.html')


class favicon():
    def GET(self):
        #todo: avoid favicon 404 not found
        pass

class pre_index():
    def GET(self):
        return render_template('pre-index.html')

    
class all_tags():
    @return_json
    def GET(self):
        tags = db.tags.find({}, {"name":1, "index": 1})
        data = []
        for t in tags:
            data.append({"name":t['name'], "index": t['index']})
        return json.dumps(data)

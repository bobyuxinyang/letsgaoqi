from gearman import *

def check_request_status(job):
    if job.complete:
        print "Job %s finished!  Result: %s - %s" % (job.job.unique, job.state, job.result)
    elif job.time_out:
        print "Job %s timed out!" % job.unique
    elif job.state == JOB_UNKNOWN:
        print "Job %s connection failed!" % job.unique
        
client = GearmanClient(["127.0.0.1"])

job = client.submit_job("reverse", "echo string", background=True, wait_until_complete=False)

check_request_status(job)
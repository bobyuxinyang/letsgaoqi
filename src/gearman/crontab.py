#!/usr/bin/env python
# -*- coding:utf8 -*-
from time import sleep
from gearman import *
from pymongo import Connection


_connection = Connection('192.168.10.10', 27017)
db = _connection['gaoqi-db']
client = GearmanClient(["127.0.0.1"])

users = db.user.find({}, {"_id": 1})

for u in users:
    job = client.submit_job("get_following_people", u["_id"])
    sleep(3)

#job = client.submit_job("get_following_people", userid, background=True)

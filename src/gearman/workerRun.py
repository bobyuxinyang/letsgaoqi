#!/usr/bin/env python
# -*- coding:utf8 -*-

from gearman import *
from SinaWeiboFriends import get_following_people
from sendemail import send_register_email

worker = GearmanWorker(["127.0.0.1"])
worker.set_client_id('default')
worker.register_task("send_register_email", send_register_email)
worker.register_task("get_following_people", get_following_people)
worker.work()
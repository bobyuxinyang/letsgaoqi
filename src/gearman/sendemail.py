# -*- coding:utf8 -*-
from gearman import *

from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEImage import MIMEImage
import smtplib
from pymongo import Connection
from bson.objectid import ObjectId

_connection = Connection('localhost', 27017)
db = _connection['gaoqi-db']

auth = {
    'server': 'smtp.gmail.com',
    'username': 'gaoqi.fm@gmail.com',
    'password': 'gaoqiweimar'
}

def connect():
    smtp = smtplib.SMTP()
    smtp.set_debuglevel(0)
    smtp.connect(auth['server'], 587)
    smtp.ehlo()
    smtp.starttls()
    smtp.ehlo()
    smtp.login(auth['username'], auth['password'])
    return smtp
    

def send_register_email(worker, job):
    '''send regist email'''
    uid = job.data
    from_addr = 'gaoqi.fm@gmail.com'

    # combina body
    msgRoot = MIMEMultipart('related')
    msgRoot['Subject'] = 'gaoqi.fm 注册通知'
    msgRoot['From'] = "搞起！ <%s>" % from_addr

    msgAlternative = MIMEMultipart('alternative')
    msgRoot.attach(msgAlternative)

    ##设定HTML信息
    user = db.user.find_one({"_id": ObjectId(uid)})
    to_addr = user['email']
    msgRoot['To'] = to_addr
    
    url = u"http://www.gaoqi.fm/user/active/%s" % uid
    orgintext = u'''
%s, 你好：<br/><br/>
欢迎加入<a href="http://www.gaoqi.fm">搞起</a>。<br/><br/>
很高兴您使用我们的服务，点击以下链接即可激活您的帐号:<br/><br/>
<a href="%s">%s</a><br/><br/>
如果链接无法点击，请完整拷贝到浏览器地址栏直接访问：）<br/><br/>
非常期待您来搞起活动
'''
    content = orgintext % (user['username'], url, url)
    msgText = MIMEText(content, 'html', 'utf-8')
    msgAlternative.attach(msgText)

#    设定内置图片信息
#    fp = open('test.jpg', 'rb')
#    msgImage = MIMEImage(fp.read())
#    fp.close()
#    msgImage.add_header('Content-ID', '<image1>')
#    msgRoot.attach(msgImage)

    # send mail
    smtp = connect()
    smtp.sendmail(from_addr, to_addr, msgRoot.as_string())
    smtp.quit()
    return "OK"

def sendResetPwdEmail(worker, job):
    '''send reset password email'''
    pass

if __name__ == "__main__":
    worker = GearmanWorker(["127.0.0.1"])
    worker.set_client_id('sendemail')
    worker.register_task("sendRegisterEmail", sendRegisterEmail)
    worker.work()
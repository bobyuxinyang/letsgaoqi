from gearman import *

client = GearmanClient(["127.0.0.1"])

#set access_token.to_string
userid = "4ea11f63345e328f28000000"

job = client.submit_job("get_following_people", userid, background=True, wait_until_complete=False)


def check_request_status(job):
    if job.complete:
        print "Job %s finished!  Result: %s - %s" % (job.job.unique, job.state, job.result)
    elif job.time_out:
        print "Job %s timed out!" % job.unique
    elif job.state == JOB_UNKNOWN:
        print "Job %s connection failed!" % job.unique
        
#if not background
check_request_status(job)

gm_admin_client = GearmanAdminClient(['127.0.0.1'])

# Inspect server state
status_response = gm_admin_client.get_status()
while 1:
    if status_response[0]['running'] == 0:
        print status_response
        break
    status_response = gm_admin_client.get_status()

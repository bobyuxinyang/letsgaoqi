# -*- coding:utf8 -*-
from gearman import *
from weibopy.auth import OAuthHandler
from weibopy.oauth import OAuthToken
from weibopy.api import API
from pymongo import Connection
from bson.objectid import ObjectId
from hanzi2pinyin import hanzi2pinyin
import datetime

_connection = Connection('localhost', 27017)
db = _connection['gaoqi-db']

CONSUME_KEY= "3658488841"
CONSUME_SECRET ="444c14e96182698a0e748de2d7d0b4d3"

def get_following_people(worker, job):
    auth = OAuthHandler(CONSUME_KEY, CONSUME_SECRET)
    
    userid = job.data
    u = db.user.find_one({'_id': ObjectId(userid)}, {"sinaweibo_access_token": 1})

    if "sinaweibo_access_token" not in u:
        print "user {0} don't have access_token"
        return ""

    auth.access_token = OAuthToken.from_string(u["sinaweibo_access_token"])
    
    api = API(auth)
    user = api.verify_credentials()
    total = user.friends_count
    
    next_cursor = 0
    limit = 200
    data = []

    # TODO: chagne api to fetch follow each
    while True:
        #print "%d%% has done" % (next_cursor * 100 / user.friends_count)
        timeline = api.friends(count=limit, cursor=next_cursor)
        data.extend(timeline)
        
        next_cursor += limit #len(timeline)
        if next_cursor > total:
            break
    
    friends_list = []
    for f in data:
        #print f.screen_name, f.id, f.profile_image_url
        friends_list.append({
          'screen_name': f.screen_name,
          'pinyin' : hanzi2pinyin(f.screen_name).lower(),
          'weibo_id': f.id,
          'profile_image_url': f.profile_image_url,
          # not very need
          #'domain': f.domain,
        })
    db.sinaweibo_friends.update(
        {'user_id': ObjectId(userid)},
        {'user_id': ObjectId(userid), 'friends':friends_list},
        True
    )

    # 更新用户好友列表，此处要求用户好友不仅在user表，而且signed_up
    friends = []
    for f in data:
        exist = db.user.find_one({"weibo_id": f.id, "is_signed_up": True}, {"_id": 1})
        if exist is None:
            continue
        # 互相关注，自己也必须出现在对方的关注列表中，在新版的互粉api之后可以废弃
        follow_me = db.sinaweibo_friends.find_one({'user_id': exist['_id'], 'friends': {'$elemMatch': {'weibo_id': u['weibo_id']}}}, {'_id': 1})
        if follow_me is not None:
            friends.append(exist["_id"])
    db.user.update({"_id": ObjectId(userid)}, {"$set": {"friends": friends}})

    # update last update time
    db.user.update({"_id": ObjectId(userid)}, {"$set": {"last_fetch_weibo_friends_at": datetime.datetime.now()}})
    #print len(data)
    return "%d" % len(friends_list)

if __name__ == "__main__":
    worker = GearmanWorker(["127.0.0.1"])
    worker.set_client_id('sinaweibo')
    worker.register_task("get_following_people", get_following_people)
    worker.work()
